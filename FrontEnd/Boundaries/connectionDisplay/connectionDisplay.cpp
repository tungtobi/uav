#include "./FrontEnd/Boundaries/connectionDisplay/connectionDisplay.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>

//Implementation of ConnectionDisplay's public methods
ConnectionDisplay::ConnectionDisplay(QObject *parent) : QObject(parent)
{
    //Initiate internal buffer variables for ConnectionDisplay
    controller = new ConnectionDisplayCtrlr(this);
    QThreadPool::globalInstance()->start(controller, 2);
}

QString ConnectionDisplay::inputAddress() {
    return inputAddressBuffer;
}

void ConnectionDisplay::setInputAddress(const QString &inputAddr) {
    inputAddressBuffer = inputAddr;
}

QString ConnectionDisplay::address() {
    return addressBuffer;
}

void ConnectionDisplay::setAddress(const QString &addr) {
    addressBuffer = addr;
    emit addressChanged();
}

QString ConnectionDisplay::connectStatus() {
    return connectStatusBuffer;
}

void ConnectionDisplay::setConnectStatus(const QString &connectStatus) {
    connectStatusBuffer = connectStatus;
    emit connectStatusChanged();
}

void ConnectionDisplay::connect() {
    controller->connect();
}

void ConnectionDisplay::disconnect() {
    controller->disconnect();
}
