#ifndef CONNECTIONDISPLAY_H
#define CONNECTIONDISPLAY_H

#include <QObject>
#include <QThreadPool>
#include "./FrontEnd/Controllers/connectionDisplayCtrlr/connectionDisplayCtrlr.h"

class ConnectionDisplay : public QObject
{
    Q_OBJECT
    //Properties for ConnectionDisplay
    Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(QString inputAddress READ inputAddress WRITE setInputAddress)
    Q_PROPERTY(QString connectStatus READ connectStatus WRITE setConnectStatus NOTIFY connectStatusChanged)

public:
    //Public methods/variables for ConnectionDisplay
    explicit ConnectionDisplay(QObject *parent = nullptr);
    Q_INVOKABLE QString inputAddress();
    QString connectStatus();
    QString address();
    Q_INVOKABLE void connect();
    Q_INVOKABLE void disconnect();

public slots:
    //Slots for ConnectionDisplay
    void setConnectStatus(const QString &connectStatus);
    void setInputAddress(const QString &inputAddr);
    void setAddress(const QString &addr);

signals:
    //Signals for ConnectionDisplay
    void connectStatusChanged();
    void addressChanged();

private:
    //Private internal buffer variables and methods for ConnectionDisplay
    QString inputAddressBuffer;
    QString connectStatusBuffer;
    QString addressBuffer;
    ConnectionDisplayCtrlr *controller;

};

#endif // CONNECTIONDISPLAY_H
