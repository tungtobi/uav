import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import gcs.display.ConnectionDisplay 1.0


Rectangle {
    id: connectionDisplay
    color: "#2B2B2B"
    anchors.fill: parent
    ConnectionDisplay {
        id: connectionDisplayCpp
    }

    RowLayout {
        id: connectorPane
        height: parent.height
        width: (2 / 3) * parent.width
        anchors.top: parent.top
        anchors.left: parent.left
        Rectangle {
            id: addressFieldBox
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            width: 0.45 * parent.width
            height: 0.95 * parent.height
            anchors.leftMargin: 0.025 * parent.width
            anchors.topMargin: 0.025 * parent.height
            color: "transparent"
            TextField {
                id: addreddField
                anchors.fill: parent
                text: connectionDisplayCpp.inputAddress
                color: "#dedcd3"
                onTextChanged: connectionDisplayCpp.inputAddress = text
                background: Rectangle {
                    anchors.top: parent.top
                    anchors.left: parent.left
                    radius: 3
                    color: "#545454"
                    width: parent.width
                    height: parent.height
                }
            }
        }

        Rectangle {
            id: buttonAndStatusBox
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: addressFieldBox.right
            width: 0.505 * parent.width
            height: 0.95 * parent.height
            anchors.leftMargin: 0.025 * parent.width
            anchors.topMargin: 0.025 * parent.height
            color: "transparent"
            Rectangle {
                id: buttonBox
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                width: 0.4 * parent.width
                height: parent.height
                color: "transparent"
                Rectangle {
                    id: connectBtnBox
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: addressFieldBox.right
                    width: 0.4875 * parent.width
                    height: parent.height
                    color: "transparent"
                    Button {
                        id: connectBtn
                        anchors.fill: parent
                        text: "Connect"
                        state: pressed ? "pressed" : "regular"
                        property color connectBtnColor
                        onClicked: connectionDisplayCpp.connect()
                        background: Rectangle {
                            anchors.top: parent.top
                            anchors.left: parent.left
                            radius: 3
                            color: connectBtn.connectBtnColor
                            width: parent.width
                            height: parent.height
                        }
                        states: [
                            State {
                                name: "regular"
                                PropertyChanges {
                                    target: connectBtn
                                    connectBtnColor: "lightgreen"
                                }
                            },
                            State {
                                name: "pressed"
                                PropertyChanges {
                                    target: connectBtn
                                    connectBtnColor: "green"
                                }
                            }
                        ]
                    }
                }

                Rectangle {
                    id: disconnectBtnBox
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: connectBtnBox.right
                    width: 0.4875 * parent.width
                    height: parent.height
                    anchors.leftMargin: 0.025 * parent.width
                    color: "transparent"
                    Button {
                        id: disconnectBtn
                        anchors.fill: parent
                        text: "Discon."
                        state: pressed ? "pressed" : "regular"
                        property color disconnectBtnColor
                        onClicked: connectionDisplayCpp.disconnect()
                        background: Rectangle {
                            radius: 3
                            anchors.top: parent.top
                            anchors.left: parent.left
                            color: disconnectBtn.disconnectBtnColor
                            width: parent.width
                            height: parent.height
                        }
                        states: [
                            State {
                                name: "regular"
                                PropertyChanges {
                                    target: disconnectBtn
                                    disconnectBtnColor: "pink"
                                }
                            },
                            State {
                                name: "pressed"
                                PropertyChanges {
                                    target: disconnectBtn
                                    disconnectBtnColor: "darkred"
                                }
                            }
                        ]
                    }
                }
            }
            Rectangle {
                id: statusBox
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                width: 0.59 * parent.width
                height: 0.95 * parent.height
                anchors.topMargin: 0.025 * parent.height
                color: "transparent"
                border.width: 3
                border.color: if (connectionDisplayCpp.connectStatus == "Connected") {
                                  "lightgreen"
                              }
                              else if (connectionDisplayCpp.connectStatus == "Connecting...") {
                                  "yellow"
                              }
                              else {
                                  "red"
                              }

                Text {
                    id: statusText
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: 0.1 * parent.height
                    text: qsTr("Aircraft: ") + connectionDisplayCpp.connectStatus
                    color: if (connectionDisplayCpp.connectStatus == "Connected") {
                               "lightgreen"
                           }
                           else if (connectionDisplayCpp.connectStatus == "Connecting...") {
                               "yellow"
                           }
                           else {
                               "red"
                           }
                    font.pointSize: 12
                }

                Text {
                    id: addressText
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottomMargin: 0.1 * parent.height
                    text: qsTr("Addr.: ") + connectionDisplayCpp.address
                    color: "#c5c2b2"
                    font.pointSize: 12
                }
            }
        }
    }

    RowLayout {
        id: softwareInfo
        height: parent.height
        width: (1 / 3) * parent.width
        anchors.top: parent.top
        anchors.left: connectorPane.right
        Text {
            id: softwareName
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 0.1 * parent.height
            text: qsTr("UAV System Ground Monitoring Software")
            font.bold: true
            color: "#c5c2b2"
            font.pointSize: 14
        }

        Text {
            id: versionBuildInfoText
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.bottomMargin: 0.1 * parent.height
            anchors.leftMargin: 0.025 * parent.width
            text: qsTr("Ver. 1.5.0A. Built 03/Dec/2021")
            color: "#c5c2b2"
            font.pointSize: 11
        }

        Text {
            id: ownershipCopyrightText
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.bottomMargin: 0.1 * parent.height
            anchors.leftMargin: 0.025 * parent.width
            text: qsTr("©2021 SAE-UET, VNUH")
            color: "#c5c2b2"
            font.pointSize: 11
        }
    }
}
