#ifndef NAVIGATIONDISPLAY_H
#define NAVIGATIONDISPLAY_H

#include <QObject>
#include <QThreadPool>
#include <QList>
#include "./FrontEnd/Controllers/navigationDisplayCtrlr/navigationDisplayCtrlr.h"

class NavigationDisplay : public QObject
{
    Q_OBJECT
    //Properties for Navigation Display
    Q_PROPERTY(int mapRangeMode READ mapRangeMode NOTIFY mapRangeModeChanged)
    Q_PROPERTY(double latitude READ latitude WRITE setLatitude NOTIFY latitudeChanged)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude NOTIFY longitudeChanged)
    Q_PROPERTY(float heading READ heading WRITE setHeading NOTIFY headingChanged)
    Q_PROPERTY(QList<qreal> predictedLatitudes READ predictedLatitudes WRITE setPredictedLatitudes NOTIFY predictedLatitudesChanged)
    Q_PROPERTY(QList<qreal> predictedLongitudes READ predictedLongitudes WRITE setPredictedLongitudes NOTIFY predictedLongitudesChanged)

public:
    explicit NavigationDisplay(QObject *parent = nullptr);

    //Public methods/variables for Navigation Display
    int mapRangeMode();
    Q_INVOKABLE void increaseRange();
    Q_INVOKABLE void decreaseRange();
    double latitude();
    double longitude();
    float heading();
    QList<qreal> predictedLatitudes();
    QList<qreal> predictedLongitudes();

public slots:
    //Slots for Navigation Display
    void setLatitude(const double &lat);
    void setLongitude(const double &lon);
    void setHeading(const float &hdg);
    void setPredictedLatitudes(const QList<qreal> &prdctLat);
    void setPredictedLongitudes(const QList<qreal> &prdctLong);

signals:
    //Signals for Navigation Display
    void mapRangeModeChanged();
    void latitudeChanged();
    void longitudeChanged();
    void headingChanged();
    void predictedLatitudesChanged();
    void predictedLongitudesChanged();

private:
    //Private internal buffer variables and methods
    NavigationDisplayCtrlr *controller;

    //Private internal buffer variables and methods for Navigation Display
    int mapRangeModeBuffer;
    double latitudeBuffer;
    double longitudeBuffer;
    float headingBuffer;
    QList<qreal> predictedLatitudesBuffer;
    QList<qreal> predictedLongitudesBuffer;

};

#endif // NAVIGATIONDISPLAY_H
