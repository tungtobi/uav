import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import QtPositioning 5.12
import QtLocation 5.12
import gcs.display.NavigationDisplay 1.0


Rectangle {
    id: nd
    anchors.fill: parent
    NavigationDisplay {
        id: navigationDisplayCpp
    }

    Loader {
        id: navDisplayHeadingIndicator
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: 0.1 * parent.height
        z: 2
        source: "./navDisplayHeadingIndicator/navDisplayHeadingIndicator.qml"
    }

    Loader {
        id: navigationDisplayMap
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: 1.4 * parent.height
        z: 1
        source: "./navigationDisplayMap/navigationDisplayMap.qml"
    }

    Rectangle {
        id: innerCircleRangeTextBox
        width: 0.5 * parent.width
        height: 0.025 * parent.height
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: rangePanel.top
        color: "transparent"
        z: 2

        Text {
            id: innerCircleRangeText
            anchors.centerIn: parent
            property var innerCircleRangeValue: if (navigationDisplayCpp.mapRangeMode == 1) {
                                                    "50"
                                                }
                                                else if (navigationDisplayCpp.mapRangeMode == 2) {
                                                    "100"
                                                }
                                                else if (navigationDisplayCpp.mapRangeMode == 3) {
                                                    "200"
                                                }
                                                else if (navigationDisplayCpp.mapRangeMode == 4) {
                                                    "400"
                                                }
                                                else if (navigationDisplayCpp.mapRangeMode == 5) {
                                                    "800"
                                                }
                                                else if (navigationDisplayCpp.mapRangeMode == 6) {
                                                    "1600"
                                                }
                                                else if (navigationDisplayCpp.mapRangeMode == 7) {
                                                    "3200"
                                                }
                                                else if (navigationDisplayCpp.mapRangeMode == 8) {
                                                    "6400"
                                                }
            text: qsTr("Inner circle range: ") + innerCircleRangeValue + qsTr(" (M)")
            color: "#2B2B2B"
            font.pointSize: 12
            font.bold: true
        }
    }

    Loader {
        id: rangePanel
        width: 0.5 * parent.width
        height: 0.05 * parent.height
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0.015 * parent.height
        z: 2
        source: "./rangePanel/rangePanel.qml"
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.9}
}
##^##*/
