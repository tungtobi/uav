#include "./FrontEnd/Boundaries/navigationDisplay/navigationDisplay.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>


//Private functions
NavigationDisplay::NavigationDisplay(QObject *parent) : QObject(parent)
{
    mapRangeModeBuffer = 1;
    controller = new NavigationDisplayCtrlr(this);
    QThreadPool::globalInstance()->start(controller, 3);
}

int NavigationDisplay::mapRangeMode() {
    return mapRangeModeBuffer;
}

void NavigationDisplay::increaseRange() {
    if (mapRangeModeBuffer < 8) {
        mapRangeModeBuffer++;
        emit mapRangeModeChanged();
    }
}

void NavigationDisplay::decreaseRange() {
    if (mapRangeModeBuffer > 1) {
        mapRangeModeBuffer--;
        emit mapRangeModeChanged();
    }
}


//Implementation of Navigation Display's public functions
void NavigationDisplay::setLatitude(const double &lat) {
    latitudeBuffer = lat;
    emit latitudeChanged();
}

double NavigationDisplay::latitude() {
    return latitudeBuffer;
}


void NavigationDisplay::setLongitude(const double &lon) {
    longitudeBuffer = lon;
    emit longitudeChanged();
}

double NavigationDisplay::longitude() {
    return longitudeBuffer;
}

void NavigationDisplay::setHeading(const float &hdg) {
    headingBuffer = hdg;
    emit headingChanged();
}

float NavigationDisplay::heading() {
    return headingBuffer;
}

void NavigationDisplay::setPredictedLatitudes(const QList<qreal> &prdctLat) {
    predictedLatitudesBuffer = prdctLat;
    emit predictedLatitudesChanged();
}

QList<qreal> NavigationDisplay::predictedLatitudes() {
    return predictedLatitudesBuffer;
}

void NavigationDisplay::setPredictedLongitudes(const QList<qreal> &prdctLong) {
    predictedLongitudesBuffer = prdctLong;
    emit predictedLongitudesChanged();
}

QList<qreal> NavigationDisplay::predictedLongitudes() {
    return predictedLongitudesBuffer;
}
