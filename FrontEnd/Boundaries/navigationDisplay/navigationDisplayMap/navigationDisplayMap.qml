import QtQuick 2.12
import QtPositioning 5.12
import QtLocation 5.12
import gcs.display.NavigationDisplay 1.0


Rectangle {
    id: navigationDisplayMap
    anchors.fill: parent

    Plugin {
        id: mapPlugin
        name: "osm"
    }

    Map {
        id: map
        anchors.fill: parent
        plugin: mapPlugin
        zoomLevel: if (navigationDisplayCpp.mapRangeMode == 1) {
                       "18"
                   }
                   else if (navigationDisplayCpp.mapRangeMode == 2) {
                       "17"
                   }
                   else if (navigationDisplayCpp.mapRangeMode == 3) {
                       "16"
                   }
                   else if (navigationDisplayCpp.mapRangeMode == 4) {
                       "15"
                   }
                   else if (navigationDisplayCpp.mapRangeMode == 5) {
                       "14"
                   }
                   else if (navigationDisplayCpp.mapRangeMode == 6) {
                       "13"
                   }
                   else if (navigationDisplayCpp.mapRangeMode == 7) {
                       "12"
                   }
                   else if (navigationDisplayCpp.mapRangeMode == 8) {
                       "11"
                   }
        bearing: navigationDisplayCpp.heading
        center {
            latitude: navigationDisplayCpp.latitude
            longitude: navigationDisplayCpp.longitude
        }

        MapCircle {
            id: mapInnerRange
            center {
                latitude: navigationDisplayCpp.latitude
                longitude: navigationDisplayCpp.longitude
            }
            radius: if (navigationDisplayCpp.mapRangeMode == 1) {
                        "50.0"
                    }
                    else if (navigationDisplayCpp.mapRangeMode == 2) {
                        "100.0"
                    }
                    else if (navigationDisplayCpp.mapRangeMode == 3) {
                        "200.0"
                    }
                    else if (navigationDisplayCpp.mapRangeMode == 4) {
                        "400.0"
                    }
                    else if (navigationDisplayCpp.mapRangeMode == 5) {
                        "800.0"
                    }
                    else if (navigationDisplayCpp.mapRangeMode == 6) {
                        "1600.0"
                    }
                    else if (navigationDisplayCpp.mapRangeMode == 7) {
                        "3200.0"
                    }
                    else if (navigationDisplayCpp.mapRangeMode == 8) {
                        "6400.0"
                    }
            border.width: 3
            border.color: "#2B2B2B"
        }

        MapPolyline {
            id: mapPredictedPath
            line.width: 3
            line.color: if (navigationDisplayCpp.mapRangeMode <= 4 && navigationDisplayCpp.mapRangeMode >= 1) {
                            "green"
                        }
                        else {
                            "transparent"
                        }
            path: if (navigationDisplayCpp.mapRangeMode == 1) {
                      [
                      { latitude: navigationDisplayCpp.predictedLatitudes[0], longitude: navigationDisplayCpp.predictedLongitudes[0] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[1], longitude: navigationDisplayCpp.predictedLongitudes[1] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[2], longitude: navigationDisplayCpp.predictedLongitudes[2] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[3], longitude: navigationDisplayCpp.predictedLongitudes[3] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[4], longitude: navigationDisplayCpp.predictedLongitudes[4] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[5], longitude: navigationDisplayCpp.predictedLongitudes[5] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[6], longitude: navigationDisplayCpp.predictedLongitudes[6] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[7], longitude: navigationDisplayCpp.predictedLongitudes[7] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[8], longitude: navigationDisplayCpp.predictedLongitudes[8] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[9], longitude: navigationDisplayCpp.predictedLongitudes[9] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[10], longitude: navigationDisplayCpp.predictedLongitudes[10] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[11], longitude: navigationDisplayCpp.predictedLongitudes[11] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[12], longitude: navigationDisplayCpp.predictedLongitudes[12] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[13], longitude: navigationDisplayCpp.predictedLongitudes[13] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[14], longitude: navigationDisplayCpp.predictedLongitudes[14] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[15], longitude: navigationDisplayCpp.predictedLongitudes[15] }
                      ]
                  }
                  else if (navigationDisplayCpp.mapRangeMode == 2) {
                      [
                      { latitude: navigationDisplayCpp.predictedLatitudes[0], longitude: navigationDisplayCpp.predictedLongitudes[0] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[1], longitude: navigationDisplayCpp.predictedLongitudes[1] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[2], longitude: navigationDisplayCpp.predictedLongitudes[2] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[3], longitude: navigationDisplayCpp.predictedLongitudes[3] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[4], longitude: navigationDisplayCpp.predictedLongitudes[4] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[5], longitude: navigationDisplayCpp.predictedLongitudes[5] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[6], longitude: navigationDisplayCpp.predictedLongitudes[6] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[7], longitude: navigationDisplayCpp.predictedLongitudes[7] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[8], longitude: navigationDisplayCpp.predictedLongitudes[8] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[9], longitude: navigationDisplayCpp.predictedLongitudes[9] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[10], longitude: navigationDisplayCpp.predictedLongitudes[10] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[11], longitude: navigationDisplayCpp.predictedLongitudes[11] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[12], longitude: navigationDisplayCpp.predictedLongitudes[12] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[13], longitude: navigationDisplayCpp.predictedLongitudes[13] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[14], longitude: navigationDisplayCpp.predictedLongitudes[14] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[15], longitude: navigationDisplayCpp.predictedLongitudes[15] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[16], longitude: navigationDisplayCpp.predictedLongitudes[16] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[17], longitude: navigationDisplayCpp.predictedLongitudes[17] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[18], longitude: navigationDisplayCpp.predictedLongitudes[18] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[19], longitude: navigationDisplayCpp.predictedLongitudes[19] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[20], longitude: navigationDisplayCpp.predictedLongitudes[20] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[21], longitude: navigationDisplayCpp.predictedLongitudes[21] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[22], longitude: navigationDisplayCpp.predictedLongitudes[22] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[23], longitude: navigationDisplayCpp.predictedLongitudes[23] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[24], longitude: navigationDisplayCpp.predictedLongitudes[24] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[25], longitude: navigationDisplayCpp.predictedLongitudes[25] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[26], longitude: navigationDisplayCpp.predictedLongitudes[26] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[27], longitude: navigationDisplayCpp.predictedLongitudes[27] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[28], longitude: navigationDisplayCpp.predictedLongitudes[28] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[29], longitude: navigationDisplayCpp.predictedLongitudes[29] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[30], longitude: navigationDisplayCpp.predictedLongitudes[30] }
                      ]
                  }
                  else if (navigationDisplayCpp.mapRangeMode == 3) {
                      [
                      { latitude: navigationDisplayCpp.predictedLatitudes[0], longitude: navigationDisplayCpp.predictedLongitudes[0] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[1], longitude: navigationDisplayCpp.predictedLongitudes[1] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[2], longitude: navigationDisplayCpp.predictedLongitudes[2] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[3], longitude: navigationDisplayCpp.predictedLongitudes[3] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[4], longitude: navigationDisplayCpp.predictedLongitudes[4] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[5], longitude: navigationDisplayCpp.predictedLongitudes[5] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[6], longitude: navigationDisplayCpp.predictedLongitudes[6] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[7], longitude: navigationDisplayCpp.predictedLongitudes[7] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[8], longitude: navigationDisplayCpp.predictedLongitudes[8] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[9], longitude: navigationDisplayCpp.predictedLongitudes[9] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[10], longitude: navigationDisplayCpp.predictedLongitudes[10] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[11], longitude: navigationDisplayCpp.predictedLongitudes[11] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[12], longitude: navigationDisplayCpp.predictedLongitudes[12] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[13], longitude: navigationDisplayCpp.predictedLongitudes[13] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[14], longitude: navigationDisplayCpp.predictedLongitudes[14] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[15], longitude: navigationDisplayCpp.predictedLongitudes[15] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[16], longitude: navigationDisplayCpp.predictedLongitudes[16] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[17], longitude: navigationDisplayCpp.predictedLongitudes[17] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[18], longitude: navigationDisplayCpp.predictedLongitudes[18] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[19], longitude: navigationDisplayCpp.predictedLongitudes[19] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[20], longitude: navigationDisplayCpp.predictedLongitudes[20] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[21], longitude: navigationDisplayCpp.predictedLongitudes[21] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[22], longitude: navigationDisplayCpp.predictedLongitudes[22] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[23], longitude: navigationDisplayCpp.predictedLongitudes[23] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[24], longitude: navigationDisplayCpp.predictedLongitudes[24] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[25], longitude: navigationDisplayCpp.predictedLongitudes[25] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[26], longitude: navigationDisplayCpp.predictedLongitudes[26] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[27], longitude: navigationDisplayCpp.predictedLongitudes[27] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[28], longitude: navigationDisplayCpp.predictedLongitudes[28] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[29], longitude: navigationDisplayCpp.predictedLongitudes[29] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[30], longitude: navigationDisplayCpp.predictedLongitudes[30] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[31], longitude: navigationDisplayCpp.predictedLongitudes[31] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[32], longitude: navigationDisplayCpp.predictedLongitudes[32] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[33], longitude: navigationDisplayCpp.predictedLongitudes[33] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[34], longitude: navigationDisplayCpp.predictedLongitudes[34] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[35], longitude: navigationDisplayCpp.predictedLongitudes[35] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[36], longitude: navigationDisplayCpp.predictedLongitudes[36] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[37], longitude: navigationDisplayCpp.predictedLongitudes[37] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[38], longitude: navigationDisplayCpp.predictedLongitudes[38] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[39], longitude: navigationDisplayCpp.predictedLongitudes[39] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[40], longitude: navigationDisplayCpp.predictedLongitudes[40] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[41], longitude: navigationDisplayCpp.predictedLongitudes[41] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[42], longitude: navigationDisplayCpp.predictedLongitudes[42] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[43], longitude: navigationDisplayCpp.predictedLongitudes[43] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[44], longitude: navigationDisplayCpp.predictedLongitudes[44] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[45], longitude: navigationDisplayCpp.predictedLongitudes[45] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[46], longitude: navigationDisplayCpp.predictedLongitudes[46] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[47], longitude: navigationDisplayCpp.predictedLongitudes[47] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[48], longitude: navigationDisplayCpp.predictedLongitudes[48] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[49], longitude: navigationDisplayCpp.predictedLongitudes[49] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[50], longitude: navigationDisplayCpp.predictedLongitudes[50] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[51], longitude: navigationDisplayCpp.predictedLongitudes[51] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[52], longitude: navigationDisplayCpp.predictedLongitudes[52] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[53], longitude: navigationDisplayCpp.predictedLongitudes[53] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[54], longitude: navigationDisplayCpp.predictedLongitudes[54] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[55], longitude: navigationDisplayCpp.predictedLongitudes[55] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[56], longitude: navigationDisplayCpp.predictedLongitudes[56] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[57], longitude: navigationDisplayCpp.predictedLongitudes[57] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[58], longitude: navigationDisplayCpp.predictedLongitudes[58] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[59], longitude: navigationDisplayCpp.predictedLongitudes[59] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[60], longitude: navigationDisplayCpp.predictedLongitudes[60] }
                      ]
                  }
                  else if (navigationDisplayCpp.mapRangeMode == 4) {
                      [
                      { latitude: navigationDisplayCpp.predictedLatitudes[0], longitude: navigationDisplayCpp.predictedLongitudes[0] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[1], longitude: navigationDisplayCpp.predictedLongitudes[1] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[2], longitude: navigationDisplayCpp.predictedLongitudes[2] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[3], longitude: navigationDisplayCpp.predictedLongitudes[3] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[4], longitude: navigationDisplayCpp.predictedLongitudes[4] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[5], longitude: navigationDisplayCpp.predictedLongitudes[5] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[6], longitude: navigationDisplayCpp.predictedLongitudes[6] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[7], longitude: navigationDisplayCpp.predictedLongitudes[7] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[8], longitude: navigationDisplayCpp.predictedLongitudes[8] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[9], longitude: navigationDisplayCpp.predictedLongitudes[9] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[10], longitude: navigationDisplayCpp.predictedLongitudes[10] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[11], longitude: navigationDisplayCpp.predictedLongitudes[11] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[12], longitude: navigationDisplayCpp.predictedLongitudes[12] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[13], longitude: navigationDisplayCpp.predictedLongitudes[13] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[14], longitude: navigationDisplayCpp.predictedLongitudes[14] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[15], longitude: navigationDisplayCpp.predictedLongitudes[15] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[16], longitude: navigationDisplayCpp.predictedLongitudes[16] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[17], longitude: navigationDisplayCpp.predictedLongitudes[17] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[18], longitude: navigationDisplayCpp.predictedLongitudes[18] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[19], longitude: navigationDisplayCpp.predictedLongitudes[19] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[20], longitude: navigationDisplayCpp.predictedLongitudes[20] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[21], longitude: navigationDisplayCpp.predictedLongitudes[21] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[22], longitude: navigationDisplayCpp.predictedLongitudes[22] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[23], longitude: navigationDisplayCpp.predictedLongitudes[23] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[24], longitude: navigationDisplayCpp.predictedLongitudes[24] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[25], longitude: navigationDisplayCpp.predictedLongitudes[25] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[26], longitude: navigationDisplayCpp.predictedLongitudes[26] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[27], longitude: navigationDisplayCpp.predictedLongitudes[27] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[28], longitude: navigationDisplayCpp.predictedLongitudes[28] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[29], longitude: navigationDisplayCpp.predictedLongitudes[29] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[30], longitude: navigationDisplayCpp.predictedLongitudes[30] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[31], longitude: navigationDisplayCpp.predictedLongitudes[31] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[32], longitude: navigationDisplayCpp.predictedLongitudes[32] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[33], longitude: navigationDisplayCpp.predictedLongitudes[33] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[34], longitude: navigationDisplayCpp.predictedLongitudes[34] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[35], longitude: navigationDisplayCpp.predictedLongitudes[35] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[36], longitude: navigationDisplayCpp.predictedLongitudes[36] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[37], longitude: navigationDisplayCpp.predictedLongitudes[37] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[38], longitude: navigationDisplayCpp.predictedLongitudes[38] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[39], longitude: navigationDisplayCpp.predictedLongitudes[39] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[40], longitude: navigationDisplayCpp.predictedLongitudes[40] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[41], longitude: navigationDisplayCpp.predictedLongitudes[41] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[42], longitude: navigationDisplayCpp.predictedLongitudes[42] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[43], longitude: navigationDisplayCpp.predictedLongitudes[43] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[44], longitude: navigationDisplayCpp.predictedLongitudes[44] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[45], longitude: navigationDisplayCpp.predictedLongitudes[45] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[46], longitude: navigationDisplayCpp.predictedLongitudes[46] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[47], longitude: navigationDisplayCpp.predictedLongitudes[47] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[48], longitude: navigationDisplayCpp.predictedLongitudes[48] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[49], longitude: navigationDisplayCpp.predictedLongitudes[49] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[50], longitude: navigationDisplayCpp.predictedLongitudes[50] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[51], longitude: navigationDisplayCpp.predictedLongitudes[51] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[52], longitude: navigationDisplayCpp.predictedLongitudes[52] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[53], longitude: navigationDisplayCpp.predictedLongitudes[53] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[54], longitude: navigationDisplayCpp.predictedLongitudes[54] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[55], longitude: navigationDisplayCpp.predictedLongitudes[55] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[56], longitude: navigationDisplayCpp.predictedLongitudes[56] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[57], longitude: navigationDisplayCpp.predictedLongitudes[57] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[58], longitude: navigationDisplayCpp.predictedLongitudes[58] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[59], longitude: navigationDisplayCpp.predictedLongitudes[59] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[60], longitude: navigationDisplayCpp.predictedLongitudes[60] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[61], longitude: navigationDisplayCpp.predictedLongitudes[61] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[62], longitude: navigationDisplayCpp.predictedLongitudes[62] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[63], longitude: navigationDisplayCpp.predictedLongitudes[63] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[64], longitude: navigationDisplayCpp.predictedLongitudes[64] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[65], longitude: navigationDisplayCpp.predictedLongitudes[65] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[66], longitude: navigationDisplayCpp.predictedLongitudes[66] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[67], longitude: navigationDisplayCpp.predictedLongitudes[67] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[68], longitude: navigationDisplayCpp.predictedLongitudes[68] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[69], longitude: navigationDisplayCpp.predictedLongitudes[69] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[70], longitude: navigationDisplayCpp.predictedLongitudes[70] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[71], longitude: navigationDisplayCpp.predictedLongitudes[71] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[72], longitude: navigationDisplayCpp.predictedLongitudes[72] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[73], longitude: navigationDisplayCpp.predictedLongitudes[73] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[74], longitude: navigationDisplayCpp.predictedLongitudes[74] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[75], longitude: navigationDisplayCpp.predictedLongitudes[75] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[76], longitude: navigationDisplayCpp.predictedLongitudes[76] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[77], longitude: navigationDisplayCpp.predictedLongitudes[77] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[78], longitude: navigationDisplayCpp.predictedLongitudes[78] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[79], longitude: navigationDisplayCpp.predictedLongitudes[79] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[80], longitude: navigationDisplayCpp.predictedLongitudes[80] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[81], longitude: navigationDisplayCpp.predictedLongitudes[81] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[82], longitude: navigationDisplayCpp.predictedLongitudes[82] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[83], longitude: navigationDisplayCpp.predictedLongitudes[83] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[84], longitude: navigationDisplayCpp.predictedLongitudes[84] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[85], longitude: navigationDisplayCpp.predictedLongitudes[85] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[86], longitude: navigationDisplayCpp.predictedLongitudes[86] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[87], longitude: navigationDisplayCpp.predictedLongitudes[87] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[88], longitude: navigationDisplayCpp.predictedLongitudes[88] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[89], longitude: navigationDisplayCpp.predictedLongitudes[89] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[90], longitude: navigationDisplayCpp.predictedLongitudes[90] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[91], longitude: navigationDisplayCpp.predictedLongitudes[91] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[92], longitude: navigationDisplayCpp.predictedLongitudes[92] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[93], longitude: navigationDisplayCpp.predictedLongitudes[93] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[94], longitude: navigationDisplayCpp.predictedLongitudes[94] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[95], longitude: navigationDisplayCpp.predictedLongitudes[95] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[96], longitude: navigationDisplayCpp.predictedLongitudes[96] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[97], longitude: navigationDisplayCpp.predictedLongitudes[97] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[98], longitude: navigationDisplayCpp.predictedLongitudes[98] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[99], longitude: navigationDisplayCpp.predictedLongitudes[99] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[100], longitude: navigationDisplayCpp.predictedLongitudes[100] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[101], longitude: navigationDisplayCpp.predictedLongitudes[101] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[102], longitude: navigationDisplayCpp.predictedLongitudes[102] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[103], longitude: navigationDisplayCpp.predictedLongitudes[103] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[104], longitude: navigationDisplayCpp.predictedLongitudes[104] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[105], longitude: navigationDisplayCpp.predictedLongitudes[105] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[106], longitude: navigationDisplayCpp.predictedLongitudes[106] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[107], longitude: navigationDisplayCpp.predictedLongitudes[107] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[108], longitude: navigationDisplayCpp.predictedLongitudes[108] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[109], longitude: navigationDisplayCpp.predictedLongitudes[109] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[110], longitude: navigationDisplayCpp.predictedLongitudes[110] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[111], longitude: navigationDisplayCpp.predictedLongitudes[111] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[112], longitude: navigationDisplayCpp.predictedLongitudes[112] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[113], longitude: navigationDisplayCpp.predictedLongitudes[113] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[114], longitude: navigationDisplayCpp.predictedLongitudes[114] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[115], longitude: navigationDisplayCpp.predictedLongitudes[115] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[116], longitude: navigationDisplayCpp.predictedLongitudes[116] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[117], longitude: navigationDisplayCpp.predictedLongitudes[117] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[118], longitude: navigationDisplayCpp.predictedLongitudes[118] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[119], longitude: navigationDisplayCpp.predictedLongitudes[119] },
                      { latitude: navigationDisplayCpp.predictedLatitudes[120], longitude: navigationDisplayCpp.predictedLongitudes[120] }
                      ]
                  }
        }
    }

    Rectangle {
        id: aircraftIcon
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        color: "blue"
        width: 0.03 * parent.width
        height: 0.03 * parent.width
        radius: width*0.5
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.9}
}
##^##*/
