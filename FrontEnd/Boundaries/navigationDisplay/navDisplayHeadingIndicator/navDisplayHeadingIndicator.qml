import QtQuick 2.12
import gcs.display.NavigationDisplay 1.0


Rectangle {
    id: navDisplayHeadingIndicator
    color: "transparent"
    anchors.fill: parent

    Text {
        id: navDisplayHeadingIndicatorTitleText
        color: "#2B2B2B"
        font.pointSize: 12
        anchors.left: parent.left
        anchors.top: parent.top
        text: qsTr("HDG")
        font.bold: true
    }

    Rectangle {
        id: navDisplayHeadingIndicatorGaugeValueBox
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        width: 0.25 * parent.width
        height: 0.3 * parent.height
        color: "#2B2B2B"
        border.color: "#c5c2b2"
        border.width: 3

        Text {
            id: navDisplayHeadingIndicatorGaugeValueText
            anchors.centerIn: parent
            font.pointSize: 12
            property var headingVar: if (navigationDisplayCpp.heading < 0) {
                                         return Math.round((180 + (180 + navigationDisplayCpp.heading)) * 10) / 10
                                     }
                                     else {
                                         return Math.round(navigationDisplayCpp.heading * 10) / 10
                                     }
            text: headingVar + qsTr("°")
            color: "lightgreen"
        }
    }

    Text {
        id: navDisplayHeadingIndicatorModeText
        color: "#2B2B2B"
        font.pointSize: 12
        anchors.right: parent.right
        anchors.top: parent.top
        text: qsTr("MAG")
        font.bold: true
    }

    Rectangle {
        id: navDisplayHeadingIndicatorGauge
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: navDisplayHeadingIndicatorGaugeValueBox.bottom
        width: parent.width
        height: 0.7 * parent.height
        clip: true
        color: "transparent"
        Rectangle {
            id: navDisplayHeadingIndicatorGaugeIndicator
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: 6
            height: 0.5 * parent.height
            color: "#2B2B2B"
            border.color: "#c5c2b2"
            border.width: 2
            z: 3
        }
        Row{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: 20
            Repeater {
                model: ["80", "", "90", "", "100", "",
                    "110", "", "120", "", "130", "", "140", "", "150", "", "160", "", "170", "", "180", "", "190",
                    "", "200", "", "210", "", "220", "", "230", "", "240", "", "250", "", "260", "", "270", "", "280",
                    "", "290",  "", "300",  "", "310",  "", "320",  "", "330",  "", "340",  "", "350", "", "0", "",
                    "10", "", "20", "", "30", "", "40", "", "50", "", "60", "", "70", "", "80", "", "90", "", "100", "",
                    "110", "", "120", "", "130", "", "140", "", "150", "", "160", "", "170", "", "180", "", "190",
                    "", "200", "", "210", "", "220", "", "230", "", "240", "", "250", "", "260", "", "270", "", "280"]
                Rectangle {
                    id: navDisplayHeadingIndicatorGaugeTickmarks
                    width: 3
                    height: 0.2 * navDisplayHeadingIndicatorGauge.height
                    y: - 1.9 * parent.height
                    color: "#2B2B2B"

                    Text {
                        id: navDisplayHeadingIndicatorGaugeTickmarkLabels
                        y:  2.2 *  parent.height
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: 11
                        text: modelData
                        font.bold: true
                        color: "#2B2B2B"
                    }
                }
            }

            transform: Translate {
                x: - navigationDisplayCpp.heading * ((20 + 2) * 2) / 10
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.9}
}
##^##*/
