import QtQuick 2.12
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import gcs.display.NavigationDisplay 1.0


Rectangle {
    id: rangePanel
    anchors.fill: parent
    border.width: 2
    border.color: "#c5c2b2"
    color: "#2B2B2B"

    Rectangle {
        id: decreaseRangeBtnBox
        anchors.top: parent.top
        anchors.left: parent.left
        width: 0.15 * parent.width
        height: parent.height
        color: "transparent"

        Button {
            id: decreaseRangeBtn
            anchors.fill: parent
            contentItem: Text {
                id: decreaseRangeBtnText
                text: qsTr("-")
                color: "#c5c2b2"
                font.pointSize: 12
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            onClicked: navigationDisplayCpp.decreaseRange()
            background: Rectangle {
                anchors.top: parent.top
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                border.width: 2
                border.color: "#c5c2b2"
                color: "transparent"
            }
        }
    }

    Rectangle {
        id: rangeTextBox
        width: 0.6 * parent.width
        height: parent.height
        anchors.left: decreaseRangeBtnBox.right
        anchors.top: parent.top
        anchors.leftMargin: 0.05 * parent.width
        color: "transparent"

        Text {
            id: rangeText
            anchors.centerIn: parent
            property var rangeValue: if (navigationDisplayCpp.mapRangeMode == 1) {
                                         "100"
                                     }
                                     else if (navigationDisplayCpp.mapRangeMode == 2) {
                                         "200"
                                     }
                                     else if (navigationDisplayCpp.mapRangeMode == 3) {
                                         "400"
                                     }
                                     else if (navigationDisplayCpp.mapRangeMode == 4) {
                                         "800"
                                     }
                                     else if (navigationDisplayCpp.mapRangeMode == 5) {
                                         "1600"
                                     }
                                     else if (navigationDisplayCpp.mapRangeMode == 6) {
                                         "3200"
                                     }
                                     else if (navigationDisplayCpp.mapRangeMode == 7) {
                                         "6400"
                                     }
                                     else if (navigationDisplayCpp.mapRangeMode == 8) {
                                         "12800"
                                     }
            text: qsTr("Range: ") + rangeValue + qsTr(" (M)")
            color: "#c5c2b2"
            font.pointSize: 12
        }
    }

    Rectangle {
        id: increaseRangeBtnBox
        anchors.top: parent.top
        anchors.left: rangeTextBox.right
        width: 0.15 * parent.width
        height: parent.height
        anchors.leftMargin: 0.05 * parent.width
        color: "transparent"

        Button {
            id: increaseRangeBtn
            anchors.fill: parent
            contentItem: Text {
                id: increaseRangeBtnText
                text: qsTr("+")
                color: "#c5c2b2"
                font.pointSize: 12
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            onClicked: navigationDisplayCpp.increaseRange()
            background: Rectangle {
                anchors.top: parent.top
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                border.width: 2
                border.color: "#c5c2b2"
                color: "transparent"
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.9}
}
##^##*/
