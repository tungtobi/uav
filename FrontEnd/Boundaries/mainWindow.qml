import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12

Window {
    id: flightMonitorWindow
    width: 1440
    height: 772
    minimumWidth: 1440
    minimumHeight: 772
    visible: true
    title: qsTr("UAV System Ground Monitoring Software")
    Rectangle {
        anchors.fill: parent
        Loader {
            id: connectionDisplay
            source: "./connectionDisplay/connectionDisplay.qml"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: 0.066653 * parent.height
            width: parent.width
        }

        Rectangle {
            id: connectionDisplayBottomBorder
            anchors.top: connectionDisplay.bottom
            anchors.left: parent.left
            height: 3
            width: parent.width
            color: "#c5c2b2"
        }

        Loader {
            id: pfd
            source: "./primaryFlightDisplay/primaryFlightDisplay.qml"
            anchors.top: connectionDisplayBottomBorder.bottom
            anchors.left: parent.left
            height: 0.933347 * parent.height
            width: (1 / 3) * parent.width
        }

        Rectangle {
            id: pfdAndNdSeperator
            anchors.top: connectionDisplayBottomBorder.bottom
            anchors.left: pfd.right
            height: 0.933347 * parent.height
            width: 3
            color: "#c5c2b2"
        }

        Loader {
            id: nd
            source: "./navigationDisplay/navigationDisplay.qml"
            anchors.top: connectionDisplayBottomBorder.bottom
            anchors.left: pfdAndNdSeperator.right
            height: 0.933347 * parent.height
            width: (1 / 3) * parent.width
        }

        Rectangle {
            id: ndAndSdSeperator
            anchors.top: connectionDisplayBottomBorder.bottom
            anchors.left: nd.right
            height: 0.933347 * parent.height
            width: 3
            color: "#c5c2b2"
        }

        Loader {
            id: sd
            source: "./systemDisplay/systemDisplay.qml"
            anchors.top: connectionDisplayBottomBorder.bottom
            anchors.left: ndAndSdSeperator.right
            height: 0.933347 * parent.height
            width: (1 / 3) * parent.width
        }
    }
}
