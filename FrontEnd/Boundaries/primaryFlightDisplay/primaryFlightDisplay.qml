import QtQuick 2.12
import QtQuick.Layouts 1.3
import gcs.display.PrimaryFlightDisplay 1.0

Rectangle {
    id: pfd
    anchors.fill: parent
    PrimaryFlightDisplay {
        id: pfdCpp
    }
    Rectangle {
        id: primaryInformationsTopBorder
        anchors.top: parent.top
        anchors.left: parent.left
        height: 3
        width: parent.width
        color: "#545454"
    }
    Rectangle {
        id: primaryInformationTitleBox
        anchors.top: primaryInformationsTopBorder.bottom
        anchors.left: parent.left
        width: parent.width
        height: 0.0443 * parent.height
        color: "#2B2B2B"
        Text {
            id: primaryInformationTitle
            text: qsTr("Primary Flight Information")
            color: "#c5c2b2"
            font.pointSize: 20
            font.bold: true
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
    Loader {
        id: primaryInformation
        anchors.top: primaryInformationTitleBox.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        height: 0.545 * parent.height
        source: "./primaryInformation/primaryInformation.qml"
    }
    Rectangle {
        id: controlStatusTopBorder
        anchors.top: primaryInformation.bottom
        anchors.left: parent.left
        height: 3
        width: parent.width
        color: "#c5c2b2"
    }
    Rectangle {
        id: controlStatusTitleBox
        anchors.top: controlStatusTopBorder.bottom
        anchors.left: parent.left
        width: parent.width
        height: 0.0536 * parent.height
        color: "#2B2B2B"
        Text {
            id: controlStatusitle
            text: qsTr("Control Status")
            color: "#c5c2b2"
            font.pointSize: 20
            font.bold: true
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
    Loader {
        id: controlStatus
        anchors.top: controlStatusTitleBox.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        height: 0.3571 * parent.height
        source: "./controlStatus/controlStatus.qml"
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:720;width:480}
}
##^##*/
