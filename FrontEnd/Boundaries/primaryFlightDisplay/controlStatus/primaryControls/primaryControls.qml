import QtQuick 2.12

Rectangle {
    id: primaryControls
    anchors.fill: parent
    color: "blue"
    Loader {
        id: aileron
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: 0.2 * parent.height
        source: "./aileron/aileron.qml"
    }
    Loader {
        id: elevator
        anchors.top: aileron.bottom
        anchors.left: parent.left
        width: parent.width
        height: 0.6 * parent.height
        source: "./elevator/elevator.qml"
    }
    Loader {
        id: rudder
        anchors.top: elevator.bottom
        anchors.left: parent.left
        width: parent.width
        height: 0.2 * parent.height
        source: "./rudder/rudder.qml"
    }

}
