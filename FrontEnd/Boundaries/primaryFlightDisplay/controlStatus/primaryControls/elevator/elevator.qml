import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: elevator
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: elevatorTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        text: qsTr("Elevator: ") + qsTr("\n") + qsTr("(Pitch)")
    }

    Text {
        id: elevatorValueText
        color: "lightgreen"
        font.pointSize: 12
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: elevatorUnitText.left
        property var elevatorVar: Math.round(pfdCpp.controlSurfaces[1] * 100) / 100;
        text: elevatorVar
    }

    Text {
        id: elevatorUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        text: qsTr(" %")
    }

    Gauge {
        id: elevatorGauge
        orientation: Qt.Vertical
        tickmarkAlignment: Qt.AlignRight
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: 0.06 * parent.width
        height: parent.height
        maximumValue: 100
        minimumValue: -100
        tickmarkStepSize: 25
//      value: -30
        value: pfdCpp.controlSurfaces[1]
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: elevatorGauge.width
                Rectangle {
                    color: "lightgreen"
                    width: parent.width
                    height: 6
                }
            }

            background: Rectangle {
                color: "#545454"
            }

            tickmark: Item {
                implicitWidth: 1
                implicitHeight: 1
                Rectangle {
                    x: - elevatorGauge.width
                    height: parent.implicitWidth
                    width: elevatorGauge.width
                    color: "#c5c2b2"
                }
            }

            tickmarkLabel: Text {
                text: control.formatValue(styleData.value)
                font.pointSize: 10
                color: "#c5c2b2"
            }

            minorTickmark: null
        }
    }

}
