import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: rudder
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: rudderTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.top: parent.top
        anchors.left: parent.left
        text: qsTr("Rudder (Yaw): ")
    }

    Text {
        id: rudderValueText
        color: "lightgreen"
        font.pointSize: 12
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        property var rudderVar: Math.round(pfdCpp.controlSurfaces[2] * 100) / 100;
        text: rudderVar
    }

    Text {
        id: rudderUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.top: parent.top
        anchors.right: parent.right
        text: qsTr(" %")
    }

    Gauge {
        id: rudderGauge
        orientation: Qt.Horizontal
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: rudderValueText.bottom
        width: parent.width
        height: 0.26 * parent.height
        maximumValue: 100
        minimumValue: -100
        tickmarkStepSize: 20
//      value: -30
        value: pfdCpp.controlSurfaces[2]
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: rudderGauge.height
                Rectangle {
                    color: "lightgreen"
                    width: parent.width
                    height: 6
                }
            }

            background: Rectangle {
                color: "#545454"
            }

            tickmark: Item {
                implicitWidth: 1
                implicitHeight: 1
                Rectangle {
                    x: - rudderGauge.height
                    height: parent.implicitHeight
                    width: rudderGauge.height
                    color: "#c5c2b2"
                }
            }

            tickmarkLabel: Text {
                text: control.formatValue(styleData.value)
                font.pointSize: 10
                color: "#c5c2b2"
            }

            minorTickmark: null
        }
    }

}
