import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: aileron
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: aileronTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.top: parent.top
        anchors.left: parent.left
        text: qsTr("Aileron (Roll): ")
    }

    Text {
        id: aileronValueText
        color: "lightgreen"
        font.pointSize: 12
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        property var aileronVar: Math.round(pfdCpp.controlSurfaces[0] * 100) / 100;
        text: aileronVar
    }

    Text {
        id: aileronUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.top: parent.top
        anchors.right: parent.right
        text: qsTr(" %")
    }

    Gauge {
        id: aileronGauge
        orientation: Qt.Horizontal
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: aileronValueText.bottom
        width: parent.width
        height: 0.26 * parent.height
        maximumValue: 100
        minimumValue: -100
        tickmarkStepSize: 20
//      value: -30
        value: pfdCpp.controlSurfaces[0]
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: aileronGauge.height
                Rectangle {
                    color: "lightgreen"
                    width: parent.width
                    height: 6
                }
            }

            background: Rectangle {
                color: "#545454"
            }

            tickmark: Item {
                implicitWidth: 1
                implicitHeight: 1
                Rectangle {
                    x: - aileronGauge.height
                    height: parent.implicitHeight
                    width: aileronGauge.height
                    color: "#c5c2b2"
                }
            }

            tickmarkLabel: Text {
                text: control.formatValue(styleData.value)
                font.pointSize: 10
                color: "#c5c2b2"
            }

            minorTickmark: null
        }
    }
}
