import QtQuick 2.12
import QtQuick.Layouts 1.3


Rectangle {
    id: controlStatus
    color: "#2B2B2B"
    anchors.fill: parent
    Loader {
        id: power
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 0.025 * parent.width
        anchors.topMargin: 0.0125 * parent.height
        anchors.bottomMargin: 0.0125 * parent.height
        width: 0.185 * parent.width
        height: 0.95 * parent.height
        source: "./power/power.qml"
    }

    Rectangle {
        id: powerAndPrimaryControlsBorder
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: power.right
        anchors.leftMargin: 6
        width: 3
        height: parent.height
        color: "#c5c2b2"
    }

    Loader {
        id: primaryControls
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.leftMargin: 0.025 * parent.width
        anchors.topMargin: 0.0125 * parent.height
        anchors.bottomMargin: 0.0125 * parent.height
        width: 0.53 * parent.width
        height: 0.95 * parent.height
        source: "./primaryControls/primaryControls.qml"

    }

    Rectangle {
        id: primaryControlsAndFlapsBorder
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: flaps.left
        anchors.rightMargin: 6
        width: 3
        height: parent.height
        color: "#c5c2b2"
    }

    Loader {
        id: flaps
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.topMargin: 0.0125 * parent.height
        width: 0.185 * parent.width
        height: 0.95 * parent.height
        source: "./flaps/flaps.qml"

    }
}
