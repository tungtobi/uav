import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: power
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: powerTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        text: qsTr("E") + qsTr("\n") + qsTr("n") + qsTr("\n") + qsTr("g.") + qsTr("\n")
              + qsTr("\n") + qsTr("P") + qsTr("\n") + qsTr("o") + qsTr("\n") + qsTr("w")
              + qsTr("\n") + qsTr("e") + qsTr("\n") + qsTr("r") + qsTr("\n")  + qsTr("%")
    }

    Text {
        id: powerTargetLabelText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.top: parent.top
        anchors.right: parent.right
        text: qsTr("Tar.")
    }

    Text {
        id: powerTargetText
        color: "#a3c1ff"
        font.pointSize: 12
        anchors.top: powerTargetLabelText.bottom
        anchors.right: parent.right
        property var powerTargetVar: Math.round(pfdCpp.controlSurfaces[3]);
        text: powerTargetVar
    }

    Text {
        id: powerTargetUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.top: powerTargetText.bottom
        anchors.right: parent.right
        text: qsTr("%")
    }

    Text {
        id: powerStatusLabelText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.bottom: powerStatusText.top
        anchors.right: parent.right
        text: qsTr("Act.")
    }

    Text {
        id: powerStatusText
        color: "lightgreen"
        font.pointSize: 12
        anchors.bottom: powerStatusUnitText.top
        anchors.right: parent.right
        property var powerStatusVar: Math.round(pfdCpp.powerPercent);
        text: powerStatusVar
    }

    Text {
        id: powerStatusUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        text: qsTr("%")
    }

    Gauge {
        id: powerStatusGauge
        orientation: Qt.Vertical
        tickmarkAlignment: Qt.AlignLeft
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: 0.15 * parent.width
        height: parent.height
        maximumValue: 100
        minimumValue: 0
        tickmarkStepSize: 20
        anchors.horizontalCenterOffset: - 0.3 * parent.width
//      value: 30
        value: pfdCpp.powerPercent
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: powerStatusGauge.width
                Rectangle {
                    color: "lightgreen"
                    width: parent.width
                  height: {
                      var maxSpan = Math.abs(powerStatusGauge.maximumValue) + Math.abs(powerStatusGauge.minimumValue)
                      var fill = Math.abs(powerStatusGauge.value)
                      var fillPercent = fill/maxSpan
                      var fillHeight = powerStatusGauge.height * fillPercent
                      return fillHeight
                  }
                }
            }

            background: Rectangle {
                color: "#545454"
            }

            tickmark: Item {
                implicitWidth: 1
                implicitHeight: 1
                Rectangle {
                    x: parent.implicitWidth
                    height: parent.implicitWidth
                    width: powerStatusGauge.width
                    color: "#c5c2b2"
                }
            }

            tickmarkLabel: Text {
                text: control.formatValue(styleData.value)
                font.pointSize: 10
                color: "#c5c2b2"
            }

            minorTickmark: null
        }
    }

    Gauge {
        id: powerTargetGauge
        orientation: Qt.Vertical
        anchors.left: powerStatusGauge.right
        anchors.verticalCenter: parent.verticalCenter
        width: 0
        height: parent.height
        maximumValue: 100
        minimumValue: 0
        tickmarkStepSize: 20
        anchors.horizontalCenterOffset: - 0.3 * parent.width
//      value: 100
        value: pfdCpp.controlSurfaces[3]
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: powerTargetGauge.width
                Rectangle {
                    x: - parent.width - powerStatusGauge.width
                    color: "#a3c1ff"
                    width: parent.width + 2 * powerStatusGauge.width
                    height: 6
                }
            }

            background: Rectangle {
                color: "#2B2B2B"
            }

            tickmark: null

            tickmarkLabel: null

            minorTickmark: null
        }
    }

}
