import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: flaps
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: flapsTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        text: qsTr("F") + qsTr("\n") + qsTr("l") + qsTr("\n") + qsTr("a") + qsTr("\n")
              + qsTr("p") + qsTr("\n") + qsTr("s")
    }

    Text {
        id: flapsValueText
        color: "lightgreen"
        font.pointSize: 12
        anchors.bottom: flapsUnitText.top
        anchors.left: parent.left
        property var flapsVar: Math.round(pfdCpp.controlSurfaces[4] * 100) / 100;
        text: flapsVar
    }

    Text {
        id: flapsUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        text: qsTr("%")
    }

    Gauge {
        id: flapsGauge
        orientation: Qt.Vertical
        tickmarkAlignment: Qt.AlignRight
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: 0.15 * parent.width
        height: parent.height
        maximumValue: 0
        minimumValue: - 100
        tickmarkStepSize: 20
        value: - pfdCpp.controlSurfaces[4]
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: flapsGauge.width
                Rectangle {
                    color: "#545454"
                    width: parent.width
                  height: {
                      var maxSpan = Math.abs(flapsGauge.maximumValue) + Math.abs(flapsGauge.minimumValue)
                      var fill = Math.abs(flapsGauge.value)
                      var fillPercent = fill/maxSpan
                      var fillHeight = flapsGauge.height * fillPercent
                      return (maxSpan * flapsGauge.height  - fillHeight)
                  }
                }
            }

            background: Rectangle {
                color: "lightgreen"
            }

            tickmark: Item {
                implicitWidth: 1
                implicitHeight: 1
                Rectangle {
                    x: - flapsGauge.width
                    height: parent.implicitWidth
                    width: flapsGauge.width
                    color: "#c5c2b2"
                }
            }

            tickmarkLabel: Text {
                text: - control.formatValue(styleData.value)
                font.pointSize: 11
                color: "#c5c2b2"
            }

            minorTickmark: null
        }
    }

}
