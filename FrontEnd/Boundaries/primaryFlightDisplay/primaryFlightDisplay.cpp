#include "./FrontEnd/Boundaries/primaryFlightDisplay/primaryFlightDisplay.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>


//Private functions

PrimaryFlightDisplay::PrimaryFlightDisplay(QObject *parent) : QObject(parent)
{
    controller = new PrimaryFlightDisplayCtrlr(this);
    QThreadPool::globalInstance()->start(controller, 2);
}


//Implementation of PrimaryFlightDisplay Display's public methods
void PrimaryFlightDisplay::setControlSurfaces(const QList<qreal> &ctrlSurfs) {
        controlSurfacesBuffer = ctrlSurfs;
    emit controlSurfacesChanged();
}

QList<qreal> PrimaryFlightDisplay::controlSurfaces() {
    return controlSurfacesBuffer;
}

void PrimaryFlightDisplay::setRoll(const float &roll) {
    rollBuffer = roll;
    emit rollChanged();
}

float PrimaryFlightDisplay::roll() {
    return rollBuffer;
}

void PrimaryFlightDisplay::setPitch(const float &pitch) {
    pitchBuffer = pitch;
    emit pitchChanged();
}

float PrimaryFlightDisplay::pitch() {
    return pitchBuffer;
}

void PrimaryFlightDisplay::setHeading(const float &hdg) {
    headingBuffer = hdg;
    emit headingChanged();
}

float PrimaryFlightDisplay::heading() {
    return headingBuffer;
}

void PrimaryFlightDisplay::setTrack(const float &track) {
    trackBuffer = track;
    emit trackChanged();
}

float PrimaryFlightDisplay::track() {
    return trackBuffer;
}

void PrimaryFlightDisplay::setIAS(const float &indctAirSpeed) {
    IASBuffer = indctAirSpeed;
    emit IASChanged();
}

float PrimaryFlightDisplay::IAS() {
    return IASBuffer;
}

void PrimaryFlightDisplay::setIASAccel(const float &iasAccel) {
    IASAccelBuffer = iasAccel;
    emit IASAccelChanged();
}

float PrimaryFlightDisplay::IASAccel() {
    return IASAccelBuffer;
}

void PrimaryFlightDisplay::setGroundSpeed(const float &groundSpeed) {
    groundSpeedBuffer = groundSpeed;
    emit groundSpeedChanged();
}

float PrimaryFlightDisplay::groundSpeed() {
    return groundSpeedBuffer;
}

void PrimaryFlightDisplay::setGroundSpeedAccel(const float &gsAccel) {
    groundSpeedAccelBuffer = gsAccel;
    emit groundSpeedAccelChanged();
}

float PrimaryFlightDisplay::groundSpeedAccel() {
    return groundSpeedAccelBuffer;
}

void PrimaryFlightDisplay::setPowerPercent(const float &throttPercent) {
    powerPercentBuffer = throttPercent;
    emit powerPercentChanged();
}

float PrimaryFlightDisplay::powerPercent() {
    return powerPercentBuffer;
}

void PrimaryFlightDisplay::setVertSpeed(const float &vSpd) {
    vertSpeedBuffer = vSpd;
    emit vertSpeedChanged();
}

float PrimaryFlightDisplay::vertSpeed() {
    return vertSpeedBuffer;
}

void PrimaryFlightDisplay::setAltitude(const float &alt){
    altitudeBuffer = alt;
    emit altitudeChanged();
}

float PrimaryFlightDisplay::altitude() {
    return altitudeBuffer;
}
