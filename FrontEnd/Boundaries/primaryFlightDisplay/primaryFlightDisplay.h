#ifndef PRIMARYFLIGHTDISPLAY_H
#define PRIMARYFLIGHTDISPLAY_H

#include <QObject>
#include <QThreadPool>
#include <QList>
#include "./FrontEnd/Controllers/primaryFlightDisplayCtrlr/primaryFlightDisplayCtrlr.h"

class PrimaryFlightDisplay : public QObject
{
    Q_OBJECT
    //Properties for PFD Display
    Q_PROPERTY(QList<qreal> controlSurfaces READ controlSurfaces WRITE setControlSurfaces NOTIFY controlSurfacesChanged)
    Q_PROPERTY(float roll READ roll WRITE setRoll NOTIFY rollChanged)
    Q_PROPERTY(float pitch READ pitch WRITE setPitch NOTIFY pitchChanged)
    Q_PROPERTY(float heading READ heading WRITE setHeading NOTIFY headingChanged)
    Q_PROPERTY(float track READ track WRITE setTrack NOTIFY trackChanged)
    Q_PROPERTY(float powerPercent READ powerPercent WRITE setPowerPercent NOTIFY powerPercentChanged)
    Q_PROPERTY(float vertSpeed READ vertSpeed WRITE setVertSpeed NOTIFY vertSpeedChanged)
    Q_PROPERTY(float altitude READ altitude WRITE setAltitude NOTIFY altitudeChanged)
    Q_PROPERTY(float IAS READ IAS WRITE setIAS NOTIFY IASChanged)
    Q_PROPERTY(float IASAccel READ IASAccel WRITE setIASAccel NOTIFY IASAccelChanged)
    Q_PROPERTY(float groundSpeed READ groundSpeed WRITE setGroundSpeed NOTIFY groundSpeedChanged)
    Q_PROPERTY(float groundSpeedAccel READ groundSpeedAccel WRITE setGroundSpeedAccel NOTIFY groundSpeedAccelChanged)

public:
    explicit PrimaryFlightDisplay(QObject *parent = nullptr);

    //Public methods/variables for PFD Display
    QList<qreal> controlSurfaces();
    float roll();
    float pitch();
    float heading();
    float track();
    float powerPercent();
    float vertSpeed();
    float altitude();
    float IAS();
    float IASAccel();
    float groundSpeed();
    float groundSpeedAccel();

public slots:
    //Slots for PFD Display
    void setControlSurfaces(const QList<qreal> &ctrlSurfs);
    void setRoll(const float &roll);
    void setPitch(const float &pitch);
    void setHeading(const float &hdg);
    void setTrack(const float &track);
    void setPowerPercent(const float &throttPercent);
    void setVertSpeed(const float &vSpd);
    void setAltitude(const float &alt);
    void setIAS(const float &indctAirSpeed);
    void setIASAccel(const float &iasAccel);
    void setGroundSpeed(const float &groundSpeed);
    void setGroundSpeedAccel(const float &gsAccel);

signals:
    //Signals for PFD Display
    void controlSurfacesChanged();
    void rollChanged();
    void pitchChanged();
    void headingChanged();
    void trackChanged();
    void powerPercentChanged();
    void vertSpeedChanged();
    void altitudeChanged();
    void IASChanged();
    void IASAccelChanged();
    void groundSpeedChanged();
    void groundSpeedAccelChanged();

private:
    //Private internal buffer variables and methods
    PrimaryFlightDisplayCtrlr *controller;

    //Private internal buffer variables and methods for PFD Display
    QList<qreal> controlSurfacesBuffer;
    float rollBuffer;
    float pitchBuffer;
    float headingBuffer;
    float trackBuffer;
    float powerPercentBuffer;
    float vertSpeedBuffer;
    float altitudeBuffer;
    float IASBuffer;
    float IASAccelBuffer;
    float groundSpeedBuffer;
    float groundSpeedAccelBuffer;

};

#endif // PRIMARYFLIGHTDISPLAY_H
