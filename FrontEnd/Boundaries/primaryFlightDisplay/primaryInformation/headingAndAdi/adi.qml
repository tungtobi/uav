import QtQuick 2.12

Rectangle {
    id: adi
    anchors.fill: parent
    Rectangle {
        id: pitchStatusBox
        anchors.top: parent.top
        anchors.left: parent.left
        width: 0.5 * parent.width
        height: 0.06 * parent.height
        color: "#2B2B2B"
        Text {
            id: pitchTitleText
            color: "#c5c2b2"
            font.pointSize: 12
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 0.05 * parent.width
            text: qsTr("Pitch:")
        }

        Text {
            id: pitchValueText
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.rightMargin: 0.05 * parent.width
            color: "lightgreen"
            font.pointSize: 12
            property var pitchVar: Math.round(pfdCpp.pitch * 100) / 100;
            text: pitchVar + qsTr("°")
        }
    }

    Rectangle {
        id: rollStatusBox
        anchors.top: parent.top
        anchors.right: parent.right
        width: 0.5 * parent.width
        height: 0.06 * parent.height
        color: "#2B2B2B"
        Text {
            id: rollTitleText
            color: "#c5c2b2"
            font.pointSize: 12
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 0.05 * parent.width
            text: qsTr("Roll:")
        }

        Text {
            id: rollValueText
            anchors.top: parent.top
            anchors.right: parent.right
            color: "lightgreen"
            font.pointSize: 12
            anchors.rightMargin: 0.05 * parent.width
            property var rollVar: Math.round(pfdCpp.roll * 100) / 100;
            text: rollVar + qsTr("°")
        }
    }

    Rectangle {
        id: adiGauge
        anchors.top: pitchStatusBox.bottom
        anchors.left: parent.left
        width: parent.width
        height: 0.94 * parent.height
        antialiasing: true
        color: "transparent"
        Rectangle {
            id: adiCrosshair
            z: 5
            anchors.centerIn: parent
            width: 0.8 * parent.width
            height: 5
            color: "transparent"
            Rectangle {
                id: crossHairLeftMark
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 0.2 * parent.width
                height: parent.height
                color: "#2B2B2B"
                border.width: 1
                border.color: "#dedcd3"
            }

            Rectangle {
                id: crossHairRightMark
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                width: 0.2 * parent.width
                height: parent.height
                color: "#2B2B2B"
                border.width: 1
                border.color: "#dedcd3"
            }

            Rectangle {
                id: crossHairCenterDot
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                width: height
                height: parent.height
                color: "#2B2B2B"
                border.width: 1
                border.color: "#dedcd3"
            }
        }

        Rectangle {
            id: adiGaugeAngleIndicator
            z: 4
            anchors.centerIn: parent
            width: parent.width
            height: 0.9 * parent.height
            clip: true
            antialiasing: true
            color: "transparent"
            Column{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                spacing: 25
                Repeater {
                    model: ["90", "", "80", "", "70", "", "60", "", "50", "", "40", "",
                            "30", "", "20", "", "10", "", "0", "", "-10", "", "-20", "",
                            "-30", "", "-40", "", "-50", "", "-60", "", "-70", "", "-80", "",
                            "-90"]
                    Rectangle {
                        id: adiGaugeAngleIndicatorTickmarks
                        width: if(modelData == "") {
                                   0.1 * adiGaugeAngleIndicator.width
                               }
                               else {
                                   0.2 * adiGaugeAngleIndicator.width
                               }
                        height: 2
                        color: "#dedcd3"
                        antialiasing: true
                        anchors.horizontalCenter: parent.horizontalCenter

                        Text {
                            id: adiGaugeAngleIndicatorTickmarkLabelsRight
                            anchors.left: parent.right
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.leftMargin: 0.1 * parent.width
                            font.pointSize: 11
                            text: modelData
                            color: if(modelData == "0") {
                                       "transparent"
                                   }
                                   else {
                                       "#dedcd3"
                                   }
                        }

                        Text {
                            id: adiGaugeAngleIndicatorTickmarkLabelsLeft
                            anchors.right: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.rightMargin: 0.1 * parent.width
                            font.pointSize: 11
                            text: modelData
                            color: if(modelData == "0") {
                                       "transparent"
                                   }
                                   else {
                                       "#dedcd3"
                                   }
                        }
                    }
                }

                transform: Translate {
                    y: (pfdCpp.pitch / 10) * (2 * (25 + 2))
//                    y: (5 / 10) * (2 * (25 + 2))
                }
            }

            transform: Rotation {
                origin.x: adiGaugeAngleIndicator.width / 2
                origin.y: adiGaugeAngleIndicator.height / 2
                angle: - pfdCpp.roll
//                angle: 45
            }
        }

        Rectangle { // Artificial Horizon
            id: adiGaugeHorizonClipperBox
            z: 1
            clip: true
            anchors.fill: parent
            Rectangle {
                id: adiGaugeHorizon
                z: 2
                width: adiGaugeHorizonClipperBox.width  * 10
                height: adiGaugeHorizonClipperBox.height * 10
                anchors.centerIn: parent
                transform: [
                    Translate {
                        y: (pfdCpp.pitch / 10) * (2 * (25 + 2))
//                        y: (5 / 10) * (2 * (25 + 2))
                    },
                    Rotation {
                        origin.x: adiGaugeHorizon.width  / 2
                        origin.y: adiGaugeHorizon.height / 2
                        angle: - pfdCpp.roll
//                        angle: 45
                    }]
                Rectangle {
                    z: 3
                    id: adiGaugeHorizonGroundLine
                    anchors.centerIn: parent
                    width: parent.width
                    height: 3
                    antialiasing: true
                    color: "#dedcd3"
                }

                Rectangle {
                    id: adiGaugeHorizonSky
                    anchors.fill: parent
                    antialiasing: true
                    color: "#263A99"
                }

                Rectangle {
                    id: adiGaugeHorizonGround
                    height: adiGaugeHorizonSky.height / 2
                    anchors.left: adiGaugeHorizonSky.left
                    anchors.right:  adiGaugeHorizonSky.right
                    anchors.bottom: adiGaugeHorizonSky.bottom
                    antialiasing: true
                    color: "#601E15"
                }
            }
        }
    }
}
