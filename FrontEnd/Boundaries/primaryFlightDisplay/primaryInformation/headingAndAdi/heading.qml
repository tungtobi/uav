import QtQuick 2.12

Rectangle {
    id: heading
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: headingTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.left: parent.left
        anchors.top: parent.top
        text: qsTr("HDG")
    }

    Rectangle {
        id: headingGaugeValueBox
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        width: 0.25 * parent.width
        height: 0.3 * parent.height
        color: "#2B2B2B"
        border.color: "#c5c2b2"
        border.width: 2

        Text {
            id: headingGaugeValueText
            anchors.centerIn: parent
            font.pointSize: 12
            property var headingVar: if (pfdCpp.heading < 0) {
                                         return Math.round((180 + (180 + pfdCpp.heading)) * 10) / 10
                                     }
                                     else {
                                         return Math.round(pfdCpp.heading * 10) / 10
                                     }
            text: headingVar + qsTr("°")
            color: "lightgreen"
        }
    }

    Text {
        id: headingModeText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.right: parent.right
        anchors.top: parent.top
        text: qsTr("MAG")
    }

    Rectangle {
        id: headingGauge
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: headingGaugeValueBox.bottom
        width: parent.width
        height: 0.7 * parent.height
        clip: true
        color: "#545454"
        Rectangle {
            id: headingGaugeIndicator
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: 6
            height: 0.5 * parent.height
            color: "#c5c2b2"
            border.color: "#dedcd3"
            border.width: 2
        }
        Row{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: 20
            Repeater {
                model: ["80", "", "90", "", "100", "",
                        "110", "", "120", "", "130", "", "140", "", "150", "", "160", "", "170", "", "180", "", "190",
                        "", "200", "", "210", "", "220", "", "230", "", "240", "", "250", "", "260", "", "270", "", "280",
                         "", "290",  "", "300",  "", "310",  "", "320",  "", "330",  "", "340",  "", "350", "", "0", "",
                        "10", "", "20", "", "30", "", "40", "", "50", "", "60", "", "70", "", "80", "", "90", "", "100", "",
                        "110", "", "120", "", "130", "", "140", "", "150", "", "160", "", "170", "", "180", "", "190",
                        "", "200", "", "210", "", "220", "", "230", "", "240", "", "250", "", "260", "", "270", "", "280"]
                Rectangle {
                    id: headingGaugeTickmarks
                    width: 2
                    height: 0.2 * headingGauge.height
                    y: - 1.9 * parent.height
                    color: "#dedcd3"

                    Text {
                        id: headingGaugeTickmarkLabels
                        y:  2.2 *  parent.height
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: 11
                        text: modelData
                        color: "#dedcd3"
                    }
                }
            }

            transform: Translate {
                x: - pfdCpp.heading * ((20 + 2) * 2) / 10
//                x: - (-150) * ((20 + 2) * 2) / 10
            }
        }
    }
}
