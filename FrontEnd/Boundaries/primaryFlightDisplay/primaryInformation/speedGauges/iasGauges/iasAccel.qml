import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: iasAccel
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: iasAccelTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        text: qsTr("Acc.")
    }

    Gauge {
        id: iasAccelGauge
        orientation: Qt.Vertical
        tickmarkAlignment: Qt.AlignLeft
        anchors.left: parent.left
        anchors.top: iasAccelTitleText.bottom
        width: 0.36 * parent.width
        height: 0.814 * parent.height
        maximumValue: 30
        minimumValue: -30
        tickmarkStepSize: 5
//      value: 0
        value: pfdCpp.IASAccel
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: iasAccelGauge.width
                Rectangle {
                    color: "lightgreen"
                    width: parent.width
                    height: 6
                }
            }

            background: Rectangle {
                color: "#545454"
            }

            tickmark: Item {
                implicitWidth: 1
                implicitHeight: 1
                Rectangle {
                    x: parent.implicitWidth
                    height: parent.implicitWidth
                    width: iasAccelGauge.width
                    color: "#c5c2b2"
                }
            }

            tickmarkLabel: Text {
                text: control.formatValue(styleData.value)
                font.pointSize: 11
                color: "#c5c2b2"
            }

            minorTickmark: null
        }
    }

    Text {
        id: iasAccelValueText
        color: "lightgreen"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: iasAccelUnitText.top
        property var iasAccelVar: Math.round(pfdCpp.IASAccel * 100) / 100;
        text: iasAccelVar
    }

    Text {
        id: iasAccelUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        text: qsTr("m/s²")
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
