import QtQuick 2.12

Rectangle {
    id: ias
    anchors.fill: parent
    color: "#2B2B2B"
    Rectangle {
        id: iasTitleBox
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: 0.06 * parent.height
        color: "#2B2B2B"
        Text {
            id: iasTitleText
            color: "#c5c2b2"
            font.pointSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            text: qsTr("IAS")
        }
    }

    Rectangle {
        id: iasGauge
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: iasTitleText.bottom
        width: parent.width
        height: 0.9 * parent.height
        clip: true
        color: "#545454"
        Column{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: 50
            Repeater {
                model: ["50", "", "40", "", "30", "", "20", "", "10", "", "0", "",
                        "-10", "", "-20", "", "-30", "", "-40", "", "-50"]
                Rectangle {
                    id: iasGaugeTickmarks
                    width: 0.2 * iasGauge.width
                    height: 2
                    x: 2.1 * parent.width
                    color: "#dedcd3"

                    Text {
                        id: iasGaugeTickmarkLabels
                        x: - 3 *  parent.width
                        font.pointSize: 11
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: iasGauge.horizontalCenter
                        text: modelData
                        color: if (modelData< 0) {
                                   "red"
                               }
                               else {
                                   "#dedcd3"
                               }
                    }
                }
            }

            transform: Translate {
                y: pfdCpp.IAS * (10 + 4 / 10)
//                y: -150 * (10 + 4 / 10)
            }
        }

        Rectangle {
            id: iasGaugeValueBox
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            width: 0.8 * parent.width
            height: 20
            color: "#2B2B2B"
            border.color: "#dedcd3"
            border.width: 2

            Text {
                id: iasGaugeValueText
                anchors.centerIn: parent
                font.pointSize: 12
                text: Math.round(pfdCpp.IAS * 100) / 100
                color: if (pfdCpp.IAS < 0) {
                           "red"
                       }
                       else {
                           "lightgreen"
                       }
            }
        }
        Rectangle {
            id: iasGaugeIndicator
            anchors.left: iasGaugeValueBox.right
            anchors.verticalCenter: parent.verticalCenter
            width: 0.2 * parent.width
            height: 6
            color: "#dedcd3"
            border.color: "#dedcd3"
            border.width: 2
        }
    }

    Text {
        id: iasUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        text: qsTr("m/s")
    }

}
