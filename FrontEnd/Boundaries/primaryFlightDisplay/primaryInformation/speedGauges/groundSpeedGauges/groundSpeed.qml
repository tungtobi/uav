import QtQuick 2.12

Rectangle {
    id: groundSpeed
    anchors.fill: parent
    color: "#2B2B2B"
    Rectangle {
        id: groundSpeedTitleBox
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: 0.05 * parent.height
        color: "#2B2B2B"
        Text {
            id: groundSpeedText
            color: "#c5c2b2"
            font.pointSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            text: qsTr("GS")
        }
    }

    Rectangle {
        id: groundSpeedAccel
        color: "#2B2B2B"
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: 0.5 *  parent.width
        height: 0.75 *  parent.height
        Text {
            id: groundSpeedAccelTitle
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 12
            text: qsTr("Acc.")
            color: "#c5c2b2"
        }

        Text {
            id: groundSpeedAccelText
            anchors.centerIn: parent
            font.pointSize: 12
            text: Math.round(pfdCpp.groundSpeedAccel * 100) / 100
//            text: qsTr("-30.00")
            color: "lightgreen"
        }

        Text {
            id: groundSpeedAccelUnitText
            color: "#c5c2b2"
            font.pointSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            text: qsTr("m/s²")
        }
    }

    Rectangle {
        id: groundSpeedValueAndAccelBorder
        color: "#c5c2b2"
        anchors.bottom: parent.bottom
        anchors.left: groundSpeedAccel.right
        anchors.leftMargin: 2
        anchors.rightMargin: 2
        width: 2
        height: 0.75 *  parent.height
    }

    Rectangle {
        id: groundSpeedValue
        color: "#2B2B2B"
        anchors.bottom: parent.bottom
        anchors.left: groundSpeedValueAndAccelBorder.right
        width: 0.5 *  parent.width
        height: 0.75 *  parent.height
        Text {
            id: groundSpeedValueTitle
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 12
            text: qsTr("Val.")
            color: "#c5c2b2"
        }

        Text {
            id: groundSpeedValueText
            anchors.centerIn: parent
            font.pointSize: 12
            text: Math.round(pfdCpp.groundSpeed * 100) / 100
//            text: qsTr("-30.00")
            color: "lightgreen"
        }

        Text {
            id: groundSpeedValueUnitText
            color: "#c5c2b2"
            font.pointSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            text: qsTr("m/s")
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
