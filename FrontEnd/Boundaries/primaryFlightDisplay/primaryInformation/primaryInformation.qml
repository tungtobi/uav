import QtQuick 2.12
import QtQuick.Layouts 1.3

Rectangle {
    id: primaryInformation
    color: "#2B2B2B"
    anchors.fill: parent
    Rectangle {
        id: speedGauges
        color: "#2B2B2B"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 0.025 * parent.width
        anchors.topMargin: 0.025 * parent.height
        anchors.bottomMargin: 0.025 * parent.height
        width: 0.185 * parent.width
        height: 0.95 * parent.height


        Rectangle {
            id: iasGauges
            color: "#2B2B2B"
            anchors.top: parent.top
            anchors.left: parent.left
            //        anchors.leftMargin: 0.025 * parent.width
            //        anchors.topMargin: 0.025 * parent.height
            //        anchors.bottomMargin: 0.025 * parent.height
            width: parent.width
            height: (0.8 / 0.95) * parent.height
            Loader {
                id: iasAccel
                anchors.top: parent.top
                anchors.left: parent.left
                width: 0.35 * parent.width
                height: parent.height
                source: "./speedGauges/iasGauges/iasAccel.qml"
            }
            Loader {
                id: ias
                anchors.top: parent.top
                anchors.left: iasAccel.right
                width: 0.6 * parent.width
                height: parent.height
                anchors.leftMargin: 0.05 * parent.width
                source: "./speedGauges/iasGauges/ias.qml"
            }
        }
        Rectangle {
            id: groundSpeedGauges
            anchors.top: iasGauges.bottom
            anchors.left: parent.left
            width: parent.width
            height: 0.18 * parent.height
//            anchors.topMargin: 0.005 * parent.height
            Loader {
                id: groundSpeed
                anchors.top: parent.top
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                source: "./speedGauges/groundSpeedGauges/groundSpeed.qml"
            }
        }
    }
    Rectangle {

        id: headingAndAdi
        color: "#2B2B2B"
        anchors.top: parent.top
        anchors.left: speedGauges.right
        anchors.leftMargin: 0.025 * parent.width
        anchors.topMargin: 0.025 * parent.height
        anchors.bottomMargin: 0.025 * parent.height
        width: 0.53 * parent.width
        height: 0.95 * parent.height
        Loader {
            id: adi
            anchors.top: parent.top
            anchors.left: parent.left
            width: parent.width
            height: 0.8 * parent.height
            source: "./headingAndAdi/adi.qml"
        }
        Loader {
            id: heading
            anchors.top: adi.bottom
            anchors.left: parent.left
            width: parent.width
            height: 0.175 * parent.height
            anchors.topMargin: 0.025 * parent.height
            source: "./headingAndAdi/heading.qml"
        }

    }

    Rectangle {
        id: altitudeAndTrackGauges
        color: "#2B2B2B"
        anchors.top: parent.top
        anchors.left: headingAndAdi.right
        anchors.leftMargin: 0.025 * parent.width
        anchors.rightMargin: 0.025 * parent.width
        anchors.topMargin: 0.025 * parent.height
        anchors.bottomMargin: 0.025 * parent.height
        width: 0.185 * parent.width
        height: 0.95 * parent.height
        Rectangle {
            id: altitudeGauges
            color: "#2B2B2B"
            anchors.top: parent.top
            anchors.left: parent.left
            //        anchors.leftMargin: 0.025 * parent.width
            //        anchors.topMargin: 0.025 * parent.height
            //        anchors.bottomMargin: 0.025 * parent.height
            width: parent.width
            height: (0.8 / 0.95) * parent.height

            Loader {
                id: altitude
                anchors.top: parent.top
                anchors.left: parent.left
                width: 0.6 * parent.width
                height: parent.height
                source: "./altitudeAndTrackGauges/altitudeGauges/altitude.qml"
            }
            Loader {
                id: vertSpeed
                anchors.top: parent.top
                anchors.left: altitude.right
                width: 0.35 * parent.width
                height: parent.height
                anchors.leftMargin: 0.05 * parent.width
                source: "./altitudeAndTrackGauges/altitudeGauges/vertSpeed.qml"
            }
        }
        Rectangle {
            id: trackGauges
            anchors.top: altitudeGauges.bottom
            anchors.left: parent.left
            color: "green"
            width: parent.width
            height: 0.18 * parent.height
//            anchors.topMargin: 0.005 * parent.height
            Loader {
                id: track
                anchors.top: parent.top
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                source: "./altitudeAndTrackGauges/trackGauges/track.qml"
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
