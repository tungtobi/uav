import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
//import QtQuick.Layouts 1.3

Rectangle {
    id: vertSpeed
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: vertSpeedTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
//        anchors.topMargin: 0.36 * parent.height
        text: qsTr("V/S")
    }

    Gauge {
        id: vertSpeedGauge
        orientation: Qt.Vertical
        tickmarkAlignment: Qt.AlignRight
        anchors.left: parent.left
        anchors.top: vertSpeedTitleText.bottom
        width: 0.36 * parent.width
        height: 0.814 * parent.height
        maximumValue: 30
        minimumValue: -30
        tickmarkStepSize: 5
//      value: -20
        value: pfdCpp.vertSpeed
        style: GaugeStyle {
            valueBar: Item {
                implicitWidth: vertSpeedGauge.width
                Rectangle {
                    color: "lightgreen"
                    width: parent.width
                    height: 6
                }
            }

            background: Rectangle {
                color: "#545454"
            }

            tickmark: Item {
                implicitWidth: 1
                implicitHeight: 1
                Rectangle {
                    x: - vertSpeedGauge.width
                    height: parent.implicitWidth
                    width: vertSpeedGauge.width
                    color: "#c5c2b2"
                }
            }

            tickmarkLabel: Text {
                text: control.formatValue(styleData.value)
                font.pointSize: 11
                color: "#c5c2b2"
            }

            minorTickmark: null
        }
    }

    Text {
        id: vertSpeedValueText
        color: "lightgreen"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: vertSpeedUnitText.top
        property var vertSpeedVar: Math.round(pfdCpp.vertSpeed * 100) / 100;
        text: vertSpeedVar
    }

    Text {
        id: vertSpeedUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        text: qsTr("m/s")
    }

}
