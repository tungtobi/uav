import QtQuick 2.12

Rectangle {
    id: altitude
    anchors.fill: parent
    color: "#2B2B2B"
    Rectangle {
        id: altitudeTitleBox
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: 0.06 * parent.height
        color: "#2B2B2B"
        Text {
            id: altitudeTitleText
            color: "#c5c2b2"
            font.pointSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            text: qsTr("Alt.")
        }
    }

    Rectangle {
        id: altitudeGauge
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: altitudeTitleText.bottom
        width: parent.width
        height: 0.9 * parent.height
        clip: true
        color: "#545454"
        Column{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: 50
            Repeater {
                model: ["8200", "", "8100", "", "8000", "", "7900", "", "7800", "", "7700",
                    "", "7600", "", "7500", "", "7400", "", "7300", "", "7200", "", "7100", "",
                    "7000", "", "6900", "", "6800", "", "6700", "", "6600", "", "6500", "",
                    "6400", "", "6300", "", "6200", "", "6100", "",
                    "6000", "", "5900", "", "5800", "", "5700", "", "5600", "", "5500", "",
                    "5400", "", "5300", "", "5200", "", "5100", "",
                    "5000", "", "4900", "", "4800", "", "4700", "", "4600", "", "4500", "",
                    "4400", "", "4300", "", "4200", "", "4100", "", "4000", "",
                    "3900", "", "3800", "", "3700", "", "3600", "", "3500", "", "3400", "",
                    "3300", "", "3200", "", "3100", "", "3000", "", "2900", "", "2800", "",
                    "2700", "", "2600", "", "2500", "", "2400", "", "2300", "", "2200", "",
                    "2100", "", "2000", "", "1900", "", "1800", "", "1700", "", "1600", "",
                    "1500", "", "1400", "", "1300", "", "1200", "", "1100", "", "1000", "",
                    "900", "", "800", "", "700", "", "600", "", "500", "", "400", "",
                    "300", "", "200", "", "100", "", "0", "", "-100", "", "-200", "",
                    "-300", "", "-400", "", "-500", "", "-600", "", "-700", "", "-800", "",
                    "-900", "", "-1000", "", "-1100", "", "-1200", "", "-1300", "", "-1400", "",
                    "-1500", "", "-1600", "", "-1700", "", "-1800", "", "-1900", "", "-2000", "",
                    "-2100", "", "-2200", "", "-2300", "", "-2400", "", "-2500", "", "-2600", "",
                    "-2700", "", "-2800", "", "-2900", "", "-3000", "", "-3100", "", "-3200", "",
                    "-3300", "", "-3400", "", "-3500", "", "-3600", "", "-3700", "", "-3800", "",
                    "-3900", "", "-4000", "", "-4100", "", "-4200", "", "-4300", "", "-4400", "",
                    "-4500", "", "-4600", "", "-4700", "", "-4800", "", "-4900", "", "-5000", "",
                    "-5100", "", "-5200", "", "-5300", "", "-5400", "",
                    "-5500", "", "-5600", "", "-5700", "", "-5800", "", "-5900", "", "-6000", "",
                    "-6100", "", "-6200", "", "-6300", "", "-6400", "",
                    "-6500", "", "-6600", "", "-6700", "", "-6800", "", "-6900", "", "-7000", "",
                    "-7100", "", "-7200", "", "-7300", "", "-7400", "",
                    "-7500", "", "-7600", "", "-7700", "", "-7800", "", "-7900", "", "-8000", "", "-8100", "", "-8200"]
                Rectangle {
                    id: altitudeGaugeTickmarks
                    width: 0.2 * altitudeGauge.width
                    height: 2
                    x: - 2.1 * parent.width
                    color: "#dedcd3"

                    Text {
                        id: altitudeGaugeTickmarkLabels
                        x: 1.3 *  parent.width
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: altitudeGauge.horizontalCenter
                        font.pointSize: 11
                        text: modelData
                        color: if (modelData < 0) {
                                   "red"
                               }
                               else {
                                   "#dedcd3"
                               }
                    }
                }
            }

            transform: Translate {
                y: pfdCpp.altitude * (1 + 4 /  100)
//                y: - 4350 * (1 + 4 / 100)
            }
        }

        Rectangle {
            id: altitudeGaugeValueBox
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            width: 0.94 * parent.width
            height: 20
            color: "#2B2B2B"
            border.color: "#dedcd3"
            border.width: 2

            Text {
                id: altitudeGaugeValueText
                anchors.centerIn: parent
//                text: qsTr("0000.0")
                font.pointSize: 12
                text: Math.round(pfdCpp.altitude * 10) / 10
                color: if (pfdCpp.altitude < 0) {
                           "red"
                       }
                       else {
                           "lightgreen"
                       }
            }
        }
        Rectangle {
            id: altitudeGaugeIndicator
            anchors.right: altitudeGaugeValueBox.left
            anchors.verticalCenter: parent.verticalCenter
            width: 0.06 * parent.width
            height: 6
            color: "#dedcd3"
            border.color: "#dedcd3"
            border.width: 2

        }
    }

    Text {
        id: altUnitText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        text: qsTr("m")
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
