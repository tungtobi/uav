import QtQuick 2.12

Rectangle {
    id: track
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: trackTitleText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        text: qsTr("Track")
    }

    Text {
        id: trackValueText
        anchors.centerIn: parent
        font.pointSize: 12
        property var trackVar: Math.round(pfdCpp.track * 100) / 100
        text: trackVar + qsTr("°")
//        text: qsTr("-360.00") + qsTr("°")
        color: "lightgreen"
    }

    Text {
        id: trackModeText
        color: "#c5c2b2"
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        text: qsTr("MAG")
    }
}


/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
