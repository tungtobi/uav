import QtQuick 2.12
import QtQuick.Layouts 1.3

Rectangle {
    id: airAndPositionStatus
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: airAndPositionStatusTitle
        text: qsTr("Air And Position")
        color: "#c5c2b2"
        font.pointSize: 20
        font.bold: true
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 0.05 * parent.height
    }
    Rectangle {
        id: airAndPositionTelemetryInfo
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: 0.9 * parent.width
        height: 0.8 * parent.height

        Rectangle {
            id: airTelemetry
            anchors.top: parent.top
            anchors.left: parent.left
            width: 0.5 * parent.width
            height: parent.height
            color: "#2B2B2B"

            Text {
                id: airTemperatureTitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.19 * parent.height
                text: qsTr("Temp.: ")
            }

            Text {
                id: airTemperatureValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.19 * parent.height
                property var airTemperatureVar: Math.round(sdCpp.temperature * 100) / 100;
                text: airTemperatureVar
            }

            Text {
                id: airTemperatureUnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.19 * parent.height
                text: qsTr(" °C")
            }

            Text {
                id: airPressTitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: airTemperatureTitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.25 * parent.height
                text: qsTr("Press.: ")
            }

            Text {
                id: airPressValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: airTemperatureValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.25 * parent.height
                property var airPressVar: Math.round(sdCpp.airPress * 100) / 100;
                text: airPressVar
            }

            Text {
                id: airPressUnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: airTemperatureUnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.25 * parent.height
                text: qsTr(" hPa")
            }
        }

        Rectangle {
            id: positionTelemetry
            anchors.top: parent.top
            anchors.right: parent.right
            width: 0.5 * parent.width
            height: parent.height
            color: "#2B2B2B"

            Text {
                id: latitudeTitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.18 * parent.height
                text: qsTr("Lat.: ")
            }

            Text {
                id: latitudeValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.18 * parent.height
                property var latitudeVar: Math.round(sdCpp.latitude * 1000000) / 1000000;
                text: latitudeVar + qsTr("°")
            }

            Text {
                id: longitudeTitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: latitudeTitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.25 * parent.height
                text: qsTr("Long.: ")
            }

            Text {
                id: longitudeValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: latitudeValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.25 * parent.height
                property var longitudeVar: Math.round(sdCpp.longitude * 1000000) / 1000000;
                text: longitudeVar + qsTr("°")
            }
        }
    }
}
