import QtQuick 2.12
import QtQuick.Layouts 1.3
import gcs.display.SystemDisplay 1.0

Rectangle {
    id: sd
    anchors.fill: parent
    SystemDisplay {
        id: sdCpp
    }
    Loader {
        id: powerSystem
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        height: 0.28 * parent.height
        source: "./powerSystem/powerSystem.qml"
    }
    Rectangle {
        id: borderBetweenEnginePowerAndBatteryCells
        anchors.top: powerSystem.bottom
        anchors.left: parent.left
        height: 3
        width: parent.width
        color: "#c5c2b2"
    }

    Loader{
        id: batteryCells
        anchors.top: borderBetweenEnginePowerAndBatteryCells.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        height: 0.413 * parent.height
        source: "./batteryCells/batteryCells.qml"
    }

    Rectangle {
        id: borderBetweenBatteryCellsAndAirPosition
        anchors.top: batteryCells.bottom
        anchors.left: parent.left
        height: 3
        width: parent.width
        color: "#c5c2b2"
    }

    Loader {
        id: airAndPositionStatus
        anchors.top: borderBetweenBatteryCellsAndAirPosition.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        height: 0.296 * parent.height
        source: "./airAndPositionStatus/airAndPositionStatus.qml"
    }


}
