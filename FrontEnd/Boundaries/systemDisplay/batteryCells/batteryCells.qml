import QtQuick 2.12
import QtQuick.Layouts 1.3

Rectangle {
    id: batteryCells
    anchors.fill: parent
    color: "#2B2B2B"
    Text {
        id: batteryCellsTitle
        text: qsTr("Battery")
        color: "#c5c2b2"
        font.pointSize: 20
        font.bold: true
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 0.03 * parent.height
    }
    Rectangle {
        id: batteryCellTelemetryInfo
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: 0.9 * parent.width
        height: 0.85 * parent.height

        Rectangle {
            id: cell0To4
            anchors.top: parent.top
            anchors.left: parent.left
            width: 0.5 * parent.width
            height: parent.height
            color: "#2B2B2B"

            Text {
                id: cell0TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.08 * parent.height
                text: qsTr("Cell 0: ")
            }

            Text {
                id: cell0ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.08 * parent.height
                property var cell0Var: Math.round(sdCpp.cellVoltages[0] * 100) / 100;
                text: cell0Var
            }

            Text {
                id: cell0UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.08 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell1TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell0TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 1: ")
            }

            Text {
                id: cell1ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell0ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell1Var: Math.round(sdCpp.cellVoltages[1] * 100) / 100;
                text: cell1Var
            }

            Text {
                id: cell1UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell0UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell2TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell1TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 2: ")
            }


            Text {
                id: cell2ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell1ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell2Var: Math.round(sdCpp.cellVoltages[2] * 100) / 100;
                text: cell2Var
            }

            Text {
                id: cell2UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell1UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell3TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell2TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 3: ")
            }

            Text {
                id: cell3ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell2ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell3Var: Math.round(sdCpp.cellVoltages[3] * 100) / 100;
                text: cell3Var
            }

            Text {
                id: cell3UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell2UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell4TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell3TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 4: ")
            }

            Text {
                id: cell4ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell3ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell4Var: Math.round(sdCpp.cellVoltages[4] * 100) / 100;
                text: cell4Var
            }

            Text {
                id: cell4UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell3UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }

        }

        Rectangle {
            id: cell5To9
            anchors.top: parent.top
            anchors.right: parent.right
            width: 0.5 * parent.width
            height: parent.height
            color: "#2B2B2B"

            Text {
                id: cell5TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.08 * parent.height
                text: qsTr("Cell 5: ")
            }

            Text {
                id: cell5ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.08 * parent.height
                property var cell5Var: Math.round(sdCpp.cellVoltages[5] * 100) / 100;
                text: cell5Var
            }

            Text {
                id: cell5UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.08 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell6TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell5TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 6: ")
            }

            Text {
                id: cell6ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell5ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell6Var: Math.round(sdCpp.cellVoltages[6] * 100) / 100;
                text: cell6Var
            }

            Text {
                id: cell6UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell5UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell7TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell6TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 7: ")
            }

            Text {
                id: cell7ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell6ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell7Var: Math.round(sdCpp.cellVoltages[7] * 100) / 100;
                text: cell7Var
            }

            Text {
                id: cell7UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell6UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell8TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell7TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 8: ")
            }

            Text {
                id: cell8ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell7ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell8Var: Math.round(sdCpp.cellVoltages[8] * 100) / 100;
                text: cell8Var
            }

            Text {
                id: cell8UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell7UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }

            Text {
                id: cell9TitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell8TitleText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Cell 9: ")
            }

            Text {
                id: cell9ValueText
                color: "lightgreen"
                font.pointSize: 12
                anchors.top: cell8ValueText.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                property var cell9Var: Math.round(sdCpp.cellVoltages[9] * 100) / 100;
                text: cell9Var
            }

            Text {
                id: cell9UnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: cell8UnitText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.1 * parent.width
                anchors.topMargin: 0.1 * parent.height
                text: qsTr(" V")
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
