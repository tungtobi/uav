import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: powerSystem
    anchors.fill: parent
    color: "#2B2B2B"

    Text {
        id: powerSystemTitle
        text: qsTr("Power System")
        color: "#c5c2b2"
        font.pointSize: 20
        anchors.top: parent.top
        font.bold: true
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 0.03 * parent.height
    }

    Rectangle {
        id: powerSystemTelemetryInfo
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: 0.9 * parent.width
        height: 0.75 * parent.height
        anchors.bottomMargin: 0.05 * parent.height
        color: "#2B2B2B"

        Rectangle {
            id: batteryRemains
            anchors.top: parent.top
            anchors.left: parent.left
            width: parent.width
            height: 0.36 * parent.height
            color: "#2B2B2B"
            Text {
                id: batteryRemainsTitleText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 0.1 * parent.height
                text: qsTr("Battery remaining")
            }

            Gauge {
                id: batteryRemainGauge
                orientation: Qt.Horizontal
                anchors.top: batteryRemainsTitleText.bottom
                anchors.left: parent.left
                anchors.topMargin: 0.05 * parent.height
                anchors.leftMargin: 0.025 * parent.width
                width: 0.84 * parent.width
                height: 0.26 * parent.height
                maximumValue: 100
                minimumValue: 0
                tickmarkStepSize: 25
                value: sdCpp.batteryRemains
                style: GaugeStyle {

                            valueBar: Rectangle {
                                color: if (sdCpp.batteryRemains <= 10) {
                                           "red"
                                       }
                                else if (sdCpp.batteryRemains <= 20 && sdCpp.batteryRemains > 10) {
                                           "yellow"
                                       }
                                 else {
                                           "lightgreen"
                                       }
                                border.color: "#c5c2b2"
                                border.width: 1
                                implicitWidth: batteryRemainGauge.height
                            }

                            background: Rectangle {
                                color: "#545454"
                                border.color: "#c5c2b2"
                                border.width: 1
                            }

                            tickmark: Item {
                                implicitWidth: 1
                                implicitHeight: 3
                                Rectangle {
                                    x: - batteryRemainGauge.height
                                    height: parent.implicitHeight
                                    width: batteryRemainGauge.height
                                    color: "#c5c2b2"
                                }

                            }

                            tickmarkLabel: Text {
                                text: control.formatValue(styleData.value)
                                font.pointSize: 10
                                color: "#c5c2b2"
                            }

                            minorTickmark: null
                        }
            }

            Text {
                id: batteryRemainsValueText
                color: if (sdCpp.batteryRemains <= 10) {
                           "red"
                       }
                else if (sdCpp.batteryRemains <= 20 && sdCpp.batteryRemains > 10) {
                           "yellow"
                       }
                 else {
                           "lightgreen"
                       }
                font.pointSize: 12
                anchors.top: batteryRemainsTitleText.bottom
                anchors.right: batteryRemainsUnitText.left
                anchors.topMargin: 0.01 * parent.height
                property var batteryRemainsVar: Math.round(sdCpp.batteryRemains * 100) / 100;
                text: batteryRemainsVar
            }

            Text {
                id: batteryRemainsUnitText
                color: "#c5c2b2"
                font.pointSize: 12
                anchors.top: batteryRemainsTitleText.bottom
                anchors.right: parent.right
                anchors.rightMargin: 0.05 * parent.width
                anchors.topMargin: 0.025 * parent.height
                text: qsTr(" %")
            }
        }

        Rectangle {
            id: batteryInfoPane
            anchors.top: batteryRemains.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: 0.6 * parent.height
            anchors.topMargin: 0.03 * parent.height

            Rectangle {
                id: batteryVoltageAndCurrent
                anchors.top: parent.top
                anchors.left: parent.left
                width: 0.5 * parent.width
                height: parent.height
                color: "#2B2B2B"

                Text {
                    id: batteryVoltageTitleText
                    color: "#c5c2b2"
                    font.pointSize: 12
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 0.1 * parent.width
                    anchors.topMargin: 0.2 * parent.height
                    text: qsTr("Voltage: ")
                }

                Text {
                    id: batteryVoltageValueText
                    color: "lightgreen"
                    font.pointSize: 12
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: 0.2 * parent.height
                    property var batteryVoltageVar: Math.round(sdCpp.batteryVoltage * 100) / 100;
                    text: batteryVoltageVar
                }

                Text {
                    id: batteryVoltageUnitText
                    color: "#c5c2b2"
                    font.pointSize: 12
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.rightMargin: 0.1 * parent.width
                    anchors.topMargin: 0.2 * parent.height
                    text: qsTr(" V")
                }

                Text {
                    id: batteryCurrentTitleText
                    color: "#c5c2b2"
                    font.pointSize: 12
                    anchors.top: batteryVoltageTitleText.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: 0.1 * parent.width
                    anchors.topMargin: 0.2 * parent.height
                    text: qsTr("Current: ")
                }

                Text {
                    id: batteryCurrentValueText
                    color: "lightgreen"
                    font.pointSize: 12
                    anchors.top: batteryVoltageValueText.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: 0.2 * parent.height
                    property var batteryCurrentVar: Math.round(sdCpp.batteryCurrent * 100) / 100;
                    text: batteryCurrentVar
                }

                Text {
                    id: batteryCurrentUnitText
                    color: "#c5c2b2"
                    font.pointSize: 12
                    anchors.top: batteryVoltageUnitText.bottom
                    anchors.right: parent.right
                    anchors.rightMargin: 0.1 * parent.width
                    anchors.topMargin: 0.2 * parent.height
                    text: qsTr(" A")
                }



            }

            Rectangle {
                id: batteryTempAndCellCounts
                anchors.top: parent.top
                anchors.right: parent.right
                width: 0.5 * parent.width
                height: parent.height
                color: "#2B2B2B"

                Text {
                    id: batteryTemperatureTitleText
                    color: "#c5c2b2"
                    font.pointSize: 12
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 0.1 * parent.width
                    anchors.topMargin: 0.2 * parent.height
                    text: qsTr("Temp.: ")
                }

                Text {
                    id: batteryTemperatureValueText
                    color: "lightgreen"
                    font.pointSize: 12
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: 0.2 * parent.height
                    property var batteryTemperatureVar: Math.round(sdCpp.batteryTemp * 100) / 100;
                    text: batteryTemperatureVar
                }

                Text {
                    id: batteryTemperatureUnitText
                    color: "#c5c2b2"
                    font.pointSize: 12
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.rightMargin: 0.1 * parent.width
                    anchors.topMargin: 0.2 * parent.height
                    text: qsTr(" °C")
                }

                Text {
                    id: batteryCellCountTitleText
                    color: "#c5c2b2"
                    font.pointSize: 12
                    anchors.top: batteryTemperatureTitleText.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: 0.1 * parent.width
                    anchors.topMargin: 0.2 * parent.height
                    text: qsTr("Cells: ")
                }

                Text {
                    id: batteryCellCountText
                    color: "lightgreen"
                    font.pointSize: 12
                    anchors.top: batteryTemperatureValueText.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: 0.2 * parent.height
                    property var batteryCellCountVar: Math.round(sdCpp.batteryCellCount * 100) / 100;
                    text: batteryCellCountVar
                }
            }
        }
    }
}
