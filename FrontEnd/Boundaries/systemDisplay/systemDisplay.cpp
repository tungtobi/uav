#include "./FrontEnd/Boundaries/systemDisplay/systemDisplay.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>


//Private functions

SystemDisplay::SystemDisplay(QObject *parent) : QObject(parent)
{
    controller = new SystemDisplayCtrlr(this);
    QThreadPool::globalInstance()->start(controller, 3);
}


//Implementation of Engine and System Display's functions
void SystemDisplay::setBatteryRemains(const float &remains) {
    batteryRemainsBuffer = remains;
    emit batteryRemainsChanged();
}

float SystemDisplay::batteryRemains() {
    return batteryRemainsBuffer;
}

void SystemDisplay::setBatteryTemp(const float &battTemp) {
    batteryTempBuffer = battTemp;
    emit batteryTempChanged();
}

float SystemDisplay::batteryTemp() {
    return batteryTempBuffer;
}

void SystemDisplay::setBatteryVoltage(const float &battVolt) {
    batteryVoltageBuffer = battVolt;
    emit batteryVoltageChanged();
}

float SystemDisplay::batteryVoltage() {
    return batteryVoltageBuffer;
}

void SystemDisplay::setBatteryCurrent(const float &battCurr) {
    batteryCurrentBuffer = battCurr;
    emit batteryCurrentChanged();
}

float SystemDisplay::batteryCurrent() {
    return batteryCurrentBuffer;
}

void SystemDisplay::setBatteryCellCount(const int32_t &battCells) {
    batteryCellCountBuffer = battCells;
    emit batteryCellCountChanged();
}

int32_t SystemDisplay::batteryCellCount() {
    return batteryCellCountBuffer;
}

void SystemDisplay::setCellVoltages(const QList<qreal> &cellVolts) {
        cellVoltagesBuffer = cellVolts;
    emit cellVoltagesChanged();
}

QList<qreal> SystemDisplay::cellVoltages() {
    return cellVoltagesBuffer;
}

void SystemDisplay::setTemperature(const float &temp) {
    temperatureBuffer = temp;
    emit temperatureChanged();
}

float SystemDisplay::temperature() {
    return temperatureBuffer;
}

void SystemDisplay::setLatitude(const double &lat) {
    latitudeBuffer = lat;
    emit latitudeChanged();
}

double SystemDisplay::latitude() {
    return latitudeBuffer;
}


void SystemDisplay::setLongitude(const double &lon) {
    longitudeBuffer = lon;
    emit longitudeChanged();
}

double SystemDisplay::longitude() {
    return longitudeBuffer;
}

void SystemDisplay::setAirPress(const float &airPress){
    airPressBuffer = airPress;
    emit airPressChanged();
}

float SystemDisplay::airPress() {
    return airPressBuffer;
}
