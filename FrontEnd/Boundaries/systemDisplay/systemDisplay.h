#ifndef SYSTEMDISPLAY_H
#define SYSTEMDISPLAY_H

#include <QObject>
#include <QThreadPool>
#include <QList>
#include "./FrontEnd/Controllers/systemDisplayCtrlr/systemDisplayCtrlr.h"

class SystemDisplay : public QObject
{
    Q_OBJECT
    //Properties for Engine and System Display
    Q_PROPERTY(float batteryRemains READ batteryRemains WRITE setBatteryRemains NOTIFY batteryRemainsChanged)
    Q_PROPERTY(float batteryTemp READ batteryTemp WRITE setBatteryTemp NOTIFY batteryTempChanged)
    Q_PROPERTY(float batteryVoltage READ batteryVoltage WRITE setBatteryVoltage NOTIFY batteryVoltageChanged)
    Q_PROPERTY(float batteryCurrent READ batteryCurrent WRITE setBatteryCurrent NOTIFY batteryCurrentChanged)
    Q_PROPERTY(int32_t batteryCellCount READ batteryCellCount WRITE setBatteryCellCount NOTIFY batteryCellCountChanged)
    Q_PROPERTY(QList<qreal> cellVoltages READ cellVoltages WRITE setCellVoltages NOTIFY cellVoltagesChanged)
    Q_PROPERTY(float temperature READ temperature WRITE setTemperature NOTIFY temperatureChanged)
    Q_PROPERTY(double latitude READ latitude WRITE setLatitude NOTIFY latitudeChanged)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude NOTIFY longitudeChanged)
    Q_PROPERTY(float airPress READ airPress WRITE setAirPress NOTIFY airPressChanged)


public:
    explicit SystemDisplay(QObject *parent = nullptr);


    //Public methods/variables for Engine and System Display
    float batteryRemains();
    float batteryTemp();
    float batteryVoltage();
    float batteryCurrent();
    int32_t batteryCellCount();
    QList<qreal> cellVoltages();
    float temperature();
    double latitude();
    double longitude();
    float airPress();


public slots:
    //Slots for Engine and System Display
    void setBatteryRemains(const float &remains);
    void setBatteryTemp(const float &battTemp);
    void setBatteryVoltage(const float &battVolt);
    void setBatteryCurrent(const float &battCurr);
    void setBatteryCellCount(const int32_t &battCells);
    void setCellVoltages(const QList<qreal> &cellVolts);
    void setTemperature(const float &temp);
    void setLatitude(const double &lat);
    void setLongitude(const double &lon);
    void setAirPress(const float &airPress);

signals:
    //Signals for Engine and System Display
    void batteryRemainsChanged();
    void batteryTempChanged();
    void batteryVoltageChanged();
    void batteryCurrentChanged();
    void batteryCellCountChanged();
    void cellVoltagesChanged();
    void temperatureChanged();
    void latitudeChanged();
    void longitudeChanged();
    void airPressChanged();

private:
    //Private internal buffer variables and methods
    SystemDisplayCtrlr *controller;

    //Private internal buffer variables and methods for Engine and System Display
    float batteryRemainsBuffer;
    float batteryTempBuffer;
    float batteryVoltageBuffer;
    float batteryCurrentBuffer;
    int32_t batteryCellCountBuffer;
    QList<qreal> cellVoltagesBuffer;
    float temperatureBuffer;
    double latitudeBuffer;
    double longitudeBuffer;
    float airPressBuffer;

};

#endif // SYSTEMDISPLAY_H
