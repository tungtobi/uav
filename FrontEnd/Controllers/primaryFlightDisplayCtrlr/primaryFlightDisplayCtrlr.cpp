#include "./FrontEnd/Controllers/primaryFlightDisplayCtrlr/primaryFlightDisplayCtrlr.h"

//Implementation of constructor
PrimaryFlightDisplayCtrlr::PrimaryFlightDisplayCtrlr(QObject *receiver) {
    primaryFlightDisplay = receiver;
    IASAccelTriggerCountBuffer = 1;
    groundSpeedAccelTriggerCountBuffer = 1;
    historicalIASBuffer = 0;
    historicalGroundSpeedBuffer = 0;
}


//Implementation of Public methods
void PrimaryFlightDisplayCtrlr::run(){
    while(true){
        this->refreshView();
        //display is refreshed at 48Hz, double the speed of backend update to ensure low update latency
        std::this_thread::sleep_for(std::chrono::milliseconds(21));
    }
}


//Implementation of Private methods
void PrimaryFlightDisplayCtrlr::refreshView() {
    this->constructDisplayableControlSurfacesStatusList();
    QMetaObject::invokeMethod(primaryFlightDisplay, "setControlSurfaces",
                              Qt::QueuedConnection,
                              Q_ARG(QList<qreal>, this->displayableControlSurfacesStatusList));
    this->resetDisplayableControlSurfacesStatusList();
    QMetaObject::invokeMethod(primaryFlightDisplay, "setRoll",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::roll()));
    QMetaObject::invokeMethod(primaryFlightDisplay, "setPitch",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::pitch()));
    QMetaObject::invokeMethod(primaryFlightDisplay, "setHeading",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::heading()));
    this->setTrack(AircraftModel::heading(), AircraftModel::groundSpeedForward(), AircraftModel::groundSpeedRight());
    QMetaObject::invokeMethod(primaryFlightDisplay, "setIAS",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::IAS()));
    this->setIASAccel(AircraftModel::IAS());
    this->setGroundSpeed(AircraftModel::groundSpeedForward(), AircraftModel::groundSpeedRight());
    this->setGroundSpeedAccel(AircraftModel::groundSpeedForward(), AircraftModel::groundSpeedRight());
    QMetaObject::invokeMethod(primaryFlightDisplay, "setPowerPercent",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::powerPercent()));
    QMetaObject::invokeMethod(primaryFlightDisplay, "setVertSpeed",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::vertSpeed()));
    QMetaObject::invokeMethod(primaryFlightDisplay, "setAltitude",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::altitude()));
}

void PrimaryFlightDisplayCtrlr::setIASAccel(const float &newIndctAirSpeed) {
    //IAS Acceleration is calculated every 1/8 of a second
    if (IASAccelTriggerCountBuffer == 6) {//Because update freq is set to 48Hz so 6 cycles of 48Hz means 1/8 of a second
        //The IAS accel is calculated by dividing the difference of current
        //and historical IAS from last sample time (last 1/4 of a second) by
        //the time variance (1/8 of a second), because the IAS accel is measured in m/s/s.
        //We chose 6 update cycles because the Telemetry backend updates at 24Hz (half of controller's rate),
        //thus 6 cycles of 48Hz equals at least 3 cycles of 24Hz (including all possible delays and thread competitions),
        //making sure that data is updated even if the backend misses 2 cycles because of delays or inteferences.
        QMetaObject::invokeMethod(primaryFlightDisplay, "setIASAccel",
                                  Qt::QueuedConnection,
                                  Q_ARG(float, ((newIndctAirSpeed - historicalIASBuffer) / 0.125))); // 1/8 == 0.125
        IASAccelTriggerCountBuffer = 1;
        historicalIASBuffer = newIndctAirSpeed;
    }
    else {
        IASAccelTriggerCountBuffer++;
    }
}

void PrimaryFlightDisplayCtrlr::setGroundSpeed(const float &groundSpeedForward, const float &groundSpeedRight) {
    QMetaObject::invokeMethod(primaryFlightDisplay, "setGroundSpeed",
                              Qt::QueuedConnection,
                              Q_ARG(float, (sqrt(pow(groundSpeedForward, 2) + pow(groundSpeedRight, 2)))));
}

void PrimaryFlightDisplayCtrlr::setGroundSpeedAccel(const float &groundSpeedForward, const float &groundSpeedRight) {
    //Ground speed (GS for short) Acceleration is calculated every 1/8 of a second
    if (groundSpeedAccelTriggerCountBuffer == 6) {//Because update freq is set to 48Hz so 6 cycles of 48Hz means 1/8 of a second
        //The GS accel is calculated by dividing the difference of current
        //and historical GS from last sample time (last 1/4 of a second) by
        //the time variance (1/8 of a second), because the GS accel is measured in m/s/s.
        //We chose 6 update cycles because the Telemetry backend updates at 24Hz (half of controller's rate),
        //thus 6 cycles of 48Hz equals at least 3 cycles of 24Hz (including all possible delays and thread competitions),
        //making sure that data is updated even if the backend misses 2 cycles because of delays or inteferences.
        float newGroundSpeed = sqrt(pow(groundSpeedForward, 2) + pow(groundSpeedRight, 2));
        QMetaObject::invokeMethod(primaryFlightDisplay, "setGroundSpeedAccel",
                                  Qt::QueuedConnection,
                                  Q_ARG(float, ((newGroundSpeed - historicalGroundSpeedBuffer) / 0.125))); // 1/8 == 0.125
        groundSpeedAccelTriggerCountBuffer = 1;
        historicalGroundSpeedBuffer = newGroundSpeed;
    }
    else {
        groundSpeedAccelTriggerCountBuffer++;
    }
}

void PrimaryFlightDisplayCtrlr::setTrack(const float &heading, const float &groundSpeedForward, const float &groundSpeedRight) {
    float track;
    float bearing;
    if (groundSpeedRight == 0) {
        bearing = 0;
    }
    else {
        bearing = ((atan2(groundSpeedRight, groundSpeedForward)) * 180) / M_PI ;
    }
    if (heading >= 0) {
        track = (heading + bearing);
    }
    else {
        track = ((180 + (180 + heading)) + bearing);
    }
    if (track > 360) {
        track = (track - 360);
    }
    else if (track < 0) {
        track = (360 + track);
    }
    QMetaObject::invokeMethod(primaryFlightDisplay, "setTrack",
                              Qt::QueuedConnection,
                              Q_ARG(float, track));
}

void PrimaryFlightDisplayCtrlr::constructDisplayableControlSurfacesStatusList() {
    for (int i = 0; i < 8; i++) {
        displayableControlSurfacesStatusList << AircraftModel::controlSurfaces(i);
    }
}

void PrimaryFlightDisplayCtrlr::resetDisplayableControlSurfacesStatusList() {
    displayableControlSurfacesStatusList.clear();
}
