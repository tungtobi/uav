#ifndef PRIMARYFLIGHTDISPLAYCTRLR_H
#define PRIMARYFLIGHTDISPLAYCTRLR_H


#include <QRunnable>
#include <QThreadPool>
#include <QList>
#include "math.h"5
#include "./Models/AircraftModel/AircraftModel.h"

class PrimaryFlightDisplayCtrlr : public QRunnable
{
private:
    //Private variables
    QObject *primaryFlightDisplay;
    QList<qreal> displayableControlSurfacesStatusList;
    float historicalIASBuffer;
    int IASAccelTriggerCountBuffer; //To count if its time to update IAS accel.
    float historicalGroundSpeedBuffer;
    int groundSpeedAccelTriggerCountBuffer; //To count if its time to update GS accel.

    //Private methods
    void refreshView();
    void setIASAccel(const float &newIndctAirSpeed);
    void setGroundSpeed(const float &groundSpeedForward, const float &groundSpeedRight);
    void setGroundSpeedAccel(const float &groundSpeedForward, const float &groundSpeedRight);
    void setTrack(const float &heading, const float &groundSpeedForward, const float &groundSpeedRight);
    void constructDisplayableControlSurfacesStatusList();
    void resetDisplayableControlSurfacesStatusList();

public:
    PrimaryFlightDisplayCtrlr(QObject *receiver);

    //Public methods
    void run();

};

#endif // PRIMARYFLIGHTDISPLAYCTRLR_H
