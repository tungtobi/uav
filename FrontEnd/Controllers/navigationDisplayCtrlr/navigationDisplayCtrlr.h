#ifndef NAVIGATIONDISPLAYCTRLR_H
#define NAVIGATIONDISPLAYCTRLR_H


#include <QRunnable>
#include <QThreadPool>
#include <QList>
#include "./Models/AircraftModel/AircraftModel.h"
#include "math.h"
#include "iomanip"

class NavigationDisplayCtrlr : public QRunnable
{
private:
    //Private variables
    QObject *navigationDisplay;
    float heading;
    float historicalHeading;
    int predictionUpdateRateCounter;
    double predictedCoordinates[2][121];
    float predictedHeading[121];
    float predictedAltitude[121];
    QList<qreal> predictedLatitudeDisplayableList;
    QList<qreal> predictedLongitudeDisplayableList;

    //Private methods
    void refreshView();
    void clearPredictedPath();
    void calculatePathPrediction(float timeInterval, float sampleRate);
    void constructDisplayableCoordinatePredictionLists();
    void resetDisplayableCoordinatePredictionLists();

    //Validation stuffs
    //Testing and validation variables
    int predictionTestCounter;
    double max;
    //Validation function
    void validatePrediction();

public:
    NavigationDisplayCtrlr(QObject *receiver);

    //Public methods
    void run();

};

#endif // NAVIGATIONDISPLAYCTRLR_H
