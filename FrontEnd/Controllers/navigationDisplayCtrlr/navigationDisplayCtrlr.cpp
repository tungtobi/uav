#include "./FrontEnd/Controllers/navigationDisplayCtrlr/navigationDisplayCtrlr.h"

//Implementation of constructor
NavigationDisplayCtrlr::NavigationDisplayCtrlr(QObject *receiver) {
    navigationDisplay = receiver;
    predictionUpdateRateCounter = 0;

    //Initialize testing and validation variables
    predictionTestCounter = 0;
    max = 0;
}


//Implementation of Public methods
void NavigationDisplayCtrlr::run(){
    while(true){
//        auto prev = std::chrono::high_resolution_clock::now();

        if (AircraftModel::connectStatus()) {
            //Heading variation speed (HVS) is calculated every 1/8 of a second
            //Because update freq is set to 48Hz so 6 cycles of 48Hz means 1/8 of a second
            //We chose 6 update cycles because the Telemetry backend updates at 24Hz (half of controller's rate),
            //thus 6 cycles of 48Hz equals at least 3 cycles of 24Hz (including all possible delays and thread competitions),
            //making sure that data is updated even if the backend misses 2 cycles because of delays or inteferences.
            //The HVS is calculated by dividing the difference of current
            //and historical heading from last sample time (last 1/4 of a second) by
            //the time variance (1/8 of a second), because the HVS is measured in degrees/s.
            //Because our calculations requires updated HVS everytime the prediction loop is redone, we have to rely
            //on the HVS's update rate, thus making the prediction only when a new HVS info has arrived (aka. 1/8 secons).
            if (predictionUpdateRateCounter < 6) {
                predictionUpdateRateCounter++;
            }
            else {
                //Sample time = 1/8 = 0.125. This is heading's sample rate (used to calculate HVS).
                //Time interval = 1/10 = 0.1. This is the interval between the predicted points in an array. For example,
                //we have a real point A, we need to predict points in the future that the plane will travel, we name
                //these points A1, A2,...An. All these predicted points are calculated based on the current situation of
                //the aircraft at a single real point A. The time interval is the spacing in time between
                //these predicted A1, A2,...An points. With each update interval in (accordance to sample rate of heading
                //value), a new actual point is recorded and an array of points are predicted. We have chosen intervals
                //of 1/10s because we feel that it is smooth enough for the predicted line to be displayed sufficiently
                //while not taking too much processing power or cause unacceptable delays for most common PCs in 2021.
                this->calculatePathPrediction(0.1, 0.125);
                predictionUpdateRateCounter = 0;
            }
            //        this->validatePrediction();
        }
        else {
            if (predictedCoordinates[0][0] != 0 || predictedCoordinates[1][0] != 0) {
                this->clearPredictedPath();
            }
        }
        this->refreshView();

//        auto curr = std::chrono::high_resolution_clock::now();
//        if (max < std::chrono::duration_cast<std::chrono::nanoseconds>(curr-prev).count()) {
//            max = std::chrono::duration_cast<std::chrono::nanoseconds>(curr-prev).count();
//        }
//        std::cout << max << "ns\n";
//        std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(curr-prev).count() << "ns\n";
//        display is refreshed at 48Hz, double the speed of backend update to ensure low update latency
        std::this_thread::sleep_for(std::chrono::milliseconds(21));
    }
}


//Implementation of Private methods
void NavigationDisplayCtrlr::refreshView() {
    QMetaObject::invokeMethod(navigationDisplay, "setLatitude",
                              Qt::QueuedConnection,
                              Q_ARG(double, AircraftModel::latitude()));
    QMetaObject::invokeMethod(navigationDisplay, "setLongitude",
                              Qt::QueuedConnection,
                              Q_ARG(double, AircraftModel::longitude()));
    QMetaObject::invokeMethod(navigationDisplay, "setHeading",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::heading()));
    this->constructDisplayableCoordinatePredictionLists();
    QMetaObject::invokeMethod(navigationDisplay, "setPredictedLatitudes",
                              Qt::QueuedConnection,
                              Q_ARG(QList<qreal>, this->predictedLatitudeDisplayableList));
    QMetaObject::invokeMethod(navigationDisplay, "setPredictedLongitudes",
                              Qt::QueuedConnection,
                              Q_ARG(QList<qreal>, this->predictedLongitudeDisplayableList));
    this->resetDisplayableCoordinatePredictionLists();
}

void NavigationDisplayCtrlr::clearPredictedPath() {
    for (int i = 0; i < 121; i++) {
        predictedCoordinates[0][i] = 0;
        predictedCoordinates[1][i] = 0;
    }
}

void NavigationDisplayCtrlr::calculatePathPrediction(float timeInterval, float sampleRate) {
    //Test with 1 second interval between calculation points
    predictedCoordinates[0][0] = AircraftModel::latitude(); //[0][n]: latitude of point n-th
    predictedCoordinates[1][0] = AircraftModel::longitude(); //[1][n]: longitude of point n-th
    predictedHeading[0] = AircraftModel::heading();
    predictedAltitude[0] = AircraftModel::altitude();

    //Calculate vevtor component X (Forward)
    float compX = AircraftModel::groundSpeedForward() * timeInterval;

    //Calculate vevtor component Y (Right)
    float compY = AircraftModel::groundSpeedRight() * timeInterval;

    //Calculate vevtor Length
    float vectorLength = (sqrt(pow(compX, 2) + pow(compY, 2))) / 1000;

    //Calculate Heading variation speed
    float headingVarSpeed = 0;
    if (AircraftModel::heading() >= 0) {
        heading = AircraftModel::heading();
    }
    else {
        heading = 180 + (180 + AircraftModel::heading());
    }
    if ((heading - historicalHeading) >= 180 || (heading - historicalHeading) <= -180) {
        if (heading < 180 && historicalHeading > 180) {
            headingVarSpeed = (((heading - 0) + (360 - historicalHeading)) / sampleRate);
        }
        else if (heading > 180 && historicalHeading < 180) {
            headingVarSpeed = (((heading - 360) + (0 - historicalHeading)) / sampleRate);
        }
    }
    else {
        headingVarSpeed = ((heading - historicalHeading) / sampleRate);
    }
    historicalHeading = heading;

    //For each 120 prediction points, the loop iterate
    for (int i = 1; i < 121; i++) {

        //Calculate movement vector's bearing (to the Earth's North/0deg vector)
        float vectorBearing;
        float bearingRelatedToAirframeForwardAxis;
        if (compY == 0) {
            bearingRelatedToAirframeForwardAxis = 0;
        }
        else {
            bearingRelatedToAirframeForwardAxis = ((atan2(compY, compX)) * 180) / M_PI ;
        }
        if (predictedHeading[i-1] >= 0) {
            vectorBearing = (predictedHeading[i-1] + bearingRelatedToAirframeForwardAxis);
        }
        else {
            vectorBearing = ((180 + (180 + predictedHeading[i-1])) + bearingRelatedToAirframeForwardAxis);
        }
        if (vectorBearing > 360) {
            vectorBearing = (vectorBearing - 360);
        }
        else if (vectorBearing < 0) {
            vectorBearing = (360 + vectorBearing);
        }
        double vectorBearingRadian = ((vectorBearing * M_PI) / 180);

        //Calculate (predicted) distance to Earth's core
        const float equatorialRadius = 6378.137;
        const float polarRadius = 6356.752;
        double latitudeRadian = ((predictedCoordinates[0][i-1] * M_PI) / 180);
        double distToEarthCore = (sqrt((pow((pow(equatorialRadius, 2) * cos(latitudeRadian)), 2)
                     + pow((pow(polarRadius, 2) * sin(latitudeRadian)), 2))
                / (pow((equatorialRadius * cos(latitudeRadian)), 2)
                   + pow((polarRadius * sin(latitudeRadian)), 2))) + (predictedAltitude[i-1] / 1000));

        //Calculate the point's predicted coordinates
        //Calculate the point's predicted Latitude
        double angularDistance = vectorLength / distToEarthCore;
        double predictedLatitudeRadian = (asin((sin(latitudeRadian) * cos(angularDistance))
                     + (cos(latitudeRadian) * sin(angularDistance) * cos(vectorBearingRadian))));
        predictedCoordinates[0][i] = ((predictedLatitudeRadian * 180) / M_PI);

        //Calculate the point's predicted Longitude
        double predictedLongitudeShiftRadian = atan2((sin(vectorBearingRadian) * sin(angularDistance) * cos(latitudeRadian)),
                                                     (cos(angularDistance) - (sin(latitudeRadian) * sin(predictedLatitudeRadian))));
        predictedCoordinates[1][i] = (predictedCoordinates[1][i-1]
                + ((predictedLongitudeShiftRadian * 180) / M_PI));

        //Calculate the point's predicted heading
        if ((predictedHeading[i-1] + headingVarSpeed * timeInterval) > 360) {
            predictedHeading[i] = ((predictedHeading[i-1] + headingVarSpeed * timeInterval) - 360);
        }
        else if ((predictedHeading[i-1] + headingVarSpeed * timeInterval) < 0) {
            predictedHeading[i] = (360 + (predictedHeading[i-1] + headingVarSpeed * timeInterval));
        }
        else {
            predictedHeading[i] = (predictedHeading[i-1] + headingVarSpeed * timeInterval);
        }

        //Calculate the point's predicted altitude
        predictedAltitude[i] = predictedAltitude[i-1] + (AircraftModel::vertSpeed() * timeInterval);
    }
}

void NavigationDisplayCtrlr::constructDisplayableCoordinatePredictionLists() {
    for (int i = 0; i < 121; i++) {
        predictedLatitudeDisplayableList << predictedCoordinates[0][i];
        predictedLongitudeDisplayableList << predictedCoordinates[1][i];
    }
}

void NavigationDisplayCtrlr::resetDisplayableCoordinatePredictionLists() {
    predictedLatitudeDisplayableList.clear();
    predictedLongitudeDisplayableList.clear();
}

//Validation functions
void NavigationDisplayCtrlr::validatePrediction() {
    if (predictionTestCounter < 48 * 15) {
        predictionTestCounter++;
    }
    else {
        std::cout << std::scientific << std::setprecision(8) << "Lat.: " << AircraftModel::latitude() << " || Long.: " << AircraftModel::longitude() << std::endl;
        std::cout << std::scientific << std::setprecision(8) << "Predct. Lat.: " << predictedCoordinates[0][150] << " || Predct. Long.: " << predictedCoordinates[1][150] << std::endl;
        predictionTestCounter = 0;
    }
}
