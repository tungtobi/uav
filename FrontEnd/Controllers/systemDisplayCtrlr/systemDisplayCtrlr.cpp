#include "./FrontEnd/Controllers/systemDisplayCtrlr/systemDisplayCtrlr.h"

//Implementation of constructor
SystemDisplayCtrlr::SystemDisplayCtrlr(QObject *receiver) {
    systemDisplay = receiver;
    qRegisterMetaType<uint32_t>("uint32_t");
    qRegisterMetaType<int32_t>("int32_t");
    qRegisterMetaType<uint64_t>("uint64_t");
}


//Implementation of Public methods
void SystemDisplayCtrlr::run(){
    while(true){
        this->refreshView();
        //display is refreshed at 48Hz, double the speed of backend update to ensure low update latency
        std::this_thread::sleep_for(std::chrono::milliseconds(21));
    }
}


//Implementation of Private methods
void SystemDisplayCtrlr::refreshView() {
    QMetaObject::invokeMethod(systemDisplay, "setBatteryRemains",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::batteryRemains()));
    QMetaObject::invokeMethod(systemDisplay, "setBatteryTemp",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::batteryTemp()));
    QMetaObject::invokeMethod(systemDisplay, "setBatteryVoltage",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::batteryVoltage()));
    QMetaObject::invokeMethod(systemDisplay, "setBatteryCurrent",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::batteryCurrent()));
    QMetaObject::invokeMethod(systemDisplay, "setBatteryCellCount",
                              Qt::QueuedConnection,
                              Q_ARG(int32_t, AircraftModel::batteryCellCount()));
    this->constructDisplayableCellVoltagesStatusList();
    QMetaObject::invokeMethod(systemDisplay, "setCellVoltages",
                              Qt::QueuedConnection,
                              Q_ARG(QList<qreal>, this->displayableCellVoltagesStatusList));
    this->resetDisplayableCellVoltagestatusList();
    QMetaObject::invokeMethod(systemDisplay, "setTemperature",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::temperature()));
    QMetaObject::invokeMethod(systemDisplay, "setLatitude",
                              Qt::QueuedConnection,
                              Q_ARG(double, AircraftModel::latitude()));
    QMetaObject::invokeMethod(systemDisplay, "setLongitude",
                              Qt::QueuedConnection,
                              Q_ARG(double, AircraftModel::longitude()));
    QMetaObject::invokeMethod(systemDisplay, "setAirPress",
                              Qt::QueuedConnection,
                              Q_ARG(float, AircraftModel::airPress()));
}

void SystemDisplayCtrlr::constructDisplayableCellVoltagesStatusList() {
    for (int i = 0; i < 10; i++) {
        displayableCellVoltagesStatusList << AircraftModel::cellVoltages(i);
    }
}

void SystemDisplayCtrlr::resetDisplayableCellVoltagestatusList() {
    displayableCellVoltagesStatusList.clear();
}
