#ifndef SYSTEMDISPLAYCTRLR_H
#define SYSTEMDISPLAYCTRLR_H


#include <QRunnable>
#include <QThreadPool>
#include <QList>
#include "./Models/AircraftModel/AircraftModel.h"

class SystemDisplayCtrlr : public QRunnable
{
private:
    //Private variables
    QObject *systemDisplay;
    QList<qreal> displayableCellVoltagesStatusList;

    //Private methods
    void refreshView();
    void constructDisplayableCellVoltagesStatusList();
    void resetDisplayableCellVoltagestatusList();

public:
    SystemDisplayCtrlr(QObject *receiver);

    //Public methods
    void run();

};

#endif // SYSTEMDISPLAYCTRLR_H
