#include "./FrontEnd/Controllers/connectionDisplayCtrlr/connectionDisplayCtrlr.h"

//Implementation of constructor
ConnectionDisplayCtrlr::ConnectionDisplayCtrlr(QObject *receiver) {
    connectionDisplay = receiver;
    AircraftModel::resetBuffer();
    systemBackend = new AircraftSystemBackend();
    telemBackend = new AircraftTelemetryBackend();
    QThreadPool::globalInstance()->start(systemBackend, 0);
    QThreadPool::globalInstance()->start(telemBackend, 1);
}


//Implementation of Public methods
void ConnectionDisplayCtrlr::run(){
    while(true){
        this->refreshView();
        //display is refreshed at 48Hz, double the speed of backend update to ensure low update latency
        std::this_thread::sleep_for(std::chrono::milliseconds(21));
    }
}

void ConnectionDisplayCtrlr::connect() {
    QString addressTmp;
    QMetaObject::invokeMethod(connectionDisplay, "inputAddress",
                              Qt::DirectConnection,
                              Q_RETURN_ARG(QString, addressTmp));
    if (addressTmp != "") {
        AircraftModel::setAddress(addressTmp.toUtf8().constData());
        AircraftModel::setConnectCommand(true);
    }
}

void ConnectionDisplayCtrlr::disconnect() {
    AircraftModel::setConnectCommand(false);
}


//Implementation of Private methods
void ConnectionDisplayCtrlr::refreshView() {
    QMetaObject::invokeMethod(connectionDisplay, "setConnectStatus",
                              Qt::QueuedConnection,
                              Q_ARG(QString, this->getConnectionStatus()));
    QMetaObject::invokeMethod(connectionDisplay, "setAddress",
                              Qt::QueuedConnection,
                              Q_ARG(QString, QString::fromStdString(AircraftModel::address())));
}

QString ConnectionDisplayCtrlr::getConnectionStatus() {
    if (!AircraftModel::connectCommand()) {
        return "Not connected";
    }
    else {
        if (AircraftModel::connectStatus()) {
            return "Connected";
        }
        else {
            return "Connecting...";
        }
    }
}
