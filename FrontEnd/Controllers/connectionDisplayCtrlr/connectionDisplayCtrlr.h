#ifndef CONNECTIONDISPLAYCTRLR_H
#define CONNECTIONDISPLAYCTRLR_H


#include <QRunnable>
#include <QThreadPool>
#include "./Models/AircraftModel/AircraftModel.h"
#include "./Backends/AircraftSystemBackend/AircraftSystemBackend.h"
#include "./Backends/AircraftTelemetryBackend/AircraftTelemetryBackend.h"


class ConnectionDisplayCtrlr : public QRunnable
{
private:
    //Private variables
    QObject *connectionDisplay;
    AircraftSystemBackend *systemBackend;
    AircraftTelemetryBackend *telemBackend;

    //Private methods
    void refreshView();
    QString getConnectionStatus();

public:
    ConnectionDisplayCtrlr(QObject *receiver);

    //Public methods
    void run();
    void connect();
    void disconnect();

};

#endif // CONNECTIONDISPLAYCTRLR_H
