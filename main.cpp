#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "./FrontEnd/Boundaries/connectionDisplay/connectionDisplay.h"
#include "./FrontEnd/Boundaries/primaryFlightDisplay/primaryFlightDisplay.h"
#include "./FrontEnd/Boundaries/systemDisplay/systemDisplay.h"
#include "./FrontEnd/Boundaries/navigationDisplay/navigationDisplay.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(5, 12, 8)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    //Set max No. of threads to 500, from default 4
    QThreadPool::globalInstance()->setMaxThreadCount(500);

    qmlRegisterType<ConnectionDisplay>("gcs.display.ConnectionDisplay", 1, 0, "ConnectionDisplay");

    qmlRegisterType<PrimaryFlightDisplay>("gcs.display.PrimaryFlightDisplay", 1, 0, "PrimaryFlightDisplay");

    qmlRegisterType<SystemDisplay>("gcs.display.SystemDisplay", 1, 0, "SystemDisplay");

    qmlRegisterType<NavigationDisplay>("gcs.display.NavigationDisplay", 1, 0, "NavigationDisplay");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/FrontEnd/Boundaries/mainWindow.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
