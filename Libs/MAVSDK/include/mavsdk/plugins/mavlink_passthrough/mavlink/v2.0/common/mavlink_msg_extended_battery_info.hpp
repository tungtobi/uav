// MESSAGE EXTENDED_BATTERY_INFO support class

#pragma once

namespace mavlink {
namespace common {
namespace msg {

/**
 * @brief EXTENDED_BATTERY_INFO message
 *
 * CUSTOM MAV MSG TEST.
 */
struct EXTENDED_BATTERY_INFO : mavlink::Message {
    static constexpr msgid_t MSG_ID = 501;
    static constexpr size_t LENGTH = 64;
    static constexpr size_t MIN_LENGTH = 64;
    static constexpr uint8_t CRC_EXTRA = 58;
    static constexpr auto NAME = "EXTENDED_BATTERY_INFO";


    uint64_t timestamp; /*<  time since system start (microseconds) */
    float temperature; /*<  Temperature of the battery. NaN if unknown. */
    float voltage_v; /*<  Battery voltage in volts, 0 if unknown. */
    float current_a; /*<  Battery current in amperes, -1 if unknown. */
    int32_t cell_count; /*<  Number of cells. */
    float voltage_cell_v_0; /*<  Battery individual cell voltages (Cell No. 1) */
    float voltage_cell_v_1; /*<  Battery individual cell voltages (Cell No. 2) */
    float voltage_cell_v_2; /*<  Battery individual cell voltages (Cell No. 3) */
    float voltage_cell_v_3; /*<  Battery individual cell voltages (Cell No. 4) */
    float voltage_cell_v_4; /*<  Battery individual cell voltages (Cell No. 5) */
    float voltage_cell_v_5; /*<  Battery individual cell voltages (Cell No. 6) */
    float voltage_cell_v_6; /*<  Battery individual cell voltages (Cell No. 7) */
    float voltage_cell_v_7; /*<  Battery individual cell voltages (Cell No. 8) */
    float voltage_cell_v_8; /*<  Battery individual cell voltages (Cell No. 9) */
    float voltage_cell_v_9; /*<  Battery individual cell voltages (Cell No. 10) */


    inline std::string get_name(void) const override
    {
            return NAME;
    }

    inline Info get_message_info(void) const override
    {
            return { MSG_ID, LENGTH, MIN_LENGTH, CRC_EXTRA };
    }

    inline std::string to_yaml(void) const override
    {
        std::stringstream ss;

        ss << NAME << ":" << std::endl;
        ss << "  timestamp: " << timestamp << std::endl;
        ss << "  temperature: " << temperature << std::endl;
        ss << "  voltage_v: " << voltage_v << std::endl;
        ss << "  current_a: " << current_a << std::endl;
        ss << "  cell_count: " << cell_count << std::endl;
        ss << "  voltage_cell_v_0: " << voltage_cell_v_0 << std::endl;
        ss << "  voltage_cell_v_1: " << voltage_cell_v_1 << std::endl;
        ss << "  voltage_cell_v_2: " << voltage_cell_v_2 << std::endl;
        ss << "  voltage_cell_v_3: " << voltage_cell_v_3 << std::endl;
        ss << "  voltage_cell_v_4: " << voltage_cell_v_4 << std::endl;
        ss << "  voltage_cell_v_5: " << voltage_cell_v_5 << std::endl;
        ss << "  voltage_cell_v_6: " << voltage_cell_v_6 << std::endl;
        ss << "  voltage_cell_v_7: " << voltage_cell_v_7 << std::endl;
        ss << "  voltage_cell_v_8: " << voltage_cell_v_8 << std::endl;
        ss << "  voltage_cell_v_9: " << voltage_cell_v_9 << std::endl;

        return ss.str();
    }

    inline void serialize(mavlink::MsgMap &map) const override
    {
        map.reset(MSG_ID, LENGTH);

        map << timestamp;                     // offset: 0
        map << temperature;                   // offset: 8
        map << voltage_v;                     // offset: 12
        map << current_a;                     // offset: 16
        map << cell_count;                    // offset: 20
        map << voltage_cell_v_0;              // offset: 24
        map << voltage_cell_v_1;              // offset: 28
        map << voltage_cell_v_2;              // offset: 32
        map << voltage_cell_v_3;              // offset: 36
        map << voltage_cell_v_4;              // offset: 40
        map << voltage_cell_v_5;              // offset: 44
        map << voltage_cell_v_6;              // offset: 48
        map << voltage_cell_v_7;              // offset: 52
        map << voltage_cell_v_8;              // offset: 56
        map << voltage_cell_v_9;              // offset: 60
    }

    inline void deserialize(mavlink::MsgMap &map) override
    {
        map >> timestamp;                     // offset: 0
        map >> temperature;                   // offset: 8
        map >> voltage_v;                     // offset: 12
        map >> current_a;                     // offset: 16
        map >> cell_count;                    // offset: 20
        map >> voltage_cell_v_0;              // offset: 24
        map >> voltage_cell_v_1;              // offset: 28
        map >> voltage_cell_v_2;              // offset: 32
        map >> voltage_cell_v_3;              // offset: 36
        map >> voltage_cell_v_4;              // offset: 40
        map >> voltage_cell_v_5;              // offset: 44
        map >> voltage_cell_v_6;              // offset: 48
        map >> voltage_cell_v_7;              // offset: 52
        map >> voltage_cell_v_8;              // offset: 56
        map >> voltage_cell_v_9;              // offset: 60
    }
};

} // namespace msg
} // namespace common
} // namespace mavlink
