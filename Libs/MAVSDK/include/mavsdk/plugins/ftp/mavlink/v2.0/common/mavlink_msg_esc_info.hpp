// MESSAGE ESC_INFO support class

#pragma once

namespace mavlink {
namespace common {
namespace msg {

/**
 * @brief ESC_INFO message
 *
 * CUSTOM MAV MSG TEST.
 */
struct ESC_INFO : mavlink::Message {
    static constexpr msgid_t MSG_ID = 500;
    static constexpr size_t LENGTH = 21;
    static constexpr size_t MIN_LENGTH = 21;
    static constexpr uint8_t CRC_EXTRA = 95;
    static constexpr auto NAME = "ESC_INFO";


    uint64_t timestamp; /*<  time since system start (microseconds) */
    int32_t esc_rpm; /*<  Motor RPM, negative for reverse rotation [RPM] - if supported. */
    float esc_voltage; /*<  Voltage measured from current ESC [V] - if supported. */
    float esc_current; /*<  Current measured from current ESC [A] - if supported. */
    uint8_t esc_temperature; /*<  Temperature measured from current ESC [degC] - if supported */


    inline std::string get_name(void) const override
    {
            return NAME;
    }

    inline Info get_message_info(void) const override
    {
            return { MSG_ID, LENGTH, MIN_LENGTH, CRC_EXTRA };
    }

    inline std::string to_yaml(void) const override
    {
        std::stringstream ss;

        ss << NAME << ":" << std::endl;
        ss << "  timestamp: " << timestamp << std::endl;
        ss << "  esc_rpm: " << esc_rpm << std::endl;
        ss << "  esc_voltage: " << esc_voltage << std::endl;
        ss << "  esc_current: " << esc_current << std::endl;
        ss << "  esc_temperature: " << +esc_temperature << std::endl;

        return ss.str();
    }

    inline void serialize(mavlink::MsgMap &map) const override
    {
        map.reset(MSG_ID, LENGTH);

        map << timestamp;                     // offset: 0
        map << esc_rpm;                       // offset: 8
        map << esc_voltage;                   // offset: 12
        map << esc_current;                   // offset: 16
        map << esc_temperature;               // offset: 20
    }

    inline void deserialize(mavlink::MsgMap &map) override
    {
        map >> timestamp;                     // offset: 0
        map >> esc_rpm;                       // offset: 8
        map >> esc_voltage;                   // offset: 12
        map >> esc_current;                   // offset: 16
        map >> esc_temperature;               // offset: 20
    }
};

} // namespace msg
} // namespace common
} // namespace mavlink
