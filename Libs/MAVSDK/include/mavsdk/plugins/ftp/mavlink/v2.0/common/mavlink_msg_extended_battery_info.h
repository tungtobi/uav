#pragma once
// MESSAGE EXTENDED_BATTERY_INFO PACKING

#define MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO 501


typedef struct __mavlink_extended_battery_info_t {
 uint64_t timestamp; /*<  time since system start (microseconds)*/
 float temperature; /*<  Temperature of the battery. NaN if unknown.*/
 float voltage_v; /*<  Battery voltage in volts, 0 if unknown.*/
 float current_a; /*<  Battery current in amperes, -1 if unknown.*/
 int32_t cell_count; /*<  Number of cells.*/
 float voltage_cell_v_0; /*<  Battery individual cell voltages (Cell No. 1)*/
 float voltage_cell_v_1; /*<  Battery individual cell voltages (Cell No. 2)*/
 float voltage_cell_v_2; /*<  Battery individual cell voltages (Cell No. 3)*/
 float voltage_cell_v_3; /*<  Battery individual cell voltages (Cell No. 4)*/
 float voltage_cell_v_4; /*<  Battery individual cell voltages (Cell No. 5)*/
 float voltage_cell_v_5; /*<  Battery individual cell voltages (Cell No. 6)*/
 float voltage_cell_v_6; /*<  Battery individual cell voltages (Cell No. 7)*/
 float voltage_cell_v_7; /*<  Battery individual cell voltages (Cell No. 8)*/
 float voltage_cell_v_8; /*<  Battery individual cell voltages (Cell No. 9)*/
 float voltage_cell_v_9; /*<  Battery individual cell voltages (Cell No. 10)*/
} mavlink_extended_battery_info_t;

#define MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN 64
#define MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN 64
#define MAVLINK_MSG_ID_501_LEN 64
#define MAVLINK_MSG_ID_501_MIN_LEN 64

#define MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC 58
#define MAVLINK_MSG_ID_501_CRC 58



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_EXTENDED_BATTERY_INFO { \
    501, \
    "EXTENDED_BATTERY_INFO", \
    15, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_extended_battery_info_t, timestamp) }, \
         { "temperature", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_extended_battery_info_t, temperature) }, \
         { "voltage_v", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_extended_battery_info_t, voltage_v) }, \
         { "current_a", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_extended_battery_info_t, current_a) }, \
         { "cell_count", NULL, MAVLINK_TYPE_INT32_T, 0, 20, offsetof(mavlink_extended_battery_info_t, cell_count) }, \
         { "voltage_cell_v_0", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_0) }, \
         { "voltage_cell_v_1", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_1) }, \
         { "voltage_cell_v_2", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_2) }, \
         { "voltage_cell_v_3", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_3) }, \
         { "voltage_cell_v_4", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_4) }, \
         { "voltage_cell_v_5", NULL, MAVLINK_TYPE_FLOAT, 0, 44, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_5) }, \
         { "voltage_cell_v_6", NULL, MAVLINK_TYPE_FLOAT, 0, 48, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_6) }, \
         { "voltage_cell_v_7", NULL, MAVLINK_TYPE_FLOAT, 0, 52, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_7) }, \
         { "voltage_cell_v_8", NULL, MAVLINK_TYPE_FLOAT, 0, 56, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_8) }, \
         { "voltage_cell_v_9", NULL, MAVLINK_TYPE_FLOAT, 0, 60, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_9) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_EXTENDED_BATTERY_INFO { \
    "EXTENDED_BATTERY_INFO", \
    15, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_extended_battery_info_t, timestamp) }, \
         { "temperature", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_extended_battery_info_t, temperature) }, \
         { "voltage_v", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_extended_battery_info_t, voltage_v) }, \
         { "current_a", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_extended_battery_info_t, current_a) }, \
         { "cell_count", NULL, MAVLINK_TYPE_INT32_T, 0, 20, offsetof(mavlink_extended_battery_info_t, cell_count) }, \
         { "voltage_cell_v_0", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_0) }, \
         { "voltage_cell_v_1", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_1) }, \
         { "voltage_cell_v_2", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_2) }, \
         { "voltage_cell_v_3", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_3) }, \
         { "voltage_cell_v_4", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_4) }, \
         { "voltage_cell_v_5", NULL, MAVLINK_TYPE_FLOAT, 0, 44, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_5) }, \
         { "voltage_cell_v_6", NULL, MAVLINK_TYPE_FLOAT, 0, 48, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_6) }, \
         { "voltage_cell_v_7", NULL, MAVLINK_TYPE_FLOAT, 0, 52, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_7) }, \
         { "voltage_cell_v_8", NULL, MAVLINK_TYPE_FLOAT, 0, 56, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_8) }, \
         { "voltage_cell_v_9", NULL, MAVLINK_TYPE_FLOAT, 0, 60, offsetof(mavlink_extended_battery_info_t, voltage_cell_v_9) }, \
         } \
}
#endif

/**
 * @brief Pack a extended_battery_info message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param timestamp  time since system start (microseconds)
 * @param temperature  Temperature of the battery. NaN if unknown.
 * @param voltage_v  Battery voltage in volts, 0 if unknown.
 * @param current_a  Battery current in amperes, -1 if unknown.
 * @param cell_count  Number of cells.
 * @param voltage_cell_v_0  Battery individual cell voltages (Cell No. 1)
 * @param voltage_cell_v_1  Battery individual cell voltages (Cell No. 2)
 * @param voltage_cell_v_2  Battery individual cell voltages (Cell No. 3)
 * @param voltage_cell_v_3  Battery individual cell voltages (Cell No. 4)
 * @param voltage_cell_v_4  Battery individual cell voltages (Cell No. 5)
 * @param voltage_cell_v_5  Battery individual cell voltages (Cell No. 6)
 * @param voltage_cell_v_6  Battery individual cell voltages (Cell No. 7)
 * @param voltage_cell_v_7  Battery individual cell voltages (Cell No. 8)
 * @param voltage_cell_v_8  Battery individual cell voltages (Cell No. 9)
 * @param voltage_cell_v_9  Battery individual cell voltages (Cell No. 10)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_extended_battery_info_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint64_t timestamp, float temperature, float voltage_v, float current_a, int32_t cell_count, float voltage_cell_v_0, float voltage_cell_v_1, float voltage_cell_v_2, float voltage_cell_v_3, float voltage_cell_v_4, float voltage_cell_v_5, float voltage_cell_v_6, float voltage_cell_v_7, float voltage_cell_v_8, float voltage_cell_v_9)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, temperature);
    _mav_put_float(buf, 12, voltage_v);
    _mav_put_float(buf, 16, current_a);
    _mav_put_int32_t(buf, 20, cell_count);
    _mav_put_float(buf, 24, voltage_cell_v_0);
    _mav_put_float(buf, 28, voltage_cell_v_1);
    _mav_put_float(buf, 32, voltage_cell_v_2);
    _mav_put_float(buf, 36, voltage_cell_v_3);
    _mav_put_float(buf, 40, voltage_cell_v_4);
    _mav_put_float(buf, 44, voltage_cell_v_5);
    _mav_put_float(buf, 48, voltage_cell_v_6);
    _mav_put_float(buf, 52, voltage_cell_v_7);
    _mav_put_float(buf, 56, voltage_cell_v_8);
    _mav_put_float(buf, 60, voltage_cell_v_9);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN);
#else
    mavlink_extended_battery_info_t packet;
    packet.timestamp = timestamp;
    packet.temperature = temperature;
    packet.voltage_v = voltage_v;
    packet.current_a = current_a;
    packet.cell_count = cell_count;
    packet.voltage_cell_v_0 = voltage_cell_v_0;
    packet.voltage_cell_v_1 = voltage_cell_v_1;
    packet.voltage_cell_v_2 = voltage_cell_v_2;
    packet.voltage_cell_v_3 = voltage_cell_v_3;
    packet.voltage_cell_v_4 = voltage_cell_v_4;
    packet.voltage_cell_v_5 = voltage_cell_v_5;
    packet.voltage_cell_v_6 = voltage_cell_v_6;
    packet.voltage_cell_v_7 = voltage_cell_v_7;
    packet.voltage_cell_v_8 = voltage_cell_v_8;
    packet.voltage_cell_v_9 = voltage_cell_v_9;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC);
}

/**
 * @brief Pack a extended_battery_info message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param timestamp  time since system start (microseconds)
 * @param temperature  Temperature of the battery. NaN if unknown.
 * @param voltage_v  Battery voltage in volts, 0 if unknown.
 * @param current_a  Battery current in amperes, -1 if unknown.
 * @param cell_count  Number of cells.
 * @param voltage_cell_v_0  Battery individual cell voltages (Cell No. 1)
 * @param voltage_cell_v_1  Battery individual cell voltages (Cell No. 2)
 * @param voltage_cell_v_2  Battery individual cell voltages (Cell No. 3)
 * @param voltage_cell_v_3  Battery individual cell voltages (Cell No. 4)
 * @param voltage_cell_v_4  Battery individual cell voltages (Cell No. 5)
 * @param voltage_cell_v_5  Battery individual cell voltages (Cell No. 6)
 * @param voltage_cell_v_6  Battery individual cell voltages (Cell No. 7)
 * @param voltage_cell_v_7  Battery individual cell voltages (Cell No. 8)
 * @param voltage_cell_v_8  Battery individual cell voltages (Cell No. 9)
 * @param voltage_cell_v_9  Battery individual cell voltages (Cell No. 10)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_extended_battery_info_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint64_t timestamp,float temperature,float voltage_v,float current_a,int32_t cell_count,float voltage_cell_v_0,float voltage_cell_v_1,float voltage_cell_v_2,float voltage_cell_v_3,float voltage_cell_v_4,float voltage_cell_v_5,float voltage_cell_v_6,float voltage_cell_v_7,float voltage_cell_v_8,float voltage_cell_v_9)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, temperature);
    _mav_put_float(buf, 12, voltage_v);
    _mav_put_float(buf, 16, current_a);
    _mav_put_int32_t(buf, 20, cell_count);
    _mav_put_float(buf, 24, voltage_cell_v_0);
    _mav_put_float(buf, 28, voltage_cell_v_1);
    _mav_put_float(buf, 32, voltage_cell_v_2);
    _mav_put_float(buf, 36, voltage_cell_v_3);
    _mav_put_float(buf, 40, voltage_cell_v_4);
    _mav_put_float(buf, 44, voltage_cell_v_5);
    _mav_put_float(buf, 48, voltage_cell_v_6);
    _mav_put_float(buf, 52, voltage_cell_v_7);
    _mav_put_float(buf, 56, voltage_cell_v_8);
    _mav_put_float(buf, 60, voltage_cell_v_9);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN);
#else
    mavlink_extended_battery_info_t packet;
    packet.timestamp = timestamp;
    packet.temperature = temperature;
    packet.voltage_v = voltage_v;
    packet.current_a = current_a;
    packet.cell_count = cell_count;
    packet.voltage_cell_v_0 = voltage_cell_v_0;
    packet.voltage_cell_v_1 = voltage_cell_v_1;
    packet.voltage_cell_v_2 = voltage_cell_v_2;
    packet.voltage_cell_v_3 = voltage_cell_v_3;
    packet.voltage_cell_v_4 = voltage_cell_v_4;
    packet.voltage_cell_v_5 = voltage_cell_v_5;
    packet.voltage_cell_v_6 = voltage_cell_v_6;
    packet.voltage_cell_v_7 = voltage_cell_v_7;
    packet.voltage_cell_v_8 = voltage_cell_v_8;
    packet.voltage_cell_v_9 = voltage_cell_v_9;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC);
}

/**
 * @brief Encode a extended_battery_info struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param extended_battery_info C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_extended_battery_info_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_extended_battery_info_t* extended_battery_info)
{
    return mavlink_msg_extended_battery_info_pack(system_id, component_id, msg, extended_battery_info->timestamp, extended_battery_info->temperature, extended_battery_info->voltage_v, extended_battery_info->current_a, extended_battery_info->cell_count, extended_battery_info->voltage_cell_v_0, extended_battery_info->voltage_cell_v_1, extended_battery_info->voltage_cell_v_2, extended_battery_info->voltage_cell_v_3, extended_battery_info->voltage_cell_v_4, extended_battery_info->voltage_cell_v_5, extended_battery_info->voltage_cell_v_6, extended_battery_info->voltage_cell_v_7, extended_battery_info->voltage_cell_v_8, extended_battery_info->voltage_cell_v_9);
}

/**
 * @brief Encode a extended_battery_info struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param extended_battery_info C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_extended_battery_info_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_extended_battery_info_t* extended_battery_info)
{
    return mavlink_msg_extended_battery_info_pack_chan(system_id, component_id, chan, msg, extended_battery_info->timestamp, extended_battery_info->temperature, extended_battery_info->voltage_v, extended_battery_info->current_a, extended_battery_info->cell_count, extended_battery_info->voltage_cell_v_0, extended_battery_info->voltage_cell_v_1, extended_battery_info->voltage_cell_v_2, extended_battery_info->voltage_cell_v_3, extended_battery_info->voltage_cell_v_4, extended_battery_info->voltage_cell_v_5, extended_battery_info->voltage_cell_v_6, extended_battery_info->voltage_cell_v_7, extended_battery_info->voltage_cell_v_8, extended_battery_info->voltage_cell_v_9);
}

/**
 * @brief Send a extended_battery_info message
 * @param chan MAVLink channel to send the message
 *
 * @param timestamp  time since system start (microseconds)
 * @param temperature  Temperature of the battery. NaN if unknown.
 * @param voltage_v  Battery voltage in volts, 0 if unknown.
 * @param current_a  Battery current in amperes, -1 if unknown.
 * @param cell_count  Number of cells.
 * @param voltage_cell_v_0  Battery individual cell voltages (Cell No. 1)
 * @param voltage_cell_v_1  Battery individual cell voltages (Cell No. 2)
 * @param voltage_cell_v_2  Battery individual cell voltages (Cell No. 3)
 * @param voltage_cell_v_3  Battery individual cell voltages (Cell No. 4)
 * @param voltage_cell_v_4  Battery individual cell voltages (Cell No. 5)
 * @param voltage_cell_v_5  Battery individual cell voltages (Cell No. 6)
 * @param voltage_cell_v_6  Battery individual cell voltages (Cell No. 7)
 * @param voltage_cell_v_7  Battery individual cell voltages (Cell No. 8)
 * @param voltage_cell_v_8  Battery individual cell voltages (Cell No. 9)
 * @param voltage_cell_v_9  Battery individual cell voltages (Cell No. 10)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_extended_battery_info_send(mavlink_channel_t chan, uint64_t timestamp, float temperature, float voltage_v, float current_a, int32_t cell_count, float voltage_cell_v_0, float voltage_cell_v_1, float voltage_cell_v_2, float voltage_cell_v_3, float voltage_cell_v_4, float voltage_cell_v_5, float voltage_cell_v_6, float voltage_cell_v_7, float voltage_cell_v_8, float voltage_cell_v_9)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, temperature);
    _mav_put_float(buf, 12, voltage_v);
    _mav_put_float(buf, 16, current_a);
    _mav_put_int32_t(buf, 20, cell_count);
    _mav_put_float(buf, 24, voltage_cell_v_0);
    _mav_put_float(buf, 28, voltage_cell_v_1);
    _mav_put_float(buf, 32, voltage_cell_v_2);
    _mav_put_float(buf, 36, voltage_cell_v_3);
    _mav_put_float(buf, 40, voltage_cell_v_4);
    _mav_put_float(buf, 44, voltage_cell_v_5);
    _mav_put_float(buf, 48, voltage_cell_v_6);
    _mav_put_float(buf, 52, voltage_cell_v_7);
    _mav_put_float(buf, 56, voltage_cell_v_8);
    _mav_put_float(buf, 60, voltage_cell_v_9);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO, buf, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC);
#else
    mavlink_extended_battery_info_t packet;
    packet.timestamp = timestamp;
    packet.temperature = temperature;
    packet.voltage_v = voltage_v;
    packet.current_a = current_a;
    packet.cell_count = cell_count;
    packet.voltage_cell_v_0 = voltage_cell_v_0;
    packet.voltage_cell_v_1 = voltage_cell_v_1;
    packet.voltage_cell_v_2 = voltage_cell_v_2;
    packet.voltage_cell_v_3 = voltage_cell_v_3;
    packet.voltage_cell_v_4 = voltage_cell_v_4;
    packet.voltage_cell_v_5 = voltage_cell_v_5;
    packet.voltage_cell_v_6 = voltage_cell_v_6;
    packet.voltage_cell_v_7 = voltage_cell_v_7;
    packet.voltage_cell_v_8 = voltage_cell_v_8;
    packet.voltage_cell_v_9 = voltage_cell_v_9;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO, (const char *)&packet, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC);
#endif
}

/**
 * @brief Send a extended_battery_info message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_extended_battery_info_send_struct(mavlink_channel_t chan, const mavlink_extended_battery_info_t* extended_battery_info)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_extended_battery_info_send(chan, extended_battery_info->timestamp, extended_battery_info->temperature, extended_battery_info->voltage_v, extended_battery_info->current_a, extended_battery_info->cell_count, extended_battery_info->voltage_cell_v_0, extended_battery_info->voltage_cell_v_1, extended_battery_info->voltage_cell_v_2, extended_battery_info->voltage_cell_v_3, extended_battery_info->voltage_cell_v_4, extended_battery_info->voltage_cell_v_5, extended_battery_info->voltage_cell_v_6, extended_battery_info->voltage_cell_v_7, extended_battery_info->voltage_cell_v_8, extended_battery_info->voltage_cell_v_9);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO, (const char *)extended_battery_info, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC);
#endif
}

#if MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_extended_battery_info_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint64_t timestamp, float temperature, float voltage_v, float current_a, int32_t cell_count, float voltage_cell_v_0, float voltage_cell_v_1, float voltage_cell_v_2, float voltage_cell_v_3, float voltage_cell_v_4, float voltage_cell_v_5, float voltage_cell_v_6, float voltage_cell_v_7, float voltage_cell_v_8, float voltage_cell_v_9)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, temperature);
    _mav_put_float(buf, 12, voltage_v);
    _mav_put_float(buf, 16, current_a);
    _mav_put_int32_t(buf, 20, cell_count);
    _mav_put_float(buf, 24, voltage_cell_v_0);
    _mav_put_float(buf, 28, voltage_cell_v_1);
    _mav_put_float(buf, 32, voltage_cell_v_2);
    _mav_put_float(buf, 36, voltage_cell_v_3);
    _mav_put_float(buf, 40, voltage_cell_v_4);
    _mav_put_float(buf, 44, voltage_cell_v_5);
    _mav_put_float(buf, 48, voltage_cell_v_6);
    _mav_put_float(buf, 52, voltage_cell_v_7);
    _mav_put_float(buf, 56, voltage_cell_v_8);
    _mav_put_float(buf, 60, voltage_cell_v_9);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO, buf, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC);
#else
    mavlink_extended_battery_info_t *packet = (mavlink_extended_battery_info_t *)msgbuf;
    packet->timestamp = timestamp;
    packet->temperature = temperature;
    packet->voltage_v = voltage_v;
    packet->current_a = current_a;
    packet->cell_count = cell_count;
    packet->voltage_cell_v_0 = voltage_cell_v_0;
    packet->voltage_cell_v_1 = voltage_cell_v_1;
    packet->voltage_cell_v_2 = voltage_cell_v_2;
    packet->voltage_cell_v_3 = voltage_cell_v_3;
    packet->voltage_cell_v_4 = voltage_cell_v_4;
    packet->voltage_cell_v_5 = voltage_cell_v_5;
    packet->voltage_cell_v_6 = voltage_cell_v_6;
    packet->voltage_cell_v_7 = voltage_cell_v_7;
    packet->voltage_cell_v_8 = voltage_cell_v_8;
    packet->voltage_cell_v_9 = voltage_cell_v_9;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO, (const char *)packet, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_MIN_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_CRC);
#endif
}
#endif

#endif

// MESSAGE EXTENDED_BATTERY_INFO UNPACKING


/**
 * @brief Get field timestamp from extended_battery_info message
 *
 * @return  time since system start (microseconds)
 */
static inline uint64_t mavlink_msg_extended_battery_info_get_timestamp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field temperature from extended_battery_info message
 *
 * @return  Temperature of the battery. NaN if unknown.
 */
static inline float mavlink_msg_extended_battery_info_get_temperature(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field voltage_v from extended_battery_info message
 *
 * @return  Battery voltage in volts, 0 if unknown.
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_v(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field current_a from extended_battery_info message
 *
 * @return  Battery current in amperes, -1 if unknown.
 */
static inline float mavlink_msg_extended_battery_info_get_current_a(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field cell_count from extended_battery_info message
 *
 * @return  Number of cells.
 */
static inline int32_t mavlink_msg_extended_battery_info_get_cell_count(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  20);
}

/**
 * @brief Get field voltage_cell_v_0 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 1)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_0(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  24);
}

/**
 * @brief Get field voltage_cell_v_1 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 2)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  28);
}

/**
 * @brief Get field voltage_cell_v_2 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 3)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  32);
}

/**
 * @brief Get field voltage_cell_v_3 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 4)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_3(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  36);
}

/**
 * @brief Get field voltage_cell_v_4 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 5)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_4(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  40);
}

/**
 * @brief Get field voltage_cell_v_5 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 6)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_5(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  44);
}

/**
 * @brief Get field voltage_cell_v_6 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 7)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_6(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  48);
}

/**
 * @brief Get field voltage_cell_v_7 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 8)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_7(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  52);
}

/**
 * @brief Get field voltage_cell_v_8 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 9)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_8(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  56);
}

/**
 * @brief Get field voltage_cell_v_9 from extended_battery_info message
 *
 * @return  Battery individual cell voltages (Cell No. 10)
 */
static inline float mavlink_msg_extended_battery_info_get_voltage_cell_v_9(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  60);
}

/**
 * @brief Decode a extended_battery_info message into a struct
 *
 * @param msg The message to decode
 * @param extended_battery_info C-struct to decode the message contents into
 */
static inline void mavlink_msg_extended_battery_info_decode(const mavlink_message_t* msg, mavlink_extended_battery_info_t* extended_battery_info)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    extended_battery_info->timestamp = mavlink_msg_extended_battery_info_get_timestamp(msg);
    extended_battery_info->temperature = mavlink_msg_extended_battery_info_get_temperature(msg);
    extended_battery_info->voltage_v = mavlink_msg_extended_battery_info_get_voltage_v(msg);
    extended_battery_info->current_a = mavlink_msg_extended_battery_info_get_current_a(msg);
    extended_battery_info->cell_count = mavlink_msg_extended_battery_info_get_cell_count(msg);
    extended_battery_info->voltage_cell_v_0 = mavlink_msg_extended_battery_info_get_voltage_cell_v_0(msg);
    extended_battery_info->voltage_cell_v_1 = mavlink_msg_extended_battery_info_get_voltage_cell_v_1(msg);
    extended_battery_info->voltage_cell_v_2 = mavlink_msg_extended_battery_info_get_voltage_cell_v_2(msg);
    extended_battery_info->voltage_cell_v_3 = mavlink_msg_extended_battery_info_get_voltage_cell_v_3(msg);
    extended_battery_info->voltage_cell_v_4 = mavlink_msg_extended_battery_info_get_voltage_cell_v_4(msg);
    extended_battery_info->voltage_cell_v_5 = mavlink_msg_extended_battery_info_get_voltage_cell_v_5(msg);
    extended_battery_info->voltage_cell_v_6 = mavlink_msg_extended_battery_info_get_voltage_cell_v_6(msg);
    extended_battery_info->voltage_cell_v_7 = mavlink_msg_extended_battery_info_get_voltage_cell_v_7(msg);
    extended_battery_info->voltage_cell_v_8 = mavlink_msg_extended_battery_info_get_voltage_cell_v_8(msg);
    extended_battery_info->voltage_cell_v_9 = mavlink_msg_extended_battery_info_get_voltage_cell_v_9(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN? msg->len : MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN;
        memset(extended_battery_info, 0, MAVLINK_MSG_ID_EXTENDED_BATTERY_INFO_LEN);
    memcpy(extended_battery_info, _MAV_PAYLOAD(msg), len);
#endif
}
