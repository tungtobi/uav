#pragma once
// MESSAGE ESC_INFO PACKING

#define MAVLINK_MSG_ID_ESC_INFO 500


typedef struct __mavlink_esc_info_t {
 uint64_t timestamp; /*<  time since system start (microseconds)*/
 int32_t esc_rpm; /*<  Motor RPM, negative for reverse rotation [RPM] - if supported.*/
 float esc_voltage; /*<  Voltage measured from current ESC [V] - if supported.*/
 float esc_current; /*<  Current measured from current ESC [A] - if supported.*/
 uint8_t esc_temperature; /*<  Temperature measured from current ESC [degC] - if supported*/
} mavlink_esc_info_t;

#define MAVLINK_MSG_ID_ESC_INFO_LEN 21
#define MAVLINK_MSG_ID_ESC_INFO_MIN_LEN 21
#define MAVLINK_MSG_ID_500_LEN 21
#define MAVLINK_MSG_ID_500_MIN_LEN 21

#define MAVLINK_MSG_ID_ESC_INFO_CRC 95
#define MAVLINK_MSG_ID_500_CRC 95



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ESC_INFO { \
    500, \
    "ESC_INFO", \
    5, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_esc_info_t, timestamp) }, \
         { "esc_rpm", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_esc_info_t, esc_rpm) }, \
         { "esc_voltage", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_esc_info_t, esc_voltage) }, \
         { "esc_current", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_esc_info_t, esc_current) }, \
         { "esc_temperature", NULL, MAVLINK_TYPE_UINT8_T, 0, 20, offsetof(mavlink_esc_info_t, esc_temperature) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ESC_INFO { \
    "ESC_INFO", \
    5, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_esc_info_t, timestamp) }, \
         { "esc_rpm", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_esc_info_t, esc_rpm) }, \
         { "esc_voltage", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_esc_info_t, esc_voltage) }, \
         { "esc_current", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_esc_info_t, esc_current) }, \
         { "esc_temperature", NULL, MAVLINK_TYPE_UINT8_T, 0, 20, offsetof(mavlink_esc_info_t, esc_temperature) }, \
         } \
}
#endif

/**
 * @brief Pack a esc_info message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param timestamp  time since system start (microseconds)
 * @param esc_rpm  Motor RPM, negative for reverse rotation [RPM] - if supported.
 * @param esc_voltage  Voltage measured from current ESC [V] - if supported.
 * @param esc_current  Current measured from current ESC [A] - if supported.
 * @param esc_temperature  Temperature measured from current ESC [degC] - if supported
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_esc_info_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint64_t timestamp, int32_t esc_rpm, float esc_voltage, float esc_current, uint8_t esc_temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ESC_INFO_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_int32_t(buf, 8, esc_rpm);
    _mav_put_float(buf, 12, esc_voltage);
    _mav_put_float(buf, 16, esc_current);
    _mav_put_uint8_t(buf, 20, esc_temperature);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ESC_INFO_LEN);
#else
    mavlink_esc_info_t packet;
    packet.timestamp = timestamp;
    packet.esc_rpm = esc_rpm;
    packet.esc_voltage = esc_voltage;
    packet.esc_current = esc_current;
    packet.esc_temperature = esc_temperature;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ESC_INFO_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ESC_INFO;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ESC_INFO_MIN_LEN, MAVLINK_MSG_ID_ESC_INFO_LEN, MAVLINK_MSG_ID_ESC_INFO_CRC);
}

/**
 * @brief Pack a esc_info message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param timestamp  time since system start (microseconds)
 * @param esc_rpm  Motor RPM, negative for reverse rotation [RPM] - if supported.
 * @param esc_voltage  Voltage measured from current ESC [V] - if supported.
 * @param esc_current  Current measured from current ESC [A] - if supported.
 * @param esc_temperature  Temperature measured from current ESC [degC] - if supported
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_esc_info_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint64_t timestamp,int32_t esc_rpm,float esc_voltage,float esc_current,uint8_t esc_temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ESC_INFO_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_int32_t(buf, 8, esc_rpm);
    _mav_put_float(buf, 12, esc_voltage);
    _mav_put_float(buf, 16, esc_current);
    _mav_put_uint8_t(buf, 20, esc_temperature);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ESC_INFO_LEN);
#else
    mavlink_esc_info_t packet;
    packet.timestamp = timestamp;
    packet.esc_rpm = esc_rpm;
    packet.esc_voltage = esc_voltage;
    packet.esc_current = esc_current;
    packet.esc_temperature = esc_temperature;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ESC_INFO_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ESC_INFO;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ESC_INFO_MIN_LEN, MAVLINK_MSG_ID_ESC_INFO_LEN, MAVLINK_MSG_ID_ESC_INFO_CRC);
}

/**
 * @brief Encode a esc_info struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param esc_info C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_esc_info_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_esc_info_t* esc_info)
{
    return mavlink_msg_esc_info_pack(system_id, component_id, msg, esc_info->timestamp, esc_info->esc_rpm, esc_info->esc_voltage, esc_info->esc_current, esc_info->esc_temperature);
}

/**
 * @brief Encode a esc_info struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param esc_info C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_esc_info_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_esc_info_t* esc_info)
{
    return mavlink_msg_esc_info_pack_chan(system_id, component_id, chan, msg, esc_info->timestamp, esc_info->esc_rpm, esc_info->esc_voltage, esc_info->esc_current, esc_info->esc_temperature);
}

/**
 * @brief Send a esc_info message
 * @param chan MAVLink channel to send the message
 *
 * @param timestamp  time since system start (microseconds)
 * @param esc_rpm  Motor RPM, negative for reverse rotation [RPM] - if supported.
 * @param esc_voltage  Voltage measured from current ESC [V] - if supported.
 * @param esc_current  Current measured from current ESC [A] - if supported.
 * @param esc_temperature  Temperature measured from current ESC [degC] - if supported
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_esc_info_send(mavlink_channel_t chan, uint64_t timestamp, int32_t esc_rpm, float esc_voltage, float esc_current, uint8_t esc_temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ESC_INFO_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_int32_t(buf, 8, esc_rpm);
    _mav_put_float(buf, 12, esc_voltage);
    _mav_put_float(buf, 16, esc_current);
    _mav_put_uint8_t(buf, 20, esc_temperature);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ESC_INFO, buf, MAVLINK_MSG_ID_ESC_INFO_MIN_LEN, MAVLINK_MSG_ID_ESC_INFO_LEN, MAVLINK_MSG_ID_ESC_INFO_CRC);
#else
    mavlink_esc_info_t packet;
    packet.timestamp = timestamp;
    packet.esc_rpm = esc_rpm;
    packet.esc_voltage = esc_voltage;
    packet.esc_current = esc_current;
    packet.esc_temperature = esc_temperature;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ESC_INFO, (const char *)&packet, MAVLINK_MSG_ID_ESC_INFO_MIN_LEN, MAVLINK_MSG_ID_ESC_INFO_LEN, MAVLINK_MSG_ID_ESC_INFO_CRC);
#endif
}

/**
 * @brief Send a esc_info message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_esc_info_send_struct(mavlink_channel_t chan, const mavlink_esc_info_t* esc_info)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_esc_info_send(chan, esc_info->timestamp, esc_info->esc_rpm, esc_info->esc_voltage, esc_info->esc_current, esc_info->esc_temperature);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ESC_INFO, (const char *)esc_info, MAVLINK_MSG_ID_ESC_INFO_MIN_LEN, MAVLINK_MSG_ID_ESC_INFO_LEN, MAVLINK_MSG_ID_ESC_INFO_CRC);
#endif
}

#if MAVLINK_MSG_ID_ESC_INFO_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_esc_info_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint64_t timestamp, int32_t esc_rpm, float esc_voltage, float esc_current, uint8_t esc_temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_int32_t(buf, 8, esc_rpm);
    _mav_put_float(buf, 12, esc_voltage);
    _mav_put_float(buf, 16, esc_current);
    _mav_put_uint8_t(buf, 20, esc_temperature);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ESC_INFO, buf, MAVLINK_MSG_ID_ESC_INFO_MIN_LEN, MAVLINK_MSG_ID_ESC_INFO_LEN, MAVLINK_MSG_ID_ESC_INFO_CRC);
#else
    mavlink_esc_info_t *packet = (mavlink_esc_info_t *)msgbuf;
    packet->timestamp = timestamp;
    packet->esc_rpm = esc_rpm;
    packet->esc_voltage = esc_voltage;
    packet->esc_current = esc_current;
    packet->esc_temperature = esc_temperature;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ESC_INFO, (const char *)packet, MAVLINK_MSG_ID_ESC_INFO_MIN_LEN, MAVLINK_MSG_ID_ESC_INFO_LEN, MAVLINK_MSG_ID_ESC_INFO_CRC);
#endif
}
#endif

#endif

// MESSAGE ESC_INFO UNPACKING


/**
 * @brief Get field timestamp from esc_info message
 *
 * @return  time since system start (microseconds)
 */
static inline uint64_t mavlink_msg_esc_info_get_timestamp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field esc_rpm from esc_info message
 *
 * @return  Motor RPM, negative for reverse rotation [RPM] - if supported.
 */
static inline int32_t mavlink_msg_esc_info_get_esc_rpm(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  8);
}

/**
 * @brief Get field esc_voltage from esc_info message
 *
 * @return  Voltage measured from current ESC [V] - if supported.
 */
static inline float mavlink_msg_esc_info_get_esc_voltage(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field esc_current from esc_info message
 *
 * @return  Current measured from current ESC [A] - if supported.
 */
static inline float mavlink_msg_esc_info_get_esc_current(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field esc_temperature from esc_info message
 *
 * @return  Temperature measured from current ESC [degC] - if supported
 */
static inline uint8_t mavlink_msg_esc_info_get_esc_temperature(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  20);
}

/**
 * @brief Decode a esc_info message into a struct
 *
 * @param msg The message to decode
 * @param esc_info C-struct to decode the message contents into
 */
static inline void mavlink_msg_esc_info_decode(const mavlink_message_t* msg, mavlink_esc_info_t* esc_info)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    esc_info->timestamp = mavlink_msg_esc_info_get_timestamp(msg);
    esc_info->esc_rpm = mavlink_msg_esc_info_get_esc_rpm(msg);
    esc_info->esc_voltage = mavlink_msg_esc_info_get_esc_voltage(msg);
    esc_info->esc_current = mavlink_msg_esc_info_get_esc_current(msg);
    esc_info->esc_temperature = mavlink_msg_esc_info_get_esc_temperature(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ESC_INFO_LEN? msg->len : MAVLINK_MSG_ID_ESC_INFO_LEN;
        memset(esc_info, 0, MAVLINK_MSG_ID_ESC_INFO_LEN);
    memcpy(esc_info, _MAV_PAYLOAD(msg), len);
#endif
}
