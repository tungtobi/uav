#include "./Models/AircraftModel/AircraftModel.h"

//Implementation of public functions
void AircraftModel::resetBuffer() {
    AircraftModel::setAddress("N/A");
    AircraftModel::setConnectStatus(0);
    AircraftModel::setConnectInitializationStatus(false);
    AircraftModel::setSystemID(0);
    AircraftModel::setConnectCommand(false);
    for (int a = 0; a < 9; a++) {
        AircraftModel::controlSurfacesBuffer[a] = 0;
    }
    AircraftModel::setRoll(0);
    AircraftModel::setPitch(0);
    AircraftModel::setHeading(0);
    AircraftModel::setBatteryRemains(0);
    AircraftModel::setBatteryTemp(0);
    AircraftModel::setBatteryVoltage(0);
    AircraftModel::setBatteryCurrent(0);
    AircraftModel::setBatteryCellCount(0);
    for (int i = 0; i < 10; i++) {
        AircraftModel::cellVoltagesBuffer[i] = 0;
    }
    AircraftModel::setIAS(0);
    AircraftModel::setPowerPercent(0);
    AircraftModel::setVertSpeed(0);
    AircraftModel::setTemperature(0);
    AircraftModel::setAltitude(0);
    AircraftModel::setLatitude(0);
    AircraftModel::setLongitude(0);
    AircraftModel::setAirPress(0);
    AircraftModel::setGroundSpeedForward(0);
    AircraftModel::setGroundSpeedRight(0);
}



std::string AircraftModel::address() {
    return addressBuffer;
}

void AircraftModel::setAddress(std::string address) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    addressBuffer = address;
}

bool AircraftModel::connectStatus() {
    return connectStatusBuffer;
}

void AircraftModel::setConnectStatus(bool connectStatus) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    connectStatusBuffer = connectStatus;
}

bool AircraftModel::connectInitializationStatus() {
    return connectInitializationStatusBuffer;
}

void AircraftModel::setConnectInitializationStatus(bool connectInitStatus) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    connectInitializationStatusBuffer = connectInitStatus;
}

bool AircraftModel::connectCommand() {
    return connectCommandBuffer;
}

void AircraftModel::setConnectCommand(bool connectCmd) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    connectCommandBuffer = connectCmd;
}

void AircraftModel::setSystemID(uint64_t systemIDResult){
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    systemIDBuffer = systemIDResult;
}

uint64_t AircraftModel::systemID() {
    return systemIDBuffer;
}


//Implementation of public methods
void AircraftModel::setControlSurfaces(float ctrlSurfs[9]) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    for (int a = 0; a < 9; a++) {
        controlSurfacesBuffer[a] = ctrlSurfs[a] * 100;
    }
}

float AircraftModel::controlSurfaces(int ctrlSurfIndex) {
    return controlSurfacesBuffer[ctrlSurfIndex];
}

void AircraftModel::setRoll(float roll) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    rollBuffer = roll;
}

float AircraftModel::roll() {
    return rollBuffer;
}

void AircraftModel::setPitch(float pitch) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    pitchBuffer = pitch;
}

float AircraftModel::pitch() {
    return pitchBuffer;
}

void AircraftModel::setHeading(float hdg) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    headingBuffer = hdg;
}

float AircraftModel::heading() {
    return headingBuffer;
}

void AircraftModel::setIAS(float indctAirSpeed) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    IASBuffer = indctAirSpeed;
}

float AircraftModel::IAS() {
    return IASBuffer;
}

void AircraftModel::setPowerPercent(float throttPercent) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    powerPercentBuffer = throttPercent;
}

float AircraftModel::powerPercent() {
    return powerPercentBuffer * 100;
}

void AircraftModel::setVertSpeed(float vSpd) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    vertSpeedBuffer = vSpd;
}

float AircraftModel::vertSpeed() {
    return vertSpeedBuffer;
}

void AircraftModel::setAltitude(float alt){
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    altitudeBuffer = alt;
}

float AircraftModel::altitude() {
    return altitudeBuffer;
}

void AircraftModel::setBatteryRemains(float remains) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    batteryRemainsBuffer = remains;
}

float AircraftModel::batteryRemains() {
    return batteryRemainsBuffer * 100;
}

void AircraftModel::setBatteryTemp(float battTemp) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    batteryTempBuffer = battTemp;
}

float AircraftModel::batteryTemp() {
    return batteryTempBuffer;
}

void AircraftModel::setBatteryVoltage(float battVolt) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    batteryVoltageBuffer = battVolt;
}

float AircraftModel::batteryVoltage() {
    return batteryVoltageBuffer;
}

void AircraftModel::setBatteryCurrent(float battCurr) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    batteryCurrentBuffer = battCurr;
}

float AircraftModel::batteryCurrent() {
    return batteryCurrentBuffer;
}

void AircraftModel::setBatteryCellCount(int32_t battCells) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    batteryCellCountBuffer = battCells;
}

int32_t AircraftModel::batteryCellCount() {
    return batteryCellCountBuffer;
}

void AircraftModel::setCellVoltages(float cellVolts[10]) {
    for (int i = 0; i < 10; i++) {
        cellVoltagesBuffer[i] = cellVolts[i];
//        std::cout << "Cell: " << i << " Volts: " << cellVoltagesBuffer[i] << std::endl;
    }
}

float AircraftModel::cellVoltages(int cellIndex) {
    return cellVoltagesBuffer[cellIndex];
}

void AircraftModel::setTemperature(float temp) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    temperatureBuffer = temp;
}

float AircraftModel::temperature() {
    return temperatureBuffer;
}

void AircraftModel::setLatitude(double lat) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    latitudeBuffer = lat;
}

double AircraftModel::latitude() {
    return latitudeBuffer;
}


void AircraftModel::setLongitude(double lon) {
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    longitudeBuffer = lon;
}

double AircraftModel::longitude() {
    return longitudeBuffer;
}

void AircraftModel::setAirPress(float airPress){
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    airPressBuffer = airPress;
}

float AircraftModel::airPress() {
    return airPressBuffer;
}

void AircraftModel::setGroundSpeedForward(float gsForward){
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    groundSpeedForwardBuffer = gsForward;
}

float AircraftModel::groundSpeedForward() {
    return groundSpeedForwardBuffer;
}

void AircraftModel::setGroundSpeedRight(float gsRight){
    std::lock_guard<std::mutex> guard(modelWriteMutex);
    groundSpeedRightBuffer = gsRight;
}

float AircraftModel::groundSpeedRight() {
    return groundSpeedRightBuffer;
}
