#ifndef AIRCRAFTMODEL_H
#define AIRCRAFTMODEL_H

#include "iostream"
#include <mavsdk.h>
#include "mutex"


class AircraftModel
{
    public:
        //Public vars
        inline static mavsdk::Mavsdk aircraftInstance; //variable to save mavsdk instance

        //Public functions
        static void resetBuffer();

        //Getter functions
        static std::string address();
        static uint64_t systemID();
        static bool connectStatus();
        static bool connectInitializationStatus();
        static bool connectCommand();
        static float controlSurfaces(int ctrlSurfIndex);
        static float roll();
        static float pitch();
        static float heading();
        static float powerPercent();
        static float vertSpeed();
        static float altitude();
        static float IAS();
        static float batteryRemains();
        static float batteryTemp();
        static float batteryVoltage();
        static float batteryCurrent();
        static int32_t batteryCellCount();
        static float cellVoltages(int cellIndex);
        static float temperature();
        static double latitude();
        static double longitude();
        static float airPress();
        static float groundSpeedForward();
        static float groundSpeedRight();

        //Setters functions
        static void setAddress(std::string address);
        static void setSystemID(uint64_t systemIDResult);
        static void setConnectStatus(bool connectStatus);
        static void setConnectInitializationStatus(bool connectInitStatus);
        static void setConnectCommand(bool connectCmd);
        static void setControlSurfaces(float ctrlSurfs[8]);
        static void setRoll(float roll);
        static void setPitch(float pitch);
        static void setHeading(float hdg);
        static void setPowerPercent(float throttPercent);
        static void setVertSpeed(float vSpd);
        static void setAltitude(float alt);
        static void setIAS(float indctAirSpeed);
        static void setBatteryRemains(float remains);
        static void setBatteryTemp(float battTemp);
        static void setBatteryVoltage(float battVolt);
        static void setBatteryCurrent(float battCurr);
        static void setBatteryCellCount(int32_t battCells);
        static void setCellVoltages(float cellVolts[10]);
        static void setTemperature(float temp);
        static void setLatitude(double lat);
        static void setLongitude(double lon);
        static void setAirPress(float airPress);
        static void setGroundSpeedForward(float gsForward);
        static void setGroundSpeedRight(float gsRight);


    private:
        //Private functions

        //Private locking mutex to protect writing to variables
        inline static std::mutex modelWriteMutex;

        //Private internal buffer variables
        inline static std::string addressBuffer;
        inline static uint64_t systemIDBuffer;
        inline static bool connectStatusBuffer;
        inline static bool connectInitializationStatusBuffer;
        inline static bool connectCommandBuffer;
        inline static float controlSurfacesBuffer[8];
        inline static float rollBuffer;
        inline static float pitchBuffer;
        inline static float headingBuffer;
        inline static float powerPercentBuffer;
        inline static float vertSpeedBuffer;
        inline static float altitudeBuffer;
        inline static float IASBuffer;
        inline static float batteryRemainsBuffer;
        inline static float batteryTempBuffer;
        inline static float batteryVoltageBuffer;
        inline static float batteryCurrentBuffer;
        inline static int32_t batteryCellCountBuffer;
        inline static float cellVoltagesBuffer[10];
        inline static float temperatureBuffer;
        inline static double latitudeBuffer;
        inline static double longitudeBuffer;
        inline static float airPressBuffer;
        inline static float groundSpeedForwardBuffer;
        inline static float groundSpeedRightBuffer;

};

#endif // AIRCRAFTMODEL_H
