#ifndef AIRCRAFTSYSTEMBACKEND_H
#define AIRCRAFTSYSTEMBACKEND_H

#include <QRunnable>
#include <QThread>
#include "./Models/AircraftModel/AircraftModel.h"

class AircraftSystemBackend : public QRunnable
{
private:
    //Private variables
    std::vector<std::vector<std::string>> existingSystems;

    //Private methods
    void addToExistingSystems(std::string address, uint64_t systemID);

    void systemExistanceCheck(std::string address);

public:
    AircraftSystemBackend();

    //Public methods
    void run();

};

#endif // AIRCRAFTSYSTEMBACKEND_H
