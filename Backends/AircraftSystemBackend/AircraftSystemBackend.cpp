#include "./Backends/AircraftSystemBackend/AircraftSystemBackend.h"

//Implementation of constructor
AircraftSystemBackend::AircraftSystemBackend(){
}


//Implementation of public methods
void AircraftSystemBackend::run(){
    while (true){

        if (AircraftModel::connectCommand()) {
            if (!AircraftModel::connectInitializationStatus()) {
                mavsdk::ConnectionResult connection_result = AircraftModel::aircraftInstance.add_any_connection(AircraftModel::address());
                if(connection_result == mavsdk::ConnectionResult::Success) {
                    AircraftModel::aircraftInstance.register_on_discover([this](uint64_t systemIDNew) {
                        AircraftModel::setSystemID(systemIDNew);
                        this->addToExistingSystems(AircraftModel::address(), systemIDNew);
                    });
                    AircraftModel::setConnectInitializationStatus(true);
                }
                else if(connection_result == mavsdk::ConnectionResult::BindError) {
                    this->systemExistanceCheck(AircraftModel::address());
                }
            }
        }
        else {
            if (AircraftModel::connectInitializationStatus() || AircraftModel::connectStatus()
                    || (AircraftModel::address() != "N/A") ) {
                AircraftModel::setConnectInitializationStatus(false);
                AircraftModel::setConnectStatus(false);
                AircraftModel::resetBuffer();
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(42));
    }
}


//Implementation of Private methods
void AircraftSystemBackend::addToExistingSystems(std::string address, uint64_t uuid) {
    std::ostringstream uuidStrCurrent;
    uuidStrCurrent << uuid;
    std::vector<std::string> newSystem{address, uuidStrCurrent.str()};
    this->existingSystems.push_back(newSystem);
}

void AircraftSystemBackend::systemExistanceCheck(std::string address) {
    for (auto & sys : existingSystems) {
        if (sys[0] == address) {
            uint64_t existingUuid;
            std::istringstream uuidStr(sys[1]);
            uuidStr >> existingUuid;
            AircraftModel::setSystemID(existingUuid);
            AircraftModel::setConnectInitializationStatus(true);
        }
    }
}

