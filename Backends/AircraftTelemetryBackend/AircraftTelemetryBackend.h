#ifndef AIRCRAFTTELEMETRYBACKEND_H
#define AIRCRAFTTELEMETRYBACKEND_H

#include <plugins/telemetry/telemetry.h>
#include <QRunnable>
#include <QThread>
#include "./Models/AircraftModel/AircraftModel.h"

Q_DECLARE_METATYPE(uint32_t)
Q_DECLARE_METATYPE(int32_t)

using namespace mavsdk;
class AircraftTelemetryBackend : public QRunnable
{
    private:

        //Private variables
        float controlsSurfaces[8];
        float roll;
        float pitch;
        float heading;
        float batteryRemains;
        float batteryTemp;
        float batteryVoltage;
        float batteryCurrent;
        int32_t batteryCellCount;
        float cellVoltages[10];
        float IAS;
        float powerPercent;
        float vertSpeed;
        float temperature;
        float altitude;
        double latitude;
        double longitude;
        float airPress;
        float groundSpeedForward;
        float groundSpeedRight;

        bool preStreamingSetup;
        int rateSetErrorCounter;

    public:
        AircraftTelemetryBackend();

        //Public methods
        void run();

};

#endif // AIRCRAFTTELEMETRYBACKEND_H
