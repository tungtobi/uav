#include "./Backends/AircraftTelemetryBackend/AircraftTelemetryBackend.h"

//Implementation of constructor
AircraftTelemetryBackend::AircraftTelemetryBackend()
{
    qRegisterMetaType<uint32_t>("uint32_t");
    qRegisterMetaType<int32_t>("int32_t");
    rateSetErrorCounter = 0;
    preStreamingSetup = false;
}


//Implementation of Public methods
void AircraftTelemetryBackend::run()
{
    //Variable declaration
    //Rate setting result callback buffers
    mavsdk::Telemetry::Result ctrl_target_set_rate_result;
    mavsdk::Telemetry::Result att_set_rate_result;
    mavsdk::Telemetry::Result battery_set_rate_result;
    mavsdk::Telemetry::Result ext_battery_set_rate_result;
    mavsdk::Telemetry::Result fw_metric_set_rate_result;
    mavsdk::Telemetry::Result imu_stat_set_rate_result;
    mavsdk::Telemetry::Result position_set_rate_result;
    mavsdk::Telemetry::Result scaled_press_set_rate_result;
    mavsdk::Telemetry::Result odometry_set_rate_result;
    //Aircraft's system pointer (required because default pointer is destructed after every use (why? ask the MAVSDK team!)
    std::shared_ptr<mavsdk::Telemetry> ac_system;

    //Main backend loop
    while (true)
    {
        //Preconnect setup/variable initialization
        if (AircraftModel::connectCommand())
        {
            if (AircraftModel::connectInitializationStatus())
            {
                if (!this->preStreamingSetup)
                {
                    ac_system = std::make_shared<mavsdk::Telemetry>(AircraftModel::aircraftInstance.system(AircraftModel::systemID()));
                    ac_system->set_rate_actuator_control_target_async(24.0, [&ctrl_target_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        ctrl_target_set_rate_result = result; //update ctrl_target_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_attitude_async(24.0, [&att_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        att_set_rate_result = result; //update att_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_battery_async(24.0, [&battery_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        battery_set_rate_result = result; //update battery_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_extended_battery_info_async(24.0, [&ext_battery_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        ext_battery_set_rate_result = result; //update ext_battery_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_fixedwing_metrics_async(24.0, [&fw_metric_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        fw_metric_set_rate_result = result; //update fw_metric_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_imu_async(24.0, [&imu_stat_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        imu_stat_set_rate_result = result; //update imu_stat_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_position_async(24.0, [&position_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        position_set_rate_result = result; //update position_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_scaled_pressure_async(24.0, [&scaled_press_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        scaled_press_set_rate_result = result; //update scaled_press_set_rate_result
                    });//update interval: 24hz (24 times per sec)
                    ac_system->set_rate_odometry_async(24.0, [&odometry_set_rate_result](mavsdk::Telemetry::Result result)
                    {
                        odometry_set_rate_result = result; //update odo_set_rate_result
                    });//update interval: 24hz (24 times per sec)

                    //Rate setting error handle
                    if (ctrl_target_set_rate_result != mavsdk::Telemetry::Result::Success
                        || att_set_rate_result != mavsdk::Telemetry::Result::Success
                        || battery_set_rate_result != mavsdk::Telemetry::Result::Success
                        || ext_battery_set_rate_result != mavsdk::Telemetry::Result::Success
                        || fw_metric_set_rate_result != mavsdk::Telemetry::Result::Success
                        || imu_stat_set_rate_result != mavsdk::Telemetry::Result::Success
                        || position_set_rate_result != mavsdk::Telemetry::Result::Success
                        || scaled_press_set_rate_result != mavsdk::Telemetry::Result::Success
                        || odometry_set_rate_result != mavsdk::Telemetry::Result::Success)
                    {
                        if (rateSetErrorCounter < 240)   //Retry for 10 seconds if set rate error occurs
                        {
                            //Note: update rate is 24Hz, so 240 attempts equal 10 seconds
                            rateSetErrorCounter++;
                        }
                        else   //Will throw error and terminate if rate setting errors occured for more than 10 seconds
                        {
                            AircraftModel::setConnectCommand(false);
                            std::cout << "Rate settings error, connection terminated!" << std::endl;
                        }
                    } //handle result update failure exception
                    else
                    {
                        rateSetErrorCounter = 0;
                        this->preStreamingSetup = true;
                    }
                }
                //Actual streaming update
                else
                {
                    if (AircraftModel::aircraftInstance.system().is_connected())
                    {
                        AircraftModel::setConnectStatus(true); //From here system is officially connected
                        //Update functions of MAVSDK
                        ac_system->subscribe_actuator_control_target([this](mavsdk::Telemetry::ActuatorControlTarget act)
                        {
                            for (int a = 0; a < act.controls.size(); a++)
                            {
                                controlsSurfaces[a] = act.controls[a];
                            }
                        });
                        ac_system->subscribe_attitude_euler([this](mavsdk::Telemetry::EulerAngle att)
                        {
                            this->pitch = att.pitch_deg;
                            this->roll = att.roll_deg;
                            this->heading = att.yaw_deg;
                        });
                        ac_system->subscribe_battery([this](mavsdk::Telemetry::Battery batt)
                        {
                            this->batteryRemains = batt.remaining_percent;
                        });
                        ac_system->subscribe_extended_battery_info([this](mavsdk::Telemetry::ExtendedBatteryInfo extBatt)
                        {
                            this->batteryTemp = extBatt.temperature;
                            this->batteryVoltage = extBatt.voltage_v;
                            this->batteryCurrent = extBatt.current_a;
                            this->batteryCellCount = extBatt.cell_count;
                            this->cellVoltages[0] = extBatt.voltage_cell_v0;
                            this->cellVoltages[1] = extBatt.voltage_cell_v1;
                            this->cellVoltages[2] = extBatt.voltage_cell_v2;
                            this->cellVoltages[3] = extBatt.voltage_cell_v3;
                            this->cellVoltages[4] = extBatt.voltage_cell_v4;
                            this->cellVoltages[5] = extBatt.voltage_cell_v5;
                            this->cellVoltages[6] = extBatt.voltage_cell_v6;
                            this->cellVoltages[7] = extBatt.voltage_cell_v7;
                            this->cellVoltages[8] = extBatt.voltage_cell_v8;
                            this->cellVoltages[9] = extBatt.voltage_cell_v9;
                        });
                        ac_system->subscribe_fixedwing_metrics([this](mavsdk::Telemetry::FixedwingMetrics fwm)
                        {
                            this->IAS = fwm.airspeed_m_s;
                            this->powerPercent = fwm.throttle_percentage;
                            this->vertSpeed = fwm.climb_rate_m_s;
                        });
                        ac_system->subscribe_imu([this](mavsdk::Telemetry::Imu imu)
                        {
                            this->temperature = imu.temperature_degc;
                        });
                        ac_system->subscribe_position([this](mavsdk::Telemetry::Position p)
                        {
                            this->altitude = p.absolute_altitude_m;
                            this->latitude = p.latitude_deg;
                            this->longitude = p.longitude_deg;
                        });
                        ac_system->subscribe_scaled_pressure([this](mavsdk::Telemetry::ScaledPressure spress)
                        {
                            this->airPress = spress.absolute_pressure_hpa;
                        });
                        ac_system->subscribe_odometry([this](mavsdk::Telemetry::Odometry odo)
                        {
                            this->groundSpeedForward = odo.velocity_body.x_m_s;
                            this->groundSpeedRight = odo.velocity_body.y_m_s;
                        });
                        //Save info fetched to local model
                        AircraftModel::setControlSurfaces(this->controlsSurfaces);
                        AircraftModel::setRoll(this->roll);
                        AircraftModel::setPitch(this->pitch);
                        AircraftModel::setHeading(this->heading);
                        AircraftModel::setPowerPercent(this->powerPercent);
                        AircraftModel::setVertSpeed(this->vertSpeed);
                        AircraftModel::setAltitude(this->altitude);
                        AircraftModel::setIAS(this->IAS);
                        AircraftModel::setBatteryRemains(this->batteryRemains);
                        AircraftModel::setBatteryTemp(this->batteryTemp);
                        AircraftModel::setBatteryVoltage(this->batteryVoltage);
                        AircraftModel::setBatteryCurrent(this->batteryCurrent);
                        AircraftModel::setBatteryCellCount(this->batteryCellCount);
                        AircraftModel::setCellVoltages(this->cellVoltages);
                        AircraftModel::setTemperature(this->temperature);
                        AircraftModel::setLatitude(this->latitude);
                        AircraftModel::setLongitude(this->longitude);
                        AircraftModel::setAirPress(this->airPress);
                        AircraftModel::setGroundSpeedForward(this->groundSpeedForward);
                        AircraftModel::setGroundSpeedRight(this->groundSpeedRight);
                    }
                    else if (!AircraftModel::aircraftInstance.system().is_connected())  //Connection lost
                    {
                        AircraftModel::setConnectStatus(false);
                    }
                }
            }
        }
        else
        {
            if (this->preStreamingSetup)
            {
                this->preStreamingSetup = false;
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(42));
    }
}
