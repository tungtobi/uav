QT += quick core

CONFIG += c++11

QMAKE_CXXFLAGS += -std=gnu++1z

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -LLibs/MAVSDK/lib/ -lmavsdk
LIBS += -LLD_LIBRARY_PATH

INCLUDEPATH += Libs/MAVSDK/include/mavsdk
DEPENDPATH += Libs/MAVSDK/include/mavsdk

SOURCES += main.cpp \
        ./FrontEnd/Boundaries/connectionDisplay/connectionDisplay.cpp \
        ./FrontEnd/Controllers/connectionDisplayCtrlr/connectionDisplayCtrlr.cpp \
        ./FrontEnd/Boundaries/primaryFlightDisplay/primaryFlightDisplay.cpp \
        ./FrontEnd/Controllers/primaryFlightDisplayCtrlr/primaryFlightDisplayCtrlr.cpp \
        ./FrontEnd/Boundaries/systemDisplay/systemDisplay.cpp \
        ./FrontEnd/Controllers/systemDisplayCtrlr/systemDisplayCtrlr.cpp \
        ./Backends/AircraftSystemBackend/AircraftSystemBackend.cpp \
        ./Backends/AircraftTelemetryBackend/AircraftTelemetryBackend.cpp \
        ./Models/AircraftModel/AircraftModel.cpp \
        ./FrontEnd/Boundaries/navigationDisplay/navigationDisplay.cpp \
        ./FrontEnd/Controllers/navigationDisplayCtrlr/navigationDisplayCtrlr.cpp

HEADERS += \
        ./FrontEnd/Boundaries/connectionDisplay/connectionDisplay.h \
        ./FrontEnd/Controllers/connectionDisplayCtrlr/connectionDisplayCtrlr.h \
        ./FrontEnd/Boundaries/primaryFlightDisplay/primaryFlightDisplay.h \
        ./FrontEnd/Controllers/primaryFlightDisplayCtrlr/primaryFlightDisplayCtrlr.h \
        ./FrontEnd/Boundaries/systemDisplay/systemDisplay.h \
        ./FrontEnd/Controllers/systemDisplayCtrlr/systemDisplayCtrlr.h \
        ./Backends/AircraftSystemBackend/AircraftSystemBackend.h \
        ./Backends/AircraftTelemetryBackend/AircraftTelemetryBackend.h \
        ./Models/AircraftModel/AircraftModel.h \
        ./FrontEnd/Boundaries/navigationDisplay/navigationDisplay.h \
        ./FrontEnd/Controllers/navigationDisplayCtrlr/navigationDisplayCtrlr.h \
#        ./Libs/MAVSDK/install/include/mavsdk/mavsdk.h \
#        ./Libs/MAVSDK/install/include/mavsdk/plugins/telemetry/telemetry.h

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#MAVSDK Library
unix:!macx: LIBS += -LLibs/MAVSDK/lib/ -lmavsdk
unix:!macx: LIBS += -LLibs/MAVSDK/lib/ -lmavsdk_telemetry
unix:!macx: LIBS += -LLibs/MAVSDK/lib/ -lmavsdk_action
unix:!macx: LIBS += -LLibs/MAVSDK/lib/ -lmavsdk_info
INCLUDEPATH += Libs/MAVSDK/include/mavsdk
DEPENDPATH += Libs/MAVSDK/include/mavsdk
